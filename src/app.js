'use strict';
var app = require('express')(),
    http = require('http'),
    server = http.createServer(app),
    config = require('./config/config'),
    common = require('./app/common');

common.connect();
var webServer = server.listen(config.port);
require('./config/express')(app, config);

webServer.on('connection', function (id) {});
module.exports.web = webServer;