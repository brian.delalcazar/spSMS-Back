var mongoose = require('mongoose'),
	Role = mongoose.model('Role'),
	User = mongoose.model('User');

exports.up = function(next) {
	Role.findOne({ name: 'root' }, function(err, role) {

		var user = new User();
		user.username = 'root@siplik.com';
		user.password = '4Geeks#siplik';
		user.active.status = true;
		user.roles.push(role._id);

		user.save(function(err, user) {
			if (err) return next(err);
			next();
		})

	});
};

exports.down = function(next) {
	User.findOneAndRemove({ username: 'root@siplik.com' }, next);
};
