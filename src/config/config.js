var path = require('path'),
    rootPath = path.normalize(__dirname + '/..'),
    database = require('./database'),
    env = process.env.NODE_ENV || 'development';

//SET PATHS FOR ENVIRONMENT
var config = {
    development: {
        env:"develop",
        rootPath: rootPath,
        app: {
            publicPath: 'http://localhost:3005',
            frontendPath: 'http://localhost:9000'
        },
        port: 3005,
        db: 'mongodb://'+database.host+'/'+database.db,
        key: "spSMSSpeficicHasKey"
    },
    test: {
        env:"test",
        rootPath: rootPath,
        app: {
            publicPath: 'http://localhost:3005',
            frontendPath: 'http://localhost:9000'
        },
        port: 3005,
        db: 'mongodb://'+process.env.WERCKER_MONGODB_HOST+'/spSMS',
        key: "spSMSSpeficicHasKey"
    },
    staging: {
        env:"staging",
        rootPath: rootPath,
        app: {
            publicPath: 'https://devsmsapi.siplik.com',
            frontendPath: 'https://devsms.siplik.com'
        },
        port: 3005,
        db: 'mongodb://'+database.user+':'+database.pwd+'@'+database.host+'/'+database.db,
        key: "spSMSSpeficicHasKey"
    }
    // production: {
    //     env:"production",
    //     rootPath: rootPath,
    //     app: {
    //         publicPath: 'https://api.siplik.com',
    //         frontendPath: 'https://doctor.siplik.com'
    //     },
    //     port: 3005,
    //     db: 'mongodb://'+database.user+':'+database.pwd+'@'+database.host+'/'+database.db
    // }
};

module.exports = config[env];
