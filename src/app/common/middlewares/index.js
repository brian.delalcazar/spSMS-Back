var mongoose = require('mongoose'),
    common = require('../index'),
    crypto = require('crypto'),
    User = mongoose.model('User'),
    Role = mongoose.model('Role'),
    Doctor = mongoose.model('Doctor'),
    Premium = mongoose.model('Premium'),
    Client = mongoose.model('Client'),
    Insurance = mongoose.model('Insurance'),
    moment = require('moment'),
    Appointment = mongoose.model('Appointment'),
    Clinic = mongoose.model('Clinic');

exports.user = {
    //CREATE USER
    create: function(req, res, next) {
        //NEW USER, SET VALUES FROM REQUEST
        var user = new User();
        user.username = req.body.username;
        if (req.body.password) {
            user.password = req.body.password;
        }

        if (req.body.role) {
            req.body.roles = [req.body.role]
        };

        Role.find({ name: { $in: req.body.roles || [] } }).exec(
            function(err, roles) {
                if (err) return next(common.catchUnhandledErrors(err));
                for (var i = 0; i < roles.length; i++) {
                    user.roles.push(roles[i]._id);
                }

                user.save(function(err, user) {
                    if (err) return next(common.catchUnhandledErrors(err));

                    req.userObject = user;
                    next();
                });
            }
        );
    },
    changePassword: function(req, res, next) {
        User.findOne({ username: req.params.email, deleted: null })
            .exec(function(err, user) {
                if (err) return next(err);
                if (!user) return next(new Error('UserNotFound'));
                var j = new Date(user.rememberDate);
                j = j.setHours(j.getHours() + 12);
                j = new Date(j);
                if (user.rememberDate > j) {
                    return next(new Error('TokenExpired'));
                }
                if (user.remember != req.params.code) return next(new Error('TokenInvalid'));

                user.remember = '';

                if (req.body.password) {
                    user.password = req.body.password;
                } else if (req.body.pass) {
                    user.password = req.body.pass;
                };

                user.updated = new Date();

                user.save(function(err, user) {
                    if (err) return next(err);

                    req.userObject = user;

                    next();
                });
            });
    }
};