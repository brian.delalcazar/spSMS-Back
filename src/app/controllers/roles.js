var mongoose = require('mongoose'),
	router = require('express').Router(),
	common = require('../common'),
	User = mongoose.model('User'),
	Role = mongoose.model('Role'),
	Permission = mongoose.model('Permission');

module.exports = function(app) {
	app.use('/api/roles', router);
};

router.get('/', function(req, res, next) {
	Role.find().exec(
		function(err, roles) {
			if (err) return next(common.catchUnhandledErrors(err));
			res.json(roles);
		}
	);
});

router.post('/', function(req, res, next) {

	var role = new Role();
	role.name = req.body.name;

	Permission.find({ name: { $in: req.body.permissions || [] } }).exec(
		function(err, permissions) {
			if (err) return next(common.catchUnhandledErrors(err));
			role.permissions = permissions;

			role.save(function(err, role) {
				if (err) return next(common.catchUnhandledErrors(err));
				res.json(role);
			});
		}
	);
});

router.put('/:role/permissions', function(req, res, next) {
	Permission.find({ name: { $in: req.body.permissions || [] } }).exec(
		function(err, permissions) {
			if (err) return next(common.catchUnhandledErrors(err));

			for (var i = 0; i < permissions.length; i++) {
				req.role.permissions.push(permissions[i]);
			}
			req.role.save(function(err, role) {
				if (err) return next(common.catchUnhandledErrors(err));
				res.json(role);
			});
		}
	);
});

router.delete('/:role/permissions', function(req, res, next) {

	for (var i = 0; i < req.role.permissions.length; i++) {
		if (req.body.permissions.indexOf(req.role.permissions[i].name) != -1) {
			req.role.permissions.splice(i, 1);
		}
	}

	req.role.save(function(err, role) {
		if (err) return next(common.catchUnhandledErrors(err));
		res.json(role);
	});
});

router.param('role', function(req, res, next, role) {
	Role.findOne({ name: role }, function(err, role) {
		if (err) return next(common.catchUnhandledErrors(err));
		if (!role) return next({ message: 'RoleNotFound', status: 400 });

		req.role = role;

		next();
	});
});
