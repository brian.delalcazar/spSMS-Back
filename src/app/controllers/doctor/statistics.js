var mongoose = require('mongoose'),
	async = require('async'),
	path = require('path'),
	fs = require('fs'),
	router = require('express').Router(),
	common = require('../../common'),
	userMiddleware = require('../../common/middlewares').user,
	doctorMiddleware = require('../../common/middlewares').doctor,
	multiparty = require('connect-multiparty'),
	config = require('../../../config/config'),
	_ = require('lodash'),
	moment = require('moment'),
	Doctor = mongoose.model('Doctor'),
	Patient = mongoose.model('Patient'),
	User = mongoose.model('User'),
	Address = mongoose.model('Address'),
	Office = mongoose.model('Office'),
	Week = mongoose.model('Week'),
	Role = mongoose.model('Role'),
	LabAppointment = mongoose.model('LabAppointment'),
	Block = mongoose.model('Block'),
	Group = mongoose.model('Group'),
	Clinic = mongoose.model('Clinic'),
	Record = mongoose.model('Record'),
	Dictionary = mongoose.model('Dictionary'),
	Consultation = mongoose.model('Consultation'),
	Appointment = mongoose.model('Appointment'),
	MercadoPago = mongoose.model('MercadoPago'),
	Payment = mongoose.model('Payment'),
	Premium = mongoose.model('Premium'),
	PremiumPlan = mongoose.model('PremiumPlan'),
	Protocol = mongoose.model('Protocol'),
	nodeXls = require('node-xls'),
	ENV = require("../../../config/config.js"),
	stripe = require("stripe")(ENV.stripeKey),
	mailer = null;


module.exports = function(app) {
	app.use('/api/doctors', router);
	mailer = app.mailer;
};

router.get("/me/statistics/genre", function(req, res, next) {
	var doctor = new mongoose.mongo.ObjectId(req.query.doctor);
	Patient.aggregate()
		.match({ $and: [{ doctors: { $in: [doctor] } }, { genre: { $exists: true } }] })
		.group({ _id: "$genre", count: { $sum: 1 } })
		.exec(function(err, result) {
			console.log(result)
			if (err) return next(common.catchUnhandledErrors(err));
			res.json(result)
		})
})

router.get("/me/statistics/advanced", function(req, res, next) {
	var doctor = new mongoose.mongo.ObjectId(req.query.doctor);
	Dictionary.findOne({ name: req.query.statistic })
		.exec(function(err, statistic) {
			if (err) return next(common.catchUnhandledErrors(err));
			Dictionary.aggregate()
				.match({
					parent: statistic._id
				})
				.lookup({
					from: "records",
					localField: "_id",
					foreignField: "item",
					as: "records"
				})
				.match({
					records: { $ne: [] }
				})
				.match({
					'records.doctor': doctor
				})
				.project({
					label: 1,
					name: 1,
					records: {
						"$filter": {
							input: "$records",
							as: "record",
							cond: {
								"$eq": ["$$record.doctor", doctor]
							}
						}
					}
				})
				.exec(function(err, result) {
					if (err) return next(common.catchUnhandledErrors(err));
					res.json(result)
				})

		})
})

router.get("/me/statistics/personalized", doctorMiddleware.checkPremium, function(req, res, next) {
	if (typeof(req.query.items) == "string") {
		req.query.items = [req.query.items];
	}
	var doctor = new mongoose.mongo.ObjectId(req.query.doctor);
	var query = Record.aggregate()
	query.lookup({
		from: "patients",
		localField: "patient",
		foreignField: "_id",
		as: "patients"
	})
	query.unwind("$patients")
	query.lookup({
		from: "dictionaries",
		localField: "item",
		foreignField: "_id",
		as: "items"
	})
	query.unwind("$items")
	query.lookup({
		from: "dictionaries",
		localField: "items.parent",
		foreignField: "_id",
		as: "parents"
	})
	query.unwind("$parents")
	if (!!req.query.age) {
		req.query.age = JSON.parse(req.query.age)
		if (!!req.query.age.from && !!req.query.age.to && !!req.query.genre) {
			query.match({ $and: [{ "patients.age": { $gt: parseInt(req.query.age.from) } }, { "patients.age": { $lt: parseInt(req.query.age.to) } }, { "patients.genre": req.query.genre }, { "doctor": doctor }] })
		}
		if (!!req.query.age.from && !!req.query.age.to && !req.query.genre) {
			query.match({ $and: [{ "patients.age": { $gt: parseInt(req.query.age.from) } }, { "patients.age": { $lt: parseInt(req.query.age.to) } }, { "doctor": doctor }] })
		}
		if (!!req.query.age.from && !req.query.age.to && !req.query.genre) {
			query.match({ $and: [{ "patients.age": { $gt: parseInt(req.query.age.from) } }, { "doctor": doctor }] })

		}
		if (!!req.query.age.from && !req.query.age.to && !!req.query.genre) {
			query.match({ $and: [{ "patients.age": { $gt: parseInt(req.query.age.from) } }, { "patients.genre": req.query.genre }, { "doctor": doctor }] })

		}
		if (!req.query.age.from && !!req.query.age.to && !!req.query.genre) {
			query.match({ $and: [{ "patients.age": { $lt: parseInt(req.query.age.to) } }, { "patients.genre": req.query.genre }, { "doctor": doctor }] })
		}
		if (!req.query.age.from && !!req.query.age.to && !req.query.genre) {
			query.match({ $and: [{ "patients.age": { $lt: parseInt(req.query.age.to) } }, { "doctor": doctor }] })
		}
		if (!req.query.age.from && !req.query.age.to && !!req.query.genre) {
			query.match({ $and: [{ "patients.genre": req.query.genre }, { "doctor": doctor }] })
		}
		if (!req.query.age.from && !req.query.age.to && !req.query.genre) {
			query.match({ "doctor": doctor })
		}
	}
	if (req.query.items && req.query.items.length > 0) {
		query.match({ "items.name": { $in: req.query.items } })
	} else {
		query.match({
			$or: [
				{ "parents.name": { $in: ["motive", "procedures", "history", "diagnosis", "plan"] } },
				{ "parents.name": { $nin: ["physical", "habits"] } }
			]
		})
	}
	query.group({
		_id: '$patients.identity',
		firstName: { '$first': '$patients.firstName' },
		lastName: { '$first': '$patients.lastName' },
		genre: { '$first': '$patients.genre' },
		age: { '$first': '$patients.age' },
		records: { $push: { label: "$items.label", parent: "$parents.name" } }
	})
	query.exec(function(err, results) {
		if (err) return next(common.catchUnhandledErrors(err));
		res.json(results)
	})

})

router.get("/me/statistics/parents", function(req, res, next) {
	Dictionary.aggregate()
		.match({ parent: { $exists: false } })
		.project({
			name: 1,
			label: 1
		})

	.exec(function(err, results) {
		if (err) return next(common.catchUnhandledErrors(err));
		res.json(results)
	})

})
