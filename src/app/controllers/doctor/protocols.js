var mongoose = require('mongoose'),
	async = require('async'),
	router = require('express').Router(),
	common = require('../../common'),
	userMiddleware = require('../../common/middlewares').user,
	doctorMiddleware = require('../../common/middlewares').doctor,
	multiparty = require('connect-multiparty'),
	config = require('../../../config/config'),
	Doctor = mongoose.model('Doctor'),
	Client = mongoose.model('Client'),
	Notification = mongoose.model('Notification'),
	Patient = mongoose.model('Patient'),
	User = mongoose.model('User'),
	Protocol = mongoose.model('Protocol'),
	ENV = require("../../../config/config.js"),
	stripe = require("stripe")(ENV.stripeKey),
	mailer = null;


module.exports = function(app) {
	app.use('/api/doctors', router);
	mailer = app.mailer;
};

router.get("/protocols", doctorMiddleware.checkPremium, function(req, res, next) {
	var doctor = req.query.doctor;
	Protocol.find({ $or: [{ owner: doctor }, { doctors: { $in: [doctor] } }] })
		.populate('owner')
		.populate('doctors', "firstName lastName title")
		.populate({path:'patients.patient',select:"firstName lastName",model:"Patient"})
		.exec(function(err, protocols) {
			if (err) return next(common.catchUnhandledErrors(err));
			res.json(protocols);
		});
})

router.post("/protocols/new", doctorMiddleware.checkPremium, function(req, res, next) {
	if (req.user && req.user.doctor) {
		var doctor = req.user.doctor;
	}
	var protocol = new Protocol();
	common.setModelValues(req.body, protocol);

	protocol.save(function(err, protocol) {
		if (err) return next(common.catchUnhandledErrors(err));
		res.json(protocol)
	})
})

router.put("/protocols/add/doctor", doctorMiddleware.checkPremium, function(req, res, next) {
	if (req.user && req.user.doctor) {
		var doctor = req.user.doctor;
	}
	User.findOne({ username: req.body.doctor, doctor: { $exists: true } }).exec(function(err, user) {
		if (err) return next(common.catchUnhandledErrors(err));
		if (!user) return next(new Error("NoDoctorFound"));

		Protocol.findOne({ _id: req.body.protocol }, function(err, protocol) {
			if (err) return next(common.catchUnhandledErrors(err));
			if (!protocol) return next(new Error("NoProtocolFound"));
			if (protocol.doctors.indexOf(user.doctor) > -1) {
				return next(new Error("DoctorAlreadyInProtocol"));
			}
			protocol.doctors.push(user.doctor);

			protocol.save(function(err, protocol) {
				if (err) return next(common.catchUnhandledErrors(err));
				var notiData = {};
				notiData = protocol;
				if (!!doctor) {
					var mess = doctor.firstName + " " + doctor.lastName + " te ha añadido a su protocolo"
				} else {
					var mess = "Te han añadido a un protocolo"
				}
				var noti = {
				    owner: user.doctor,
				    type: "newProtocol",
				    created: new Date(),
				    data: {
				        protocol: notiData
				    },
				    title: "Nuevo Protocolo",
				    message: mess
				}
				var notification = new Notification(noti)
				notification.save(function(err, noti){})
				Client.findOne({clientId:user.doctor}, function (err, client) {
				    if (!!client.socket) {
				        req.io.to(client.socket).emit('notificationReload')
				    }
					res.json(protocol)
				})
			})
		})
	})

})

router.put("/protocols/add/patients", doctorMiddleware.checkPremium, function(req, res, next) {
	if (req.user && req.user.doctor) {
		var doctor = req.user.doctor;
	}
	Patient.findOne({identity:req.body.patient}, function(err, patient){
		if (err) return next(common.catchUnhandledErrors(err));
		if (!patient) return next(new Error("NoPatientFound"));
		Protocol.findOne({ _id: req.body.protocol }, function(err, protocol) {
			if (err) return next(common.catchUnhandledErrors(err));
			if (!protocol) return next(new Error("NoProtocolFound"));
			for (var i = 0; i < protocol.patients.length; i++) {
				console.log("PATIENT IN", protocol.patients[i].patient , patient._id)
				if (protocol.patients[i].patient.toString() == patient._id.toString()){

					return next(new Error("PatientAlredyInProtocol"));
				}
			}
			var obj = {
				patient:patient._id,
				doctor:req.body.doctor,
				items: req.body.items
			}
			protocol.patients.push(obj);
			protocol.save(function(err, protocol) {
				if (err) return next(common.catchUnhandledErrors(err));
				res.json(protocol)
			})
		})
	})
})

router.put("/protocols/remove", doctorMiddleware.checkPremium, function(req, res, next) {
	if (req.user && req.user.doctor) {
		var doctor = req.user.doctor;
	}
	Protocol.findOne({ _id: req.body.protocol }, function(err, protocol) {
		if (err) return next(common.catchUnhandledErrors(err));
		if (!protocol) return next(new Error("NoProtocolFound"));

		if (protocol.owner == req.body.doctor) {
			protocol.remove(function() {
				res.send(200)
			})
		} else {
			next(new Error("NoProtocolOwner"))
		};
	})
})

router.put("/protocols/remove/doctors", doctorMiddleware.checkPremium, function(req, res, next) {
	if (req.user && req.user.doctor) {
		var doctor = req.user.doctor;
	}
	Protocol.findOne({ _id: req.body.protocol }, function(err, protocol) {
		if (err) return next(common.catchUnhandledErrors(err));
		if (!protocol) return next(new Error("NoProtocolFound"));
		var docIndex = protocol.doctors.indexOf(req.body.doctor);
		if (protocol.owner == req.body.owner) {
			if (docIndex == -1) {
				next(new Error("NoDoctorFound"))
			} else {
				protocol.doctors.splice(docIndex, 1);
				protocol.save(function(err, protocol) {
					if (err) return next(common.catchUnhandledErrors(err));
					res.json(protocol)
				})
			}
		} else {
			next(new Error("NoProtocolOwner"))
		};

	})
})

router.put("/protocols/remove/patients", doctorMiddleware.checkPremium, function(req, res, next) {
	if (req.user && req.user.doctor) {
		var doctor = req.user.doctor;
	}
	Protocol.findOne({ _id: req.body.protocol }, function(err, protocol) {
		if (err) return next(common.catchUnhandledErrors(err));
		if (!protocol) return next(new Error("NoProtocolFound"));

		if (!protocol.patients[req.body.index]) {
			return next(new Error("WrongIndex"));			
		}
		console.log(protocol.patients[req.body.index])
		if (protocol.patients[req.body.index].doctor != req.body.doctor){
			return next(new Error("NoPatientOwner"));			
		}
		if (protocol.patients[req.body.index].patient != req.body.patient){
			return next(new Error("WrongPatient"));			
		}

		protocol.patients.splice(req.body.index, 1);
		protocol.save(function(err, protocol) {
			if (err) return next(common.catchUnhandledErrors(err));
			res.json(protocol)
		})
	})
})

function patientIndexProtocol(docId, patId, arr) {
	for (var i = 0; i < arr.length; i++) {
		if (arr[i].doctor == docId && arr[i].patient == patId) {
			return i
		}
	}
	return -1
}
