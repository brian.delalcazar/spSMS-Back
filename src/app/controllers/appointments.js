var mongoose = require('mongoose'),
    async = require('async'),
    router = require('express').Router(),
    common = require('../common'),
    appointmentMiddleware = require('../common/middlewares').appointment,
    Doctor = mongoose.model('Doctor'),
    Week = mongoose.model('Week'),
    config = require('../../config/config'),
    Block = mongoose.model('Block'),
    Notification = mongoose.model('Notification'),
    Appointment = mongoose.model('Appointment'),
    Client = mongoose.model('Client'),
    User = mongoose.model('User'),
    Patient = mongoose.model('Patient'),
    mailer = null,
    cronJob = require('cron').CronJob,
    moment = require('moment'),
    pug = require('pug'),
    AWS = require("aws-sdk");

var ses = new AWS.SES({
    accessKeyId: "AKIAILQGFDBGE3QMZBMQ",
    secretAccessKey: "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",
    endpoint: 'email.us-west-2.amazonaws.com',
    region: "us-west-2"
})


module.exports = function(app) {
    app.use('/api/appointments', router);
};

router.get('/:doctor', function(req, res, next) {
    if (req.query.page && isNaN(req.query.page)) return next(new Error('PageInvalid'));
    if (!req.doctor.week) return next(new Error('NoWeekFound'));

    async.parallel({
        week: function(callback) {
            Week.findOneAndPopulateAllFields(req.doctor.week, function(err, week) {
                if (err) return callback(err);
                callback(null, week);
            });
        },
        appointments: function(callback) {
            var weekDays = [];

            var appointments = {
                sunday: [],
                monday: [],
                tuesday: [],
                wednesday: [],
                thursday: [],
                friday: [],
                saturday: []
            };
            var days = ['sunday', 'monday', 'tuesday', 'wednesday', 'thursday', 'friday', 'saturday'];

            appointments.days = {};
            for (var day = 0; day < 7; day++) {
                var string = moment().utc().day(day + ((req.query.page | 0) * 7)).format('YYYY-MM-DD');
                var date = moment(string).utc().toDate();
                appointments.days[days[day]] = string;
                weekDays.push(date);
            }

            Appointment.find({
                date: {
                    $in: weekDays
                },
                doctor: req.doctor._id
            }, function(err, appointmentsModel) {
                if (err) return callback(err);

                appointmentsModel.forEach(function(appointment) {
                    appointments[days[moment(appointment.date).utc().weekday()]].push(appointment);
                });

                callback(null, appointments);
            });
        }
    }, function(err, results) {
        if (err) return next(err);

        results.appointments.week = results.week;

        res.json(results.appointments);
    });


});

router.post('/:doctor', appointmentMiddleware.check, function(req, res, next) {
    if (!req.body.block) return next(new Error('BlockRequired'));
    if (!req.body.date) return next(new Error('DateRequired'));
    if (!req.body.patient) return next(new Error('PatientRequired'));
    if (!req.body.patient.identity) return next(new Error('IdentityRequired'));
    var dateString = moment(req.body.date).utc().format('YYYY-MM-DD'),
        videocall = false,
        fullDate = {},
        over12 = false,
        realHour = 0,
        momHour = moment().hour(),
        overDate = false,
        todayDate = moment().format("YYYY-MM-DD")
    isDateInvalid = false;
    if (req.body.office.name.toLowerCase() == 'videollamada') {
        videocall = true;
        fullDate = getTime(req.body.begin, req.body.date);
        var minus30 = {};
        if (fullDate.mins == 0) {
            minus30.min = 30;
            minus30.hour = fullDate.hours - 1;
        } else {
            minus30.min = 0;
            minus30.hour = fullDate.hours;
        };
        fullDate.before = minus30;
    };

    if (req.body.begin[1] == ":") {
        realHour = parseInt(req.body.begin[0])
    } else {
        realHour = parseInt((req.body.begin[0] + req.body.begin[1]))
    };
    if (req.body.begin[5] == "p") {
        over12 = true
    };
    if (over12 && realHour != 12) {
        realHour = realHour + 12
    };

    if (dateString > todayDate) {
        isDateInvalid = false
    }
    if (dateString < todayDate) {
        isDateInvalid = true
    }
    if (dateString == todayDate) {
        if (realHour > momHour) {
            isDateInvalid = false
        } else {
            isDateInvalid = true
        };
    }

    if (isDateInvalid) {
        return next(new Error('DateInvalid'));
    }

    async.auto({
        week: function(callback) {
            var days = ['sunday', 'monday', 'tuesday', 'wednesday', 'thursday', 'friday', 'saturday'];
            var query = {};
            query[days[moment(dateString).utc().day()]] = req.body.block;
            query._id = req.doctor.week;
            Week.findOne(query, function(err, week) {
                if (err) return callback(err);
                if (!week) return callback(new Error('BlockInvalid'));

                callback(null, week);
            });
        },
        patient: ['week', function(callback) {
            Patient.findOne({
                    identity: req.body.patient.identity
                })
                .exec(function(err, patient) {
                    if (err) return callback(err);
                    if (!patient) {
                        patient = new Patient(req.body.patient);
                        patient.status = "PREREGISTERED";
                    }
                    patient.save(function(err, patient) {
                        if (err) return callback(err);
                        callback(null, patient);
                    });
                })
        }],
        appointment: ['week', function(callback) {

            Appointment.findOne({
                block: req.body.block,
                date: moment(dateString).utc().toDate(),
                doctor: req.doctor._id,
                status: {
                    $in: ['PENDING', 'DONE']
                }
            }).exec(function(err, appointment) {
                if (err) return callback(err);
                if (appointment) return callback(new Error('BlockAlreadyTaken'));

                appointment = new Appointment();
                appointment.block = req.body.block;
                appointment.doctor = req.doctor._id;
                appointment.date = moment(dateString).utc().toDate();

                callback(null, appointment);
            });
        }],
        storedPatient: ['patient', 'appointment', function(callback, results) {
            results.patient.doctors.push(req.doctor._id);
            results.patient.appointments.push(results.appointment._id);
            results.patient.save(function(err, patient) {
                if (err) return callback(err);
                callback(null, patient);
            });
        }],
        storedAppointment: ['patient', 'appointment', function(callback, results) {
            results.appointment.patient = results.patient._id;
            results.appointment.doctor = req.doctor._id;
            results.appointment.save(function(err, appointment) {
                if (err) return callback(err);
                callback(null, appointment);
            });

        }],
        storedDoctor: ['patient', 'appointment', function(callback, results) {
            req.doctor.patients.push(results.patient._id);
            req.doctor.appointments.push(results.appointment._id);
            req.doctor.save(function(err, doctor) {
                if (err) return callback(err);
                callback(null, doctor);
            });
        }]
    }, function(err, results) {
        if (err) return next(err);
        Doctor.populate(req.doctor, { path: "user", model: "User" }, function(err, doctor) {
            var dateString = moment(req.body.date).utc().format('DD-MM-YYYY')
            if (req.body.begin) {
                dateString = dateString + " a la hora " + req.body.begin
            }
            var options = {
                frontendUrl: config['app'].frontendPath,
                backendUrl: config['app'].publicPath,
                replyTo: req.body.patient.email,
                subject: 'Nueva Cita',
                name: req.body.patient.lastName + ", " + req.body.patient.firstName,
                date: dateString
            }
            var html = pug.renderFile("app/views/newAppointment.pug", options);
            var params = {
                Destination: {
                    ToAddresses: [doctor.user.username]
                },
                Message: {
                    Body: {
                        Html: {
                            Data: html
                        }
                    },
                    Subject: {
                        Data: 'Nueva Cita'
                    }
                },
                Source: "g.escalona@siplik.com"
            }
            ses.sendEmail(params, function(err, res) {});
            var options = {
                frontendUrl: config['app'].frontendPath,
                backendUrl: config['app'].publicPath,
                replyTo: req.body.patient.email,
                subject: 'Nueva Cita',
                name: "El/La Doctor(a) " + doctor.fullNameLA,
                date: dateString
            }
            var html = pug.renderFile("app/views/newAppointment.pug", options);
            var params = {
                Destination: {
                    ToAddresses: [req.body.patient.email]
                },
                Message: {
                    Body: {
                        Html: {
                            Data: html
                        }
                    },
                    Subject: {
                        Data: 'Nueva Cita'
                    }
                },
                Source: "g.escalona@siplik.com"
            }
            ses.sendEmail(params, function(err, res) {});
            if (videocall) {
                var emailDateBefore = new Date(fullDate.year, fullDate.month - 1, fullDate.day, fullDate.before.hour, fullDate.before.min, 0);
                var emailDate = new Date(fullDate.year, fullDate.month - 1, fullDate.day, fullDate.hours, fullDate.mins, 0);
                var job = new cronJob(emailDateBefore, function() {
                        var options = {
                            frontendUrl: config['app'].frontendPath,
                            backendUrl: config['app'].publicPath,
                            replyTo: req.body.patient.email,
                            subject: 'Recordatorio Cita',
                            name: doctor.fullNameLA
                        }
                        var html = pug.renderFile("app/views/appoint_before.pug", options);

                        var params = {
                            Destination: {
                                ToAddresses: [req.body.patient.email]
                            },
                            Message: {
                                Body: {
                                    Html: {
                                        Data: html
                                    }
                                },
                                Subject: {
                                    Data: 'Recordatorio Cita'
                                }
                            },
                            Source: "g.escalona@siplik.com"
                        }
                        ses.sendEmail(params, function(err, res) {});

                        var options = {
                            frontendUrl: config['app'].frontendPath,
                            backendUrl: config['app'].publicPath,
                            replyTo: doctor.user.username,
                            subject: 'Recordatorio Cita',
                            name: req.body.patient.firstName + req.body.patient.lastName,
                        }
                        var html = pug.renderFile("app/views/appoint_before.pug", options);

                        var params = {
                            Destination: {
                                ToAddresses: [doctor.user.username]
                            },
                            Message: {
                                Body: {
                                    Html: {
                                        Data: html
                                    }
                                },
                                Subject: {
                                    Data: 'Recordatorio Cita'
                                }
                            },
                            Source: "g.escalona@siplik.com"
                        }
                        ses.sendEmail(params, function(err, res) {});
                    }, function() {
                        console.log('sended')
                    },
                    true,
                    req.body.timezone
                );

                var job2 = new cronJob(emailDate, function() {
                        var options = {
                            frontendUrl: config['app'].frontendPath,
                            backendUrl: config['app'].publicPath,
                            replyTo: req.body.patient.email,
                            subject: 'Recordatorio Cita',
                            name: doctor.fullNameLA
                        }
                        var html = pug.renderFile("app/views/appoint_after.pug", options);

                        var params = {
                            Destination: {
                                ToAddresses: [req.body.patient.email]
                            },
                            Message: {
                                Body: {
                                    Html: {
                                        Data: html
                                    }
                                },
                                Subject: {
                                    Data: 'Recordatorio Cita'
                                }
                            },
                            Source: "g.escalona@siplik.com"
                        }
                        ses.sendEmail(params, function(err, res) {});

                        var options = {
                            frontendUrl: config['app'].frontendPath,
                            backendUrl: config['app'].publicPath,
                            replyTo: doctor.user.username,
                            subject: 'Recordatorio Cita',
                            name: req.body.patient.firstName + req.body.patient.lastName,
                        }
                        var html = pug.renderFile("app/views/appoint_after.pug", options);

                        var params = {
                            Destination: {
                                ToAddresses: [doctor.user.username]
                            },
                            Message: {
                                Body: {
                                    Html: {
                                        Data: html
                                    }
                                },
                                Subject: {
                                    Data: 'Recordatorio Cita'
                                }
                            },
                            Source: "g.escalona@siplik.com"
                        }
                        ses.sendEmail(params, function(err, res) {});
                    }, function() {},
                    true,
                    req.body.timezone
                );

            };
            var type = videocall ? "newVideocall" : "newAppointment",
                title = videocall ? "Nueva Videollamada" : "Nueva Cita",
                message = videocall ? "Tienes una nueva videollamada con " + req.body.patient.firstName + " " + req.body.patient.lastName : "Tienes una nueva cita con " + req.body.patient.firstName + " " + req.body.patient.lastName
            var notiAppointment = {};
            notiAppointment.patient = req.body.patient;
            notiAppointment.block = {};
            notiAppointment.block.begin = req.body.begin;
            notiAppointment.block.end = req.body.end;
            var noti = {
                owner:req.doctor._id,
                type: type,
                created: new Date(),
                data: {
                    appointment: notiAppointment
                },
                title: title,
                message: message
            }
            var notification = new Notification(noti)
            notification.save(function(err, noti){})
            Client.findOne({clientId:req.doctor._id}, function (err, client) {
                if (!!client && !!client.socket) {
                    req.io.to(client.socket).emit('notificationReload')
                }
                res.json(results.storedAppointment);
            })

        })
    });


});

router.delete('/delete/:appointment', function(req, res, next) {
    console.log("here")
    var AppointmentDate, Hour;
    Appointment.findOne({ _id: req.params.appointment })
        .populate("doctor")
        .populate("patient")
        .populate("block")
        .exec(function(err, appointment) {

            if (err) return next(common.catchUnhandledErrors(err));

            Hour = appointment.block.begin
            AppointmentDate = new Date(appointment.date).toISOString()

            var year = AppointmentDate.split("T")[0].split("-")
            var month = parseInt(year[1]);
            var day = parseInt(year[2]);
            year = parseInt(year[0]);
            var date = new Date();
            if (date.getFullYear() >= year &&
                date.getMonth() + 1 >= month &&
                date.getDate() >= day) {
                return next(new Error("PastAppointmentDate"))
            }
            var doctor = appointment.doctor;
            var patient = appointment.patient;
            var docIndex = doctor.appointments.indexOf(appointment._id);
            var patIndex = patient.appointments.indexOf(appointment._id);
            if (docIndex > -1) {
                doctor.appointments.splice(docIndex, 1);
            }
            if (patIndex > -1) {
                patient.appointments.splice(patIndex, 1);
            }
            async.auto({
                doctor: function(callback) {
                    doctor.save(function(err, doctor) {
                        if (err) return callback(err);
                        callback(err, doctor)
                    })
                },
                patient: function(callback) {
                    patient.save(function(err, patient) {
                        if (err) return callback(err);
                        callback(err, patient)
                    })
                },
                appointment: function(callback) {
                    appointment.remove(function(err) {
                        callback(err)
                    })
                }
            }, function(err, results) {
                var populateOptions = {
                    path: 'doctor.user',
                    model: 'User'
                };
                Appointment.populate(appointment, populateOptions, function(err, appointment) {
                    var year = AppointmentDate.split("T")[0].split("-")
                    var month = parseInt(year[1]);
                    var day = parseInt(year[2]);
                    year = parseInt(year[0]);
                    var options = {
                        name: results.patient.fullName,
                        date: day + "/" + month + "/" + year + " " + Hour,
                        frontendUrl: config['app'].frontendPath,
                        backendUrl: config['app'].publicPath,
                    }
                    var html = pug.renderFile("app/views/deleteAppointment.pug", options);

                    var params = {
                        Destination: {
                            ToAddresses: [appointment.doctor.user.username]
                        },
                        Message: {
                            Body: {
                                Html: {
                                    Data: html
                                }
                            },
                            Subject: {
                                Data: 'Cita eliminada'
                            }
                        },
                        Source: "g.escalona@siplik.com"
                    }
                    ses.sendEmail(params, function(err, res) {});
                    res.json(results)
                })
            })
        })
})

router.param('doctor', function(req, res, next, value) {
    Doctor.findOne({
            _id: value
        })
        .exec(function(err, doctor) {
            if (err) return next(common.catchUnhandledErrors(err));
            if (!doctor) return next({
                message: 'DoctorNotFound',
                status: 400
            });

            req.doctor = doctor;
            next();
        });
});

function getTime(time, date) {
    var dots = time.indexOf(":");
    var hours = '';
    var mins = '';
    var amPm = '';
    var fullDate = {};
    var dates = date.split('-');
    fullDate.day = dates[2]
    fullDate.month = dates[1]
    fullDate.year = dates[0]
    if (dots == 2) {
        hours = hours + dots[0] + dots[1];
        mins = mins + dots[3] + dots[4];
        hours = parseInt(hours);
        mins = parseInt(mins);

    } else if (dots == 1) {
        hours = hours + '0' + dots[0];
        mins = mins + dots[2] + dots[3];
        hours = parseInt(hours);
        mins = parseInt(mins);
    };

    switch (dots[6]) {
        case 'a':
            amPm = "am"
            break;
        case 'p':
            amPm = "pm"
            break;
        case 'm':
            amPm = "pm"
            break;
    }

    if (hours < 12 && amPm == "pm") {
        hours = hours + 12;
    };
    fullDate.hours = hours;
    fullDate.mins = mins;
    fullDate.amPm = amPm;
    return fullDate
}
