var mongoose = require('mongoose'),
    router = require('express').Router(),
    Address = mongoose.model('Address'),
    countries = require('../scripts/resources/country.json','utf8'),
    stateTownParish = require('../scripts/resources/addressVE.json','utf8');
    
module.exports = function (app) {
    app.use('/api/address', router);
};

router.get('/countries', function(req, res, next) {
	res.json(countries);
});
router.get('/:address', function (req, res, next) {
    Address.findOne({_id:req.params.address}, function(err, address) {
    	if (err) return next(err)
    	res.json(address)
    })
});

router.get('/stateTownParish', function(req, res, next) {
	res.json(stateTownParish);
});

router.get('/towns/:state', function(req, res, next) {
	var towns = [];
	for (var i = 0; i < stateTownParish.length; i++) {
		if (stateTownParish[i].estado == req.params.state) {
			towns = stateTownParish[i].municipios
			res.json(towns);
		};
	};

});

router.get('/parishes/:state/:town', function(req, res, next) {
	var parishes = [];
	for (var i = 0; i < stateTownParish.length; i++) {
		if (stateTownParish[i].estado == req.params.state) {
			for (var j = 0; j < stateTownParish[i].municipios.length; j++) {
				if (stateTownParish[i].municipios[j].municipio == req.params.town) {
					parishes = stateTownParish[i].municipios[j].parroquias
				}
			};
		};
	};
	res.json(parishes);
});
