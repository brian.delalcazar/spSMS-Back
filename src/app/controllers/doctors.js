var mongoose = require('mongoose'),
	async = require('async'),
	path = require('path'),
	fs = require('fs'),
	router = require('express').Router(),
	common = require('../common'),
	userMiddleware = require('../common/middlewares').user,
	multiparty = require('connect-multiparty'),
	config = require('../../config/config'),
	_ = require('lodash'),
	moment = require('moment'),
	Doctor = mongoose.model('Doctor'),
	Patient = mongoose.model('Patient'),
	User = mongoose.model('User'),
	Address = mongoose.model('Address'),
	Office = mongoose.model('Office'),
	Week = mongoose.model('Week'),
	Role = mongoose.model('Role'),
	LabAppointment = mongoose.model('LabAppointment'),
	Block = mongoose.model('Block'),
	Notification = mongoose.model('Notification'),
	Group = mongoose.model('Group'),
	Clinic = mongoose.model('Clinic'),
	Consultation = mongoose.model('Consultation'),
	Appointment = mongoose.model('Appointment'),
	MercadoPago = mongoose.model('MercadoPago'),
	PayPal = mongoose.model('PayPal'),
	Payment = mongoose.model('Payment'),
	Premium = mongoose.model('Premium'),
	PremiumPlan = mongoose.model('PremiumPlan'),
	nodeXls = require('node-xls'),
	ENV = require("../../config/config.js"),
	stripe = require("stripe")(ENV.stripeKey),
	multer = require('multer'),
	upload = multer({ dest: '' }),
	mailer = null,
	pug = require('pug'),
	AWS = require("aws-sdk");

var ses = new AWS.SES({
	accessKeyId: "AKIAILQGFDBGE3QMZBMQ",
	secretAccessKey: "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",
	endpoint: 'email.us-west-2.amazonaws.com',
	region: "us-west-2"
})

module.exports = function(app) {
	app.use('/api/doctors', router);
	mailer = app.mailer;
};
router.get('/', function(req, res, next) {
	var query = User.find({ doctor: { $exists: true, $ne: null } });

	if (!req.query.all) {
		query.where({ 'active.status': true });
	}

	query.exec(function(err, users) {
		var query = { _id: { $in: _.pluck(users, 'doctor') } };
		if (req.query.premium && JSON.parse(req.query.premium)) {
			query.isPremium = true
		}
		if (req.query && (req.query.doctor || req.query.title) && req.query.country) {
			if (req.query.doctor) {
				queryDoc = new RegExp(req.query.doctor);
			} else {
				queryDoc = false;
			};
			if (req.query.title) {
				queryTitle = new RegExp(req.query.title);
			} else {
				queryTitle = false
			};
			if (req.query.country) {
				queryCountry = req.query.country;
			} else {
				queryCountry = false
			};
			var query;
			if (!!queryDoc) {
				query = Doctor.find({ _id: { $in: _.pluck(users, 'doctor') }, $or: [{ firstName: new RegExp(req.query.doctor, 'i') }, { lastName: new RegExp(req.query.doctor, 'i') }] })
			};
			if (!!queryTitle) {
				query = Doctor.find({ _id: { $in: _.pluck(users, 'doctor') }, title: new RegExp(req.query.title, 'i') })

			};
			query
				.populate({
					path: "personalAddress",
					match: {
						country: req.query.country
					}
				})
				.exec(function(err, doctors) {
					var docs = doctors.filter(function(doc) {
						delete doc.user
						delete doc.appointments
						delete doc.consultations
						delete doc.patients
						return doc.personalAddress
					});
					res.json(docs)
				});
			return
		};

		if (req.query && req.query.country) {
			Doctor.find(query)
				.populate('user addresses offices personalAddress')
				.exec(function(err, doctors) {
					var array = [];
					for (var i = 0; i < doctors.length; i++) {
						if (doctors[i].personalAddress && doctors[i].personalAddress.country == req.query.country) {
							array.push(doctors[i]);
						};
					};
					res.json(array)
				});
			return
		};

		if (req.query && req.query.doctor) {
			Doctor.find({ _id: { $in: _.pluck(users, 'doctor') }, $or: [{ firstName: new RegExp(req.query.doctor, 'i') }, { lastName: new RegExp(req.query.doctor, 'i') }] })
				.exec(function(err, doctors) {
					for (var i = 0; i < doctors.length; i++) {
						delete doctors[i].user
						delete doctors[i].appointments
						delete doctors[i].consultations
						delete doctors[i].patients
						delete doctors[i].personalAddress
					};
					res.json(doctors)
				});
			return
		};

		if (req.query && req.query.title) {
			Doctor.find({ _id: { $in: _.pluck(users, 'doctor') }, title: new RegExp(req.query.title, 'i') })
				.exec(function(err, doctors) {
					for (var i = 0; i < doctors.length; i++) {
						delete doctors[i].user
						delete doctors[i].appointments
						delete doctors[i].consultations
						delete doctors[i].patients
						delete doctors[i].personalAddress
					};
					res.json(doctors)
				});
			return
		};

		Doctor.find(query)
			.populate('user addresses offices personalAddress')
			.exec(function(err, doctors) {
				if (err) return next(common.catchUnhandledErrors(err));
				res.json(doctors);
			});
	});
});

router.get('/:doctor', function(req, res, next) {
	res.json(req.doctor)
});

router.get('/:doctor/clinics', function(req, res, next) {
	req.doctor.getClinics(function(err, clinics) {
		res.json(clinics);
	});
});

router.get('/me/appointments', function(req, res, next) {

	if (req.query.page && isNaN(req.query.page)) return next(new Error('PageInvalid'));
	if (!req.user.doctor.week) return next(new Error('NoWeekFound'));

	async.parallel({
		week: function(callback) {
			Week.findOneAndPopulateAllFields(req.user.doctor.week, function(err, week) {
				if (err) return callback(err);
				callback(null, week);
			});
		},
		appointments: function(callback) {
			var weekDays = [];

			var appointments = { sunday: {}, monday: {}, tuesday: {}, wednesday: {}, thursday: {}, friday: {}, saturday: {} };
			var days = ['sunday', 'monday', 'tuesday', 'wednesday', 'thursday', 'friday', 'saturday'];

			appointments.days = {};
			for (var day = 0; day < 7; day++) {
				var string = moment().utc().day(day + ((req.query.page | 0) * 7)).format('YYYY-MM-DD');
				var date = moment(string).utc().toDate();
				appointments.days[days[day]] = string;
				weekDays.push(date);
			}

			Appointment.find({ date: { $in: weekDays }, doctor: req.user.doctor._id })
				.populate('patient')
				.exec(function(err, appointmentsModel) {
					if (err) return callback(err);

					appointmentsModel.forEach(function(appointment) {
						appointments[days[moment(appointment.date).utc().weekday()]][appointment.block] = appointment;
					});

					callback(null, appointments);
				});
		}
	}, function(err, results) {
		if (err) return next(err);

		results.appointments.week = results.week;

		res.json(results.appointments);
	});

});

router.get('/:doctor/lab-appointments', function(req, res, next) {
	LabAppointment
		.find({
			owner: req.doctor.user,
			done: true
		})
		.populate('service patient')
		.exec(function(err, appointments) {
			res.json(appointments);
		});
});

router.put('/me', userMiddleware.validateNames, function(req, res, next) {
	if (!!req.user.doctor) {
		delete req.body.addresses;
		delete req.body.personalAddress;
		delete req.body.offices;
		delete req.body.user;
		delete req.body.patients;
		common.setModelValues(req.body, req.user.doctor);

		req.user.doctor.save(function(err, doctor) {
			if (err) return next(common.catchUnhandledErrors(err));

			res.json(doctor);
		});
	} else {
		return next({ message: 'DoctorNotExists', status: 400 });
	}
});


router.post('/me/photo', function(req, res, next) {
	req.pipe(req.busboy);
	req.busboy.on('file', function(fieldname, file, filename) {
		var fstream = fs.createWriteStream("uploads/" + req.user.doctor._id + "avatar.pdf")
		file.pipe(fstream)
		req.user.doctor.photo.path = 'uploads/' + req.user.doctor._id + "avatar.pdf";
		req.user.doctor.save(function(err, savedDoctor) {
			if (err) return callback(err);

			savedDoctor = savedDoctor.toObject();

			res.json(savedDoctor);
		});
	});
});
router.post('/me/cv', function(req, res, next) {
	req.pipe(req.busboy);
	req.busboy.on('file', function(fieldname, file, filename) {
		var fstream = fs.createWriteStream("uploads/" + req.user.doctor._id + "cv.pdf")
		file.pipe(fstream)
		req.user.doctor.professional.path = 'uploads/' + req.user.doctor._id + "cv.pdf";
		req.user.doctor.save(function(err, savedDoctor) {
			if (err) return callback(err);

			savedDoctor = savedDoctor.toObject();

			res.json(savedDoctor);
		});
	});

});

router.get('/me/settings/weeks', function(req, res, next) {
	if (!req.user.doctor.week) {
		Group.findOne({ name: '20 min' }, function(err, group) {
			if (err) return next(err);
			if (!group) return next(new Error('NoGroupFound'));

			var week = new Week();
			req.user.doctor.week = week._id;
			week.doctor = req.user.doctor._id;
			week.group = group._id;

			week.active = true;
			async.parallel({
				week: function(callback) {
					week.save(function(err, week) {
						if (err) return callback(err);
						callback(null, week);
					});
				},
				doctor: function(callback) {
					req.user.doctor.save(function(err, doctor) {
						if (err) return callback(err);
						callback(null, doctor);
					})
				}
			}, function(err, results) {
				if (err) return next(err);

				results.week.populateAllFields(function(err, week) {
					res.json(week);
				});
			});

		});

	} else {
		Week.findOneAndPopulateAllFields(req.user.doctor.week, function(err, week) {
			res.json(week);
		});
	}

});

router.put('/me/settings/weeks/:week/:day', function(req, res, next) {
	var total = req.body.blocks.length,
		result = [];

	function saveAll() {
		var block = req.body.blocks.pop();
		if (!block.day) return next(new Error('DayRequired'));
		if (!block.block) return next(new Error('BlockRequired'));
		var office = {
			day: block.day,
			office: block.office,
			block: block.block
		}
		req.week.offices.push(office);
		req.week[block.day.code].push(block.block);

		req.week.save(function(err, week) {
			if (err) return next(err); //handle error

			if (--total) {
				saveAll()
			} else {
				week.populateAllFields(function(err, week) {
					res.json(week);
				});
			}
		})
	}
	saveAll()
});

router.delete('/me/settings/weeks/:week/:day/:block', function(req, res, next) {
	if (!req.params.day) return next(new Error('DayRequired'));
	var dayCode = req.params.day
	var blockId = req.block._id

	for (var i = 0; i < req.week.offices.length; i++) {
		if (req.week.offices[i].block == blockId && req.week.offices[i].day.code == dayCode) {
			req.week.offices.splice(i, 1);
			req.week.save(function(err, week) {
				if (err) return next(common.catchUnhandledErrors(err));
			})
		}
	}

	var condition = {};
	condition[req.day] = req.block._id;


	console.log(req.week)
	req.week.update({ $pull: condition }, function(err, affected, raw) {
		if (err) return callback(err);

		Week.findOneAndPopulateAllFields(req.week, function(err, week) {
			res.json(week);
		});
	});

});

router.get('/:doctor', function(req, res, next) {
	var doctor = req.doctor.toObject();

	delete doctor.user;
	delete doctor.patients;
	delete doctor.consultations;

	res.json(doctor);
});

router.post('/me/addresses', function(req, res, next) {
	var address = new Address(req.body);


	async.parallel({
		address: function(callback) {
			address.save(function(err, address) {
				if (err) return callback(address);
				callback(null, address);
			});
		},
		doctor: function(callback) {
			req.user.doctor.save(function(err, doctor) {
				if (err) return callback(doctor);
				callback(null, doctor);
			});
		}
	}, function(err, results) {
		if (err) return next(err);
		res.json(results.address);
	});
});

router.delete('/me/addresses/:address', function(req, res, next) {

	async.parallel({
		address: function(callback) {
			req.address.remove(function(err, address) {
				if (err) return callback(err);
				callback(null, address);
			});
		},
		doctor: function(callback) {
			req.user.doctor.update({
				$pull: { addresses: req.address._id }
			}, function(err, doctor) {
				if (err) return callback(err);
				callback(null, doctor);
			});
		}
	}, function(err, results) {
		if (err) return next(common.catchUnhandledErrors(err));
		res.json(results.address);
	});

});

router.post('/', userMiddleware.create, function(req, res, next) {
	var doctor = new Doctor();
	doctor.tutorial = true;
	if (req.body.country) {
		var address = new Address(req.body);
		doctor.personalAddress = address._id;
	}
	// var office = new Office();
	// office.name = "Videollamada";
	// office.doctor = doctor._id;
	// doctor.offices.push(office._id);

	common.setModelValues(req.body, doctor);

	doctor.user = req.userObject._id;
	req.userObject.doctor = doctor._id;

	async.parallel({
		user: function(callback) {
			req.userObject.save(function(err, user) {
				callback(err, user);
			});
		},
		doctor: function(callback) {
			doctor.save(function(err, doctor) {
				callback(err, doctor);
			});
		},
		// office: function(callback) {
		// 	office.save(function(err, office) {
		// 		callback(err, office);
		// 	});
		// },
		address: function(callback) {
			if (address) {
				address.save(function(err, address) {
					callback(err, address);
				});
			} else {
				callback(null);
			}
		}
	}, function(err, results) {
		if (err) {
			return next(common.catchUnhandledErrors(err));
		}

		results.doctor.populate('user addresses', function(err, doctor) {
			if (err) return next(common.catchUnhandledErrors(err));

			if (ENV.env == "staging" || ENV.env == "production") {
				var options = {
					name: results.doctor.fullName,
					frontendUrl: config['app'].frontendPath,
					backendUrl: config['app'].publicPath,
					url: config['app'].frontendPath + "/#/activate/" + results.doctor.id
				}
				var html = pug.renderFile("app/views/activate.pug", options);

				var params = {
					Destination: {
						ToAddresses: [results.user.username]
					},
					Message: {
						Body: {
							Html: {
								Data: html
							}
						},
						Subject: {
							Data: 'Activa tu cuenta en Siplik'
						}
					},
					Source: "g.escalona@siplik.com"
				}
				ses.sendEmail(params, function(err, res) {});

				var html = pug.renderFile("app/views/doctor/welcome.pug", options);

				var params = {
					Destination: {
						ToAddresses: [results.user.username]
					},
					Message: {
						Body: {
							Html: {
								Data: html
							}
						},
						Subject: {
							Data: 'Bienvenido a Siplik!!!'
						}
					},
					Source: "g.escalona@siplik.com"
				}
				ses.sendEmail(params, function(err, res) {});

				var options = {
					name: results.doctor.fullName,
					title: results.doctor.title,
					frontendUrl: config['app'].frontendPath,
					backendUrl: config['app'].publicPath
				}
				var html = pug.renderFile("app/views/doctor/new.pug", options);

				var params = {
					Destination: {
						ToAddresses: ["brian.delalcazar@gmail.com", "g.escalona@siplik.com", "icordoba@4geeks.co"]
					},
					Message: {
						Body: {
							Html: {
								Data: html
							}
						},
						Subject: {
							Data: 'Nuevo usuario registrado en Siplik'
						}
					},
					Source: "g.escalona@siplik.com"
				}
				ses.sendEmail(params, function(err, res) {});

			};
			var editCalendarNoti = {
			    owner: results.doctor._id,
			    type: "system",
			    created: new Date(),
			    data: {
			        state: 'user.settings.calendar'			    },
			    title: "Edita tu agenda de citas",
			    message: "Haz click aqui para ir a editar tu agenda de citas"
			}
			var notification = new Notification(editCalendarNoti)
			notification.save()

			var editMpNoti = {
			    owner: results.doctor._id,
			    type: "system",
			    created: new Date(),
			    data: {
			        state: 'user.settings.options'
			    },
			    title: "Edita tu boton de pago",
			    message: "Haz click aqui para ir a editar tu boton de pago"
			}
			var notification = new Notification(editMpNoti)
			notification.save()

			var editProfileNoti = {
			    owner: results.doctor._id,
			    type: "system",
			    created: new Date(),
			    data: {
			        state: 'user.profile'
			    },
			    title: "Edita tu perfil",
			    message: "Haz click aqui para ir a editar tu perfil"
			}
			var notification = new Notification(editProfileNoti)
			notification.save()
			
			res.json(results.doctor);

		});
	});
});

router.put('/:doctor/activate', function(req, res, next) {
	req.doctor.user.active.status = true;

	req.doctor.user.save(function(err, user) {
		if (err) return next(common.catchUnhandledErrors(err));

		var options = {
			name: req.doctor.fullName,
			backendUrl: config['app'].publicPath,
			frontendUrl: config['app'].frontendPath
		}
		var html = pug.renderFile("app/views/doctor/activated.pug", options);

		var params = {
			Destination: {
				ToAddresses: [req.doctor.user.username]
			},
			Message: {
				Body: {
					Html: {
						Data: html
					}
				},
				Subject: {
					Data: 'Cuenta aprobada'
				}
			},
			Source: "g.escalona@siplik.com"
		}
		ses.sendEmail(params, function(err, data) {
			if (err) return next(common.catchUnhandledErrors(err));
			res.json(req.doctor);
		});

	});
});

router.put('/:doctor/inactivate', function(req, res, next) {
	req.doctor.user.active.status = false;

	req.doctor.user.save(function(err, user) {
		if (err) return next(common.catchUnhandledErrors(err));

		res.json(req.doctor);
	});
});


router.param('address', function(req, res, next, value) {
	Address.findOne({ _id: value })
		.exec(function(err, address) {
			if (err) return next(common.catchUnhandledErrors(err));
			if (!address) return next({ message: 'AddressNotFound', status: 400 });

			req.address = address;

			next();
		});
});

router.param('doctor', function(req, res, next, value) {
	if (value === 'me') {
		exec(Doctor.findById(req.user.doctor));
	} else if (mongoose.Types.ObjectId.isValid(value)) {
		exec(Doctor.findById(value));
	} else {
		User.findOne({
			username: value,
			doctor: { $exists: true }
		}, function(err, user) {
			if (!user) return next(new Error('NotFound'));
			exec(Doctor.findById(user.doctor));
		});
	}

	function exec(query) {
		query
			.populate('user')
			.populate('patients')
			.populate('addresses')
			.populate('personalAddress')
			.populate('offices')
			.populate('mercadoPago')
			.populate('payPal')
			.exec(function(err, doctor) {
				if (err) return next(common.catchUnhandledErrors(err));
				if (!doctor) return next({ message: 'DoctorNotFound', status: 400 });

				req.doctor = doctor;

				next();
			});
	};
});


router.param('week', function(req, res, next, value) {
	Week.findOne({ _id: value })
		.exec(function(err, week) {
			if (err) return next(common.catchUnhandledErrors(err));
			if (!week) return next({ message: 'NoWeekFound', status: 400 });

			req.week = week;

			next();
		});
});

router.param('block', function(req, res, next, value) {
	Block.findOne({ _id: value })
		.exec(function(err, block) {
			if (err) return next(common.catchUnhandledErrors(err));
			if (!block) return next({ message: 'NoBlockFound', status: 400 });

			req.block = block;

			next();
		});
});

router.post('/me/offices', function(req, res, next) {
	var office = new Office();
	office.name = req.body.name
	if (req.body.address && req.body.address.country) {
		var address = new Address(req.body.address);
		office.address = address._id
	};
	office.doctor = req.user.doctor._id;

	async.auto({
		address: function(callback) {
			if (address) {
				address.save(function(err, address) {
					if (err) return callback(address);

					callback(err, address);
				});
			} else {
				callback(null);
			}
		},
		office: function(callback) {
			office.save(function(err, office) {
				if (err) return callback(office);

				callback(null, office);
			});
		},
		doctor: ['office', function(callback) {
			req.user.doctor.update({
				$push: { offices: office._id }
			}, function(err, doctor) {
				if (err) return callback(doctor);
				callback(null, doctor);
			});
		}]
	}, function(err, results) {
		if (err) return next(err);
		res.json(results.office);
	});
});

router.put('/me/offices/:office', function(req, res, next) {
	if (req.office.name.toLowerCase() == "videollamada") {
		return next(new Error("No se puede editar este consultorio"))
	};
	common.setModelValues(req.body, req.office);
	common.setModelValues(req.body.address, req.address);

	if (req.body.address.state) {
		req.address.state = req.body.address.state
		req.address.town = req.body.address.town
		req.address.parish = req.body.address.parish
	};

	async.parallel({
		address: function(callback) {
			req.address.save(function(err, address) {
				callback(err, address)
			})
		},
		office: function(callback) {
			req.office.save(function(err, office) {
				callback(err, office)
			})
		}
	}, function(err, results) {
		if (err) return next(common.catchUnhandledErrors(err));
		res.json(req.office);
	});
});

router.put('/me/clinics/:office/:clinic', function(req, res, next) {
	if (req.office.name.toLowerCase() == "videollamada") {
		return next(new Error("NameInvalid"))
	};
	common.setModelValues(req.body, req.office);
	var finded = false
	for (var i = 0; i < req.clinic.activeCodes.length; i++) {
		if (req.clinic.activeCodes[i].code == req.body.code && req.clinic.activeCodes[i].email == req.body.email) {
			finded = true;
			req.clinic.activeCodes.splice(i, 1);
			break
		};
	};
	if (finded) {
		async.parallel({
			clinic: function(callback) {
				req.clinic.save(function(err, clinic) {
					callback(err, clinic)
				})
			},
			office: function(callback) {
				req.office.save(function(err, office) {
					callback(err, office)
				})
			}
		}, function(err, results) {
			if (err) return next(common.catchUnhandledErrors(err));
			res.json(req.results);
		});
	} else {
		return next(new Error("CodeClinicInvalid"))
	};

});

router.post('/me/clinics/:clinic', function(req, res, next) {
	if (req.body.name.toLowerCase() == "videollamada") {
		return next(new Error("NameInvalid"))
	};
	var office = new Office();
	office.name = req.body.name
	if (req.body.address && req.body.address.country) {
		var address = new Address(req.body.address);
		office.address = address._id
	};
	office.doctor = req.user.doctor._id;
	office.clinic = req.body.clinic;

	var finded = false
	for (var i = 0; i < req.clinic.activeCodes.length; i++) {
		if (req.clinic.activeCodes[i].code == req.body.code && req.clinic.activeCodes[i].email == req.body.email) {
			finded = true;
			req.clinic.activeCodes.splice(i, 1);
			break
		};
	};
	if (finded) {
		async.auto({
			address: function(callback) {
				if (address) {
					address.save(function(err, address) {
						if (err) return callback(address);

						callback(err, address);
					});
				} else {
					callback(null);
				}
			},
			clinic: function(callback) {
				req.clinic.save(function(err, clinic) {
					callback(err, clinic)
				})
			},
			office: function(callback) {
				office.save(function(err, office) {
					if (err) return callback(office);

					callback(null, office);
				});
			},
			doctor: ['office', function(callback) {
				req.user.doctor.update({
					$push: { offices: office._id }
				}, function(err, doctor) {
					if (err) return callback(doctor);
					callback(null, doctor);
				});
			}]
		}, function(err, results) {
			if (err) return next(err);
			res.json(results.office);
		});
	} else {
		return next(new Error("CodeClinicInvalid"))
	};
});

router.param('clinic', function(req, res, next, value) {
	var query = Clinic.findById(value)
	query
		.exec(function(err, clinic) {
			if (err) return next(err);
			if (!clinic) return next({
				message: 'ClinicNotFound',
				status: 400
			})
			req.clinic = clinic;
			next();
		});
});

router.param('office', function(req, res, next, value) {
	Office.findById(value)
		.populate('address')
		.populate('clinic')
		.exec(function(err, office) {
			if (err) return next(common.catchUnhandledErrors(err));
			if (!office) return next({ message: 'OfficeNotFound', status: 400 });

			req.office = office;
			req.address = office.address
			next();
		});
});

router.delete('/me/offices/:office', function(req, res, next) {
	var office = req.office;
	async.auto({
		office: function(callback) {
			office.remove(function(err, office) {
				if (err) return callback(err);
				callback(null, office);
			})
		},
		address: function(callback) {
			req.office.address.remove(function(err, address) {
				if (err) return callback(err);
				callback(null, address);
			});
		},
		doctor: function(callback) {
			req.user.doctor.update({
				$pull: { offices: office._id }
			}, callback);
		}
	}, function(err, results) {
		if (err) return next(err);
		res.json(results);
	});
});

router.post('/me/personalAddress', function(req, res, next) {
	var personalAddress = new Address(req.body);

	req.user.doctor.personalAddress = personalAddress._id;

	async.parallel({
		personalAddress: function(callback) {
			personalAddress.save(function(err, personalAddress) {
				if (err) return callback(personalAddress);
				callback(null, personalAddress);
			});
		},
		doctor: function(callback) {
			req.user.doctor.save(function(err, doctor) {
				if (err) return callback(doctor);
				callback(null, doctor);
			});
		}
	}, function(err, results) {
		if (err) return next(err);
		res.json(results.personalAddress);
	});
});

router.put('/me/personalAddress', function(req, res, next) {
	Address.findOne({ _id: req.body._id })
		.exec(function(err, personalAddress) {
			if (err) return next(common.catchUnhandledErrors(err));
			if (!personalAddress) return next({ message: 'AddressNotFound', status: 400 });

			common.setModelValues(req.body, personalAddress)

			async.parallel({
				personalAddress: function(callback) {
					personalAddress.save(function(err, personalAddress) {
						if (err) return callback(personalAddress);
						callback(null, personalAddress);
					});
				}
			}, function(err, results) {
				if (err) return next(err);
				res.json(personalAddress);
			});

		});
});

router.get('/me/officesAll', function(req, res, next) {
	var doctor = req.user.doctor;
	doctor.populate('offices', function(err, doctor) {
		Address.populate(doctor.offices, { "path": "address" }, function(err, offices) {
			if (err) return next(common.catchUnhandledErrors(err));
			Clinic.populate(doctor.offices, { "path": "clinic" }, function(err, offices) {
				if (err) return next(common.catchUnhandledErrors(err));
				res.json(offices);
			})
		});

	});
});

router.get('/me/search/:search', function(req, res, next) {
	User.findOne({ username: req.params.search }, function(err, user) {
		if (err) return next(common.catchUnhandledErrors(err));
		if (user && user.doctor) {
			Doctor.findOne({ _id: user.doctor }, function(err, doctor) {
				if (err) return next(common.catchUnhandledErrors(err));
				res.json(doctor)
			})
		} else {
			return next(new Error('DoctorWithThatEmailNotExists'))
		}
	});
});

router.put('/remove/access/:doctor/:patient', function(req, res, next) {
	Doctor.findOne({ _id: req.params.doctor }, function(err, doctor) {
		if (err) return next(common.catchUnhandledErrors(err));
		if (!doctor) return next(new Error("DoctorNotFound"))
		Patient.findOne({ _id: req.params.patient }, function(err, patient) {
			if (err) return next(common.catchUnhandledErrors(err));
			if (!patient) return next(new Error("PatientNotFound"))

			var docIndex = doctor.patients.indexOf(patient._id),
				patIndex = patient.doctors.indexOf(doctor._id)
			if (docIndex != -1 && patIndex != -1) {
				doctor.splice(docIndex, 1);
				patient.splice(patIndex, 1);
				async.parallel({
					doctor: function(callback) {
						doctor.save(function(err, doctor) {
							if (err) return callback(doctor);
							callback(null, doctor);
						});
					},
					patient: function(callback) {
						patient.save(function(err, patient) {
							if (err) return callback(doctor);
							callback(null, patient)
						})
					}
				}, function(err, results) {
					if (err) return next(err);
					res.json(results.patient);
				});
			} else {
				return next(new Error('NoPatientDoctorRelation'))
			}
		})
	});
});

router.param('day', function(req, res, next, value) {
	var week = ['sunday', 'monday', 'tuesday', 'wednesday', 'thursday', 'friday', 'saturday'];
	var index = week.indexOf(req.params.day);
	if (index === -1) return next({ message: 'InvalidDay', status: 400 });
	req.day = week[index];

	next();
});

router.post('/export/doctors', function(req, res, next) {
	var data = req.body.data,
		tool = new nodeXls();
	console.log(req.body.data)
	var xls = tool.json2xls(data, {
		fieldMap: {
			name: "Nombre",
			title: "Especialiadad",
			email: "Correo Electronico",
			phone: "Numero Movil",
			phoneOffices: "Numero de Oficina",
			birthday: "Fecha de nacimiento"
		}
	});
	res.end(new Buffer(xls, 'binary'));

});

router.put('/me/tutorial', function(req, res, next) {
	var doctor = req.user.doctor;
	if (doctor.tutorial == undefined) {
		doctor.tutorial = false;
	} else {
		doctor.tutorial = !doctor.tutorial;
	};
	doctor.save(function(err, doc) {
		if (err) return next(common.catchUnhandledErrors(err));
		res.json(doc)
	})
})

router.post('/users/doctor', function(req, res, next) {
	User.findOne({ username: req.body.username }, function(err, user) {
		if (err) return next(common.catchUnhandledErrors(err));
		if (!user) return next(new Error("UserNotFound"));
		var doctor = new Doctor();
		doctor.tutorial = true;
		if (req.body.country) {
			var address = new Address(req.body);
			doctor.personalAddress = address._id;
		}
		var office = new Office();
		office.name = "Videollamada";
		office.doctor = doctor._id;
		doctor.offices.push(office._id);

		common.setModelValues(req.body, doctor);

		doctor.user = user._id;
		user.doctor = doctor._id;

		Role.find({ name: { $in: req.body.roles || [] } }).exec(
			function(err, roles) {
				if (err) return next(common.catchUnhandledErrors(err));
				for (var i = 0; i < roles.length; i++) {
					user.roles.push(roles[i]._id);
				}

				async.parallel({
					user: function(callback) {
						user.save(function(err, user) {
							callback(err, user);
						});
					},
					doctor: function(callback) {
						doctor.save(function(err, doctor) {
							callback(err, doctor);
						});
					},
					office: function(callback) {
						office.save(function(err, office) {
							callback(err, office);
						});
					},
					address: function(callback) {
						if (address) {
							address.save(function(err, address) {
								callback(err, address);
							});
						} else {
							callback(null);
						}
					}
				}, function(err, results) {
					if (err) {
						if (results.doctor) results.doctor.remove();
						if (results.user) results.user.remove();
						if (results.address) results.address.remove();
						return next(common.catchUnhandledErrors(err));
					}

					results.doctor.populate('user addresses', function(err, doctor) {
						if (err) return next(common.catchUnhandledErrors(err));

						if (ENV.env == "staging" || ENV.env == "production") {
							var options = {
								name: results.doctor.fullName,
								title: results.doctor.title,
								frontendUrl: config['app'].frontendPath,
								backendUrl: config['app'].publicPath
							}
							var html = pug.renderFile("app/views/doctor/new.pug", options);

							var params = {
								Destination: {
									ToAddresses: ["brian.delalcazar@gmail.com", "g.escalona@siplik.com", "icordoba@4geeks.co"]
								},
								Message: {
									Body: {
										Html: {
											Data: html
										}
									},
									Subject: {
										Data: 'Nuevo usuario registrado en Siplik'
									}
								},
								Source: "g.escalona@siplik.com"
							}
							ses.sendEmail(params, function(err, res) {});

							var html = pug.renderFile("app/views/doctor/new.pug", options);

							var params = {
								Destination: {
									ToAddresses: [results.user.username]
								},
								Message: {
									Body: {
										Html: {
											Data: html
										}
									},
									Subject: {
										Data: 'Bienvenido a Siplik!!!'
									}
								},
								Source: "g.escalona@siplik.com"
							}
							ses.sendEmail(params, function(err, res) {});

						};

						res.json(doctor);
					});
				});
			}
		);
	})
})

router.post('/me/mercadoPago', function(req, res, next) {
	var doctor = req.user.doctor;
	var mp = new MercadoPago();
	common.setModelValues(req.body, mp);
	mp.doctor = doctor._id;
	doctor.mercadoPago = mp._id;
	async.parallel({
		doctor: function(callback) {
			doctor.save(function(err, doctor) {
				callback(err, doctor)
			})
		},
		mp: function(callback) {
			mp.save(function(err, mp) {
				callback(err, mp);
			})
		}
	}, function(err, results) {
		if (err) return next(common.catchUnhandledErrors(err));
		res.json(results.doctor)
	})
})
router.put('/me/mercadoPago/qrCode', multiparty({ uploadDir: './uploads' }), function(req, res, next) {
	var doctor = req.user.doctor;
	var mp = doctor.mercadoPago;
	common.setModelValues(req.body, mp);
	if (req.files.image.size > 4194304) {
		return next(new Error('FileSizeLimit'));
	}
	async.series({
		validation: function(callback) {
			if (!!req.user.doctor.mercadoPago.path) {
				fs.unlink(path.normalize(__dirname + '/../../' + req.user.doctor.mercadoPago.path), function(err) {
					if (err) return callback(err);
					callback();
				});
			} else {
				callback();
			}
		},
		mercadoPago: function(callback) {

			req.user.doctor.mercadoPago.path = 'uploads/' + req.user.doctor._id + path.extname(req.files.image.name);
			fs.rename(req.files.image.path, path.normalize(__dirname + '/../../' + req.user.doctor.mercadoPago.path), function(err) {
				if (err) return callback(err);

				req.user.doctor.save(function(err, savedDoctor) {
					if (err) return callback(err);

					savedDoctor = savedDoctor.toObject();

					callback(err, savedDoctor.mercadoPago);
				});

			});
		}
	}, function(err, results) {
		if (err) return next(common.catchUnhandledErrors(err));

		res.json(results.mercadoPago);
	});
})
router.put('/me/mercadoPago', function(req, res, next) {
	var doctor = req.user.doctor;
	MercadoPago.findOne({ _id: doctor.mercadoPago }, function(err, mp) {
		if (err) return next(common.catchUnhandledErrors(err));
		common.setModelValues(req.body, mp);
		mp.save(function(err, mp) {
			if (err) return next(common.catchUnhandledErrors(err));
			doctor.mercadoPago = mp;
			res.json(doctor)
		})
	})
})
router.post('/me/premium', function(req, res, next) {
	var doctor = req.user.doctor,
		token = req.body.token; // Using Express

	// CHECK IF DOCTOR HAS BEEN PREMIUM BEFORE
	if (doctor.premium && doctor.premium.firstTime) {
		// SEARCH 1 MONTH PLAN
		PremiumPlan.findOne({ currency: "usd", name: "1 Mes" }, function(err, plan) {
			if (err) return next(commom.catchUnhandledErrors(err))
			if (!plan) return next(new Error("NoPlanFound"))
				// CREATE STRIPE CUSTOMER 
			stripe.customers.create({
				source: token,
				description: req.user.username
			}).then(function(customer) {
				// CHARGE THE CREDIT CARD
				return stripe.charges.create({
					amount: plan.price, // Amount in cents
					currency: plan.currency,
					customer: customer.id
				});
			}).then(function(charge) {
				// CREATE PAYTMENT
				var payment = new Payment(),
					doctor = req.user.doctor
					//THIS PAYMENT NAME IS STRIPE
				payment.name = "Stripe";
				// CREATE PREMIUM
				var premium = new Premium(),
					dateNow = new Date(),
					dateEnd = new Date();
				//SET DATES FOR PREMY, ASSIGN DOCTOR, PAYMENT AND PLAN
				premium.startDate = dateNow;
				dateEnd.setMonth(dateEnd.getMonth() + 1);
				premium.endDate = dateEnd;
				premium.payment = payment._id;
				premium.doctor = doctor.id;
				// SAVE ALL THE DATA WHATS COME FROM STRIPE
				payment.data = charge;
				premium.plan = plan._id;
				//IF DOCTOR HAVENT BEEN PREMIUM, ASSING PREMY 
				if (!doctor.premium) {
					doctor.premium = {};
				}
				// SET FIRST TIME PREMY TO FALSE
				if (doctor.premium.firstTime) {
					doctor.premium.firstTime = false;
				}
				// IF PREMY TIME WASNT ACTIVE, ACTIVE IT
				if (!doctor.premium.active) {
					doctor.premium.active = true;
				}
				//ASSING PREMY TO DOCTOR
				doctor.premium.lastTime = dateEnd;
				doctor.premium.premium = premium._id
					//SAVE ALL
				Doctor.populate(req.user.doctor, { path: "offices", model: "Office" }, function(err, doc) {
					if (err) return next(commom.catchUnhandledErrors(err))
					var videoOffice = false;
					for (var i = 0; i < doc.offices.length; i++) {
						if (doc.offices[i].name == "Videollamada") {
							var videoOffice = true;
						}
					}
					if (!videoOffice) {
						var office = new Office();
						office.name = "Videollamada";
						office.doctor = doctor._id;
						doctor.offices.push(office._id);
					}

					async.series({
						doctor: function(callback) {
							doctor.save(function(err, doctor) {
								callback(err, doctor);
							})
						},
						premium: function(callback) {
							premium.save(function(err, premium) {
								callback(err, premium);
							})
						},
						payment: function(callback) {
							payment.save(function(err, payment) {
								callback(err, payment);
							})
						},
						office: function(callback) {
							if (!videoOffice) {
								office.save(function(err, office) {
									callback(err, office);
								})
							} else {
								callback(null);
							}
						}
					}, function(err, results) {
						if (err) return next(common.catchUnhandledErrors(err));
						//RETURN DOCTOR
						res.json(results.doctor)
					});
				})
			})
		});
	} else {
		//IF DOCTOR HAS ALREADY BEEN PREMY
		var doctor = req.user.doctor;
		// SEARCH AN OLD PREMIUM
		Premium.findOne({ doctor: doctor._id })
			.populate('payment')
			.populate('plan')
			.sort('-created')
			.exec(function(err, oldPremium) {
				if (err) return next(common.catchUnhandledErrors(err))
				if (!oldPremium) return next(new Error("NoPremiumFound"))
				console.log(oldPremium)
					// CHARGES THE CREDIT CARD SAVED BEFORE
				stripe.charges.create({
					amount: oldPremium.plan.price, // Amount in cents
					currency: oldPremium.plan.currency,
					customer: oldPremium.payment.data.customer
				}, function(err, charge) {
					//CREATING PAYMENT
					var payment = new Payment(),
						doctor = req.user.doctor
						//THIS PAYMENT NAME IS STRIPE
					payment.name = "Stripe";
					// CREATE PREMIUM
					var premium = new Premium(),
						dateNow = new Date(),
						dateEnd = new Date();
					//SET DATES FOR PREMY, ASSIGN DOCTOR, PAYMENT AND PLAN
					premium.startDate = dateNow;
					dateEnd.setMonth(dateEnd.getMonth() + 1);
					premium.endDate = dateEnd;
					premium.payment = payment._id;
					premium.doctor = doctor.id;
					if (err) return next(commom.catchUnhandledErrors(err))
						// SAVE ALL THE DATA WHATS COME FROM STRIPE
					payment.data = charge;
					premium.plan = oldPremium.plan._id;
					//IF DOCTOR HAVENT BEEN PREMIUM, ASSING PREMY 
					if (!doctor.premium) {
						doctor.premium = {};
					}
					// SET FIRST TIME PREMY TO FALSE
					if (doctor.premium.firstTime) {
						doctor.premium.firstTime = false;
					}
					// IF PREMY TIME WASNT ACTIVE, ACTIVE IT
					if (!doctor.premium.active) {
						doctor.premium.active = true;
					}
					//ASSING PREMY TO DOCTOR
					doctor.premium.lastTime = dateEnd;
					doctor.premium.premium = premium._id;
					//SAVE ALL
					Doctor.populate(req.user.doctor, { path: "offices", model: "Office" }, function(err, doc) {
						if (err) return next(commom.catchUnhandledErrors(err))
						var videoOffice = false;
						for (var i = 0; i < doc.offices.length; i++) {
							if (doc.offices[i].name == "Videollamada") {
								var videoOffice = true;
							}
						}
						if (!videoOffice) {
							var office = new Office();
							office.name = "Videollamada";
							office.doctor = doctor._id;
							doctor.offices.push(office._id);
						}

						async.series({
							doctor: function(callback) {
								doctor.save(function(err, doctor) {
									callback(err, doctor);
								})
							},
							premium: function(callback) {
								premium.save(function(err, premium) {
									callback(err, premium);
								})
							},
							payment: function(callback) {
								payment.save(function(err, payment) {
									callback(err, payment);
								})
							},
							office: function(callback) {
								if (!videoOffice) {
									office.save(function(err, office) {
										callback(err, office);
									})
								} else {
									callback(null);
								}
							}
						}, function(err, results) {
							if (err) return next(common.catchUnhandledErrors(err));
							//RETURN DOCTOR
							res.json(results.doctor)
						});
					})
				})

			})

	}
})

router.post('/me/premium/MP', function(req, res, next) {
	var doctor = req.user.doctor;
	if (req.body.collection_status == 'approved') {
		// CHECK IF DOCTOR HAS BEEN PREMIUM BEFORE
		if (doctor.premium && doctor.premium.firstTime) {
			// SEARCH 1 MONTH PLAN
			PremiumPlan.findOne({ currency: "usd", name: "1 Mes" }, function(err, plan) {
				if (err) return next(commom.catchUnhandledErrors(err))
				if (!plan) return next(new Error("NoPlanFound"))
					// CREATE STRIPE CUSTOMER 
					// CREATE PAYTMENT
				var payment = new Payment();
				//THIS PAYMENT NAME IS STRIPE
				payment.name = "MercadoPago";
				// CREATE PREMIUM
				var premium = new Premium(),
					dateNow = new Date(),
					dateEnd = new Date();
				//SET DATES FOR PREMY, ASSIGN DOCTOR, PAYMENT AND PLAN
				premium.startDate = dateNow;
				dateEnd.setMonth(dateEnd.getMonth() + 1);
				premium.endDate = dateEnd;
				premium.payment = payment._id;
				premium.doctor = doctor.id;
				// SAVE ALL THE DATA WHATS COME FROM STRIPE
				payment.data = req.body;
				premium.plan = plan._id;
				//IF DOCTOR HAVENT BEEN PREMIUM, ASSING PREMY 
				if (!doctor.premium) {
					doctor.premium = {};
				}
				// SET FIRST TIME PREMY TO FALSE
				if (doctor.premium.firstTime) {
					doctor.premium.firstTime = false;
				}
				// IF PREMY TIME WASNT ACTIVE, ACTIVE IT
				if (!doctor.premium.active) {
					doctor.premium.active = true;
				}
				//ASSING PREMY TO DOCTOR
				doctor.premium.lastTime = dateEnd;
				doctor.premium.premium = premium._id
					//SAVE ALL
				Doctor.populate(req.user.doctor, { path: "offices", model: "Office" }, function(err, doc) {
					if (err) return next(commom.catchUnhandledErrors(err))
					var videoOffice = false;
					for (var i = 0; i < doc.offices.length; i++) {
						if (doc.offices[i].name == "Videollamada") {
							var videoOffice = true;
						}
					}
					if (!videoOffice) {
						var office = new Office();
						office.name = "Videollamada";
						office.doctor = doctor._id;
						doctor.offices.push(office._id);
					}

					async.series({
						doctor: function(callback) {
							doctor.save(function(err, doctor) {
								callback(err, doctor);
							})
						},
						premium: function(callback) {
							premium.save(function(err, premium) {
								callback(err, premium);
							})
						},
						payment: function(callback) {
							payment.save(function(err, payment) {
								callback(err, payment);
							})
						},
						office: function(callback) {
							if (!videoOffice) {
								office.save(function(err, office) {
									callback(err, office);
								})
							} else {
								callback(null);
							}
						}
					}, function(err, results) {
						if (err) return next(common.catchUnhandledErrors(err));
						//RETURN DOCTOR
						res.json(results.doctor)
					});
				})
			})
		} else {
			//IF DOCTOR HAS ALREADY BEEN PREMY
			var doctor = req.user.doctor;
			// SEARCH AN OLD PREMIUM
			Premium.findOne({ doctor: doctor._id })
				.populate('payment')
				.populate('plan')
				.sort('-created')
				.exec(function(err, oldPremium) {
					if (err) return next(common.catchUnhandledErrors(err))
					if (!oldPremium) return next(new Error("NoPremiumFound"))
						// CHARGES THE CREDIT CARD SAVED BEFORE
						//CREATING PAYMENT
					var payment = new Payment(),
						doctor = req.user.doctor
						//THIS PAYMENT NAME IS STRIPE
					payment.name = "MercadoPago";
					// CREATE PREMIUM
					var premium = new Premium(),
						dateNow = new Date(),
						dateEnd = new Date();
					//SET DATES FOR PREMY, ASSIGN DOCTOR, PAYMENT AND PLAN
					premium.startDate = dateNow;
					dateEnd.setMonth(dateEnd.getMonth() + 1);
					premium.endDate = dateEnd;
					premium.payment = payment._id;
					premium.doctor = doctor.id;
					if (err) return next(commom.catchUnhandledErrors(err))
						// SAVE ALL THE DATA WHATS COME FROM STRIPE
					payment.data = req.body;
					premium.plan = oldPremium.plan._id;
					//IF DOCTOR HAVENT BEEN PREMIUM, ASSING PREMY 
					if (!doctor.premium) {
						doctor.premium = {};
					}
					// SET FIRST TIME PREMY TO FALSE
					if (doctor.premium.firstTime) {
						doctor.premium.firstTime = false;
					}
					// IF PREMY TIME WASNT ACTIVE, ACTIVE IT
					if (!doctor.premium.active) {
						doctor.premium.active = true;
					}
					//ASSING PREMY TO DOCTOR
					doctor.premium.lastTime = dateEnd;
					doctor.premium.premium = premium._id;
					//SAVE ALL
					Doctor.populate(req.user.doctor, { path: "offices", model: "Office" }, function(err, doc) {
						if (err) return next(commom.catchUnhandledErrors(err))
						var videoOffice = false;
						for (var i = 0; i < doc.offices.length; i++) {
							if (doc.offices[i].name == "Videollamada") {
								var videoOffice = true;
							}
						}
						if (!videoOffice) {
							var office = new Office();
							office.name = "Videollamada";
							office.doctor = doctor._id;
							doctor.offices.push(office._id);
						}

						async.series({
							doctor: function(callback) {
								doctor.save(function(err, doctor) {
									callback(err, doctor);
								})
							},
							premium: function(callback) {
								premium.save(function(err, premium) {
									callback(err, premium);
								})
							},
							payment: function(callback) {
								payment.save(function(err, payment) {
									callback(err, payment);
								})
							},
							office: function(callback) {
								if (!videoOffice) {
									office.save(function(err, office) {
										callback(err, office);
									})
								} else {
									callback(null);
								}
							}
						}, function(err, results) {
							if (err) return next(common.catchUnhandledErrors(err));
							//RETURN DOCTOR
							res.json(results.doctor)
						});
					})
				})

		}

	} else if (req.body.collection_status == 'pending') {
		return next(new Error('PayNotCompleted'));
	} else if (req.body.collection_status == 'in_process') {
		return next(new Error('PayIsBeingProcessed'));
	} else if (req.body.collection_status == 'rejected') {
		return next(new Error('PayRejected'));
	} else if (req.body.collection_status == null) {
		return next(new Error('PayNotReceived'));
	}
})
router.post('/me/payPal', function(req, res, next) {
	var doctor = req.user.doctor;
	var pPal = new PayPal();
	common.setModelValues(req.body, pPal);
	pPal.doctor = doctor._id;
	doctor.payPal = pPal._id;
	async.parallel({
		doctor: function(callback) {
			doctor.save(function(err, doctor) {
				callback(err, doctor)
			})
		},
		pPal: function(callback) {
			pPal.save(function(err, pPal) {
				callback(err, pPal);
			})
		}
	}, function(err, results) {
		if (err) return next(common.catchUnhandledErrors(err));
		res.json(results.doctor)
	})
})

router.put('/me/payPal', function(req, res, next) {
	var doctor = req.user.doctor;
	PayPal.findOne({ _id: doctor.payPal }, function(err, pPal) {
		if (err) return next(common.catchUnhandledErrors(err));
		common.setModelValues(req.body, pPal);
		pPal.save(function(err, pPal) {
			if (err) return next(common.catchUnhandledErrors(err));
			doctor.payPal = pPal;
			res.json(doctor)
		})
	})
})