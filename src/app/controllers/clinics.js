var mongoose = require('mongoose'),
    router = require('express').Router(),
    common = require('../common'),
    async = require('async'),
    path = require('path'),
    fs = require('fs'),
    crypto = require('crypto'),
    multiparty = require('connect-multiparty'),
    middlewares = require('../common/middlewares'),
    userMiddleware = require('../common/middlewares').user,
    Clinic = mongoose.model('Clinic'),
    Office = mongoose.model('Office'),
    Address = mongoose.model('Address'),
    Doctor = mongoose.model('Doctor'),
    Patient = mongoose.model('Patient'),
    Consultation = mongoose.model('Consultation'),
    User = mongoose.model('User'),
    config = require('../../config/config'),
    mailer = null,
    pug = require('pug'),
    AWS = require("aws-sdk");

    var ses = new AWS.SES({
        accessKeyId: "AKIAILQGFDBGE3QMZBMQ",
        secretAccessKey:"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",
        endpoint: 'email.us-west-2.amazonaws.com',
        region:"us-west-2"
    })

module.exports = function (app) {
    app.use('/api/clinics', router);
    mailer = app.mailer;
};

router.get('/', function(req, res, next) {
	var query = User.find({
		clinic: { $exists: true, $ne: null }
	});

	if (!req.query.all) {
		query.where({ 'active.status': true });
	}

	query.populate('clinic')
		.exec(function(err, users) {
			Clinic.populate(users, [{
				path: 'clinic.user',
				model: User
			}, {
				path: 'clinic.address',
				model: Address
			}], function(err, users) {
				if (err) return next(common.catchUnhandledErrors(err));
				res.json(users);
			});
		});
});

router.get('/:clinic', function(req, res, next) {
	res.json(req.clinic);
});

router.get('/:clinic/doctors', function(req, res, next) {
	req.clinic.getDoctors(function(err, doctors) {
		if (err) return next(err);
		res.json(doctors);
	});
});

router.post('/', userMiddleware.create, function(req, res, next) {
	var clinic = new Clinic();

	var address = new Address(req.body);
	clinic.address = address._id;

	common.setModelValues(req.body, clinic);

	clinic.user = req.userObject._id;
	req.userObject.clinic = clinic._id;
	req.userObject.active.status = true;

	async.parallel({
		user: function(callback) {
			req.userObject.save(function(err, user) {
				callback(err, user);
			});
		},
		clinic: function(callback) {
			clinic.save(function(err, clinic) {
				callback(err, clinic);
			});
		},
		address: function(callback) {
			if (address) {
				address.save(function(err, address) {
					callback(err, address);
				});
			} else {
				callback(null);
			}
		}
	}, function(err, results) {
		if (err) {
			// TODO: esto no sirve
			if (results.clinic) results.clinic.remove();
			if (results.user) results.user.remove();
			if (results.address) results.address.remove();
			if (results.office) results.office.remove();
			return next(common.catchUnhandledErrors(err));
		}

		results.clinic.populate('user address', function(err, clinic) {
			if (err) return next(common.catchUnhandledErrors(err));
			res.json(clinic);
		});
	});
});
router.put('/:clinic/photo', multiparty({ uploadDir: './uploads' }), function(req, res, next) {
	// 4194304 bytes == 4 megabytes
	if (req.files.image.size > 4194304) {
		return next(new Error('FileSizeLimit'));
	}

	async.series({
		validation: function(callback) {
			if (!!req.clinic.photo.path) {
				fs.unlink(path.normalize(__dirname + '/../../' + req.clinic.photo.path), function(err) {
					if (err) return callback(err);
					callback();
				});
			} else {
				callback();
			}
		},
		photo: function(callback) {
			req.clinic.photo.path = 'uploads/' + req.clinic._id + path.extname(req.files.image.name);
			fs.rename(req.files.image.path, path.normalize(__dirname + '/../../' + req.clinic.photo.path), function(err) {
				if (err) return callback(err);

				req.clinic.save(function(err, savedClinic) {
					if (err) return callback(err);

					savedClinic = savedClinic.toObject();

					callback(err, savedClinic.photo);
				});

			});
		}
	}, function(err, results) {
		if (err) return next(common.catchUnhandledErrors(err));

		res.json(results.photo);
	});
});

router.put('/:clinic', function(req, res, next) {
	common.setModelValues(req.body.address, req.clinic.address)
	req.clinic.assign(req.body);
	if (req.body.address.state) {
		req.clinic.address.state = req.body.address.state
		req.clinic.address.town = req.body.address.town
		req.clinic.address.parish = req.body.address.parish
	};
	async.parallel({
		address: function(callback) {
			req.clinic.address.save(function(err, address) {
				callback(err, address)
			})
		},
		clinic: function(callback) {
			req.clinic.save(function(err, clinic) {
				callback(err, clinic)
			})
		}
	}, function(err, results) {
		if (err) return next(common.catchUnhandledErrors(err));
		res.json(req.clinic);
	});
});

// TODO: para que sirve este endpoint?
router.get('/consultation/:doctor/:office', function(req, res, next) {
	var results = {};
	results.doctor = req.doctor
	Consultation.count({ doctor: req.doctor._id, office: req.params.office })
		.exec(function(err, consultations) {
			if (err) return next(common.catchUnhandledErrors(err));
			results.consultations = consultations;
			res.json(results);
		})
});

router.post('/:clinic/invite/:email', function(req, res, next) {
	User.findOne({
		username: req.params.email
	}, function(err, user) {
		if (err) return next(err);
		if (user && user.doctor) {
			var isUser = true;
			var isPatientUser = false;
		} else if (user && !user.doctor) {
			var isUser = false;
			var isPatientUser = true;
		} else {
			var isUser = false
			var isPatientUser = false;
		};
		var invalidEmail = false;
		for (var i = 0; i < req.clinic.activeCodes.length; i++) {
			if (req.clinic.activeCodes[i].email == req.params.email) {
				invalidEmail = true
			};
		};
		if (!invalidEmail) {
			var code = crypto.randomBytes(16).toString('hex');
			var activeCode = {
				code:code,
				email:req.params.email
			}
			req.clinic.activeCodes.push(activeCode)

			var writeInUrl = isUser == true ? "" : "/"+isPatientUser;

			var url = config['app'].frontendPath +(isUser ? '/#/invitation/' : '/#/invitation/siplik/')+req.params.email +"/"+code+"/"+req.clinic._id.toString() +writeInUrl
			var view = isUser ? 'invitation' : 'invitationSiplik';
			var subject = isUser ?
				'Invitacion a Clinica' :
				'Invitacion a Siplik de ' + req.clinic.name;

            var options = {
                code: code,
                name: req.clinic.name,
                url: url
            }
            var pathtoview = "app/views/"+view+".pug"
            var html = pug.renderFile(pathtoview, options);

            var params = {
                Destination: {
                    ToAddresses: [req.params.email]
                },
                Message: {
                    Body: {
                        Html: {
                            Data: html
                        }
                    },
                    Subject: {
                        Data: subject
                    }
                },
                Source: "g.escalona@siplik.com"
            }
            ses.sendEmail(params, function(err, res) {});
			req.clinic.save(function(err, clinic) {
				if (err) return next(err);
				return res.json(clinic);
			});
		} else {
			return next(new Error("EmailAlreadyInvited"))
		}

	});
});

router.get('/:clinic/patients', function(req, res, next) {
	Office.find({ clinic: req.clinic }, function(err, offices) {
		Consultation.distinct('patient', {
			office: { $in: offices }
		}, function(err, results) {
			Patient.find({
				_id: { $in: results }
			}, function(err, patients) {
				res.json(patients);
			})
		});
	});
});

router.get('/:clinic/doctors/:doctor', function(req, res, next) {
	res.json(req.doctor);
});

router.get('/:clinic/doctors/:doctor/patients', function(req, res, next) {
	Office.find({ clinic: req.clinic }, function(err, offices) {
		Consultation.distinct('patient', {
			office: { $in: offices },
			doctor: req.doctor._id
		}, function(err, results) {
			Patient.find({
				_id: { $in: results }
			}, function(err, patients) {
				res.json(patients);
			})
		});
	});
});

router.get('/:clinic/offices', function(req, res, next) {
	Office.find({ clinic: req.clinic })
		.populate('doctor')
		.exec(function(err, offices) {
			res.json(offices);
		});
});

router.put('/:clinic/offices/:office/doctor/:email', function(req, res, next) {
	if (req.office.doctor) return next({
		message: 'InvalidAction',
		status: 400
	});
	User.findOne({
		username: req.params.email,
		doctor: { $exists: true }
	}).populate('doctor').exec(function(err, user) {
		if (err) return next(err);
		if (!user) return next({ message: 'UserNotFound', status: 400 });
		Doctor.update({
			_id: user.doctor
		}, {
			$push: { offices: req.office._id }
		}, function(err) {
			req.office.doctor = user.doctor;
			req.office.save(function(err, office) {
				if (err) return next(err);
				res.json(office);
			});
		})
	});
});

router.delete('/:clinic/offices/:office/doctor', function(req, res, next) {
	Doctor.update({
		_id: req.office.doctor
	}, {
		$pull: { offices: req.office._id }
	}, function(err) {
		req.office.doctor = undefined;
		req.office.save(function(err, office) {
			res.json(office);
		});
	});
});

router.param('clinic', function(req, res, next, value) {
	var query;
	if (value === 'me') {
		query = Clinic.findOne({
			user: req.user._id
		})
	} else {
		query = Clinic.findById(value)
	}
	query
		.populate('user')
		.populate('address')
		.exec(function(err, clinic) {
			if (err) return next(err);
			if (!clinic) return next({
				message: 'ClinicNotFound',
				status: 400
			})
			req.clinic = clinic;
			next();
		});
});

router.param('doctor', function(req, res, next, value) {
	Doctor.findById(value)
		.populate('user')
		.exec(function(err, doctor) {
			if (err) return next(err);
			if (!doctor) return next({
				message: 'DoctorNotFound',
				status: 400
			});
			req.doctor = doctor;
			next();
		})
});

router.param('office', function(req, res, next, value) {
	Office.findOne({
		_id: value,
		clinic: req.clinic._id
	}, function(err, office) {
		if (err) return next(err);
		if (!office) return next({
			message: 'OfficeNotFound',
			status: 400
		});
		req.office = office;
		next();
	});
});
