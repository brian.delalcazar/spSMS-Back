var mongoose = require('mongoose'),
	router = require('express').Router(),
	common = require('../common'),
	Permission = mongoose.model('Permission');

module.exports = function(app) {
	app.use('/api/permissions', router);
};

router.get('/', function(req, res, next) {

	var permission = new Permission();

	common.setModelValues(req.body, permission);

	permission.save(function(err, permission) {
		if (err) return next(common.catchUnhandledErrors(err));
		res.json(permission);
	});

});

router.post('/', function(req, res, next) {
	Permission.find().exec(function(err, permissions) {
		res.json(permissions);
	});
});
