var mongoose = require('mongoose'),
    router = require('express').Router(),
    common = require('../../common'),
    async = require('async'),
    path = require('path'),
    fs = require('fs'),
    crypto = require('crypto'),
    multiparty = require('connect-multiparty'),
    middlewares = require('../../common/middlewares'),
    cleanWords = require('../../common/middlewares/cleanRecords'),
    DataRecord = mongoose.model('DataRecord'),
    WordDictionary = mongoose.model('WordDictionary'),
    userMiddleware = require('../../common/middlewares').user,
    User = mongoose.model('User'),
    Data = mongoose.model('Data'),
    Role = mongoose.model('Role'),
    PercentilData = mongoose.model('PercentilData'),
    config = require('../../../config/config'),
    mailer = null,
    util = require('util'),
    pug = require('pug'),
    AWS = require("aws-sdk"),
    PythonShell = require('python-shell');

const ObjectId = mongoose.Types.ObjectId;

var ses = new AWS.SES({
    accessKeyId: "AKIAILQGFDBGE3QMZBMQ",
    secretAccessKey: "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",
    endpoint: 'email.us-west-2.amazonaws.com',
    region: "us-west-2"
})

module.exports = function(app) {
    app.use('/api/datas', router);
    mailer = app.mailer;
};
router.post('/accounts/registers/new', userMiddleware.create, function(req, res, next) {
    var dataUser = new Data();
    common.setModelValues(req.body, dataUser);
    dataUser.user = req.userObject._id;
    req.userObject.data = dataUser._id;

    async.parallel({
        user: function(callback) {
            req.userObject.save(function(err, user) {
                callback(err, user);
            });
        },
        dataUser: function(callback) {
            dataUser.save(function(err, dataUser) {
                callback(err, dataUser);
            });
        }
    }, function(err, results) {
        if (err) return next(common.catchUnhandledErrors(err));
        res.json(results.dataUser)
    })
});

router.post('/accounts/registers/old', function(req, res, next) {
    User.findOne({ username: req.body.username })
        .exec(function(err, user) {
            if (err) return next(common.catchUnhandledErrors(err));
            if (!user) {
                return next(new Error("UserNotFound"))
            }
            if (user.data) {
                return next(new Error("UserAlreadyHasDataUser"))
            }
            Role.findOne({ name: 'data' })
                .exec(function(err, role) {
                    var dataUser = new Data();
                    common.setModelValues(req.body, dataUser);
                    dataUser.user = user._id;
                    user.data = dataUser._id;
                    user.roles.push(role._id);
                    async.parallel({
                        user: function(callback) {
                            user.save(function(err, user) {
                                callback(err, user);
                            });
                        },
                        dataUser: function(callback) {
                            dataUser.save(function(err, dataUser) {
                                callback(err, dataUser);
                            });
                        }
                    }, function(err, results) {
                        if (err) return next(common.catchUnhandledErrors(err));
                        res.json(results.dataUser)
                    })
                })
        })
});

router.post('/accounts/forgotPassword', middlewares.remember.generateCode, function(req, res, next) {
    var options = {
        url: config['app'].dataPath + '/#/recovery/' + req.body.username + '/' + req.userObject.remember,
        backendUrl: config['app'].publicPath,
        frontendUrl: config['app'].frontendPath
    }
    var html = pug.renderFile("app/views/doctor/remember.pug", options);

    var params = {
        Destination: {
            ToAddresses: [req.body.username]
        },
        Message: {
            Body: {
                Html: {
                    Data: html
                }
            },
            Subject: {
                Data: 'Recupera tu cuenta.'
            }
        },
        Source: "g.escalona@siplik.com"
    }
    ses.sendEmail(params, function(err, res) {
        if (err) return next(err);
        res.json(req.userObject);
    });

});
router.post('/accounts/recovery/:email/:code', middlewares.remember.changePassword, function(req, res, next) {
    res.json(req.userObject);

    // mailer.send('doctor/recovery', {
    //     to: req.params.email,
    //     subject: 'Tu cuenta está recuperada.',
    //     backendUrl: config['app'].publicPath,
    //     frontendUrl: config['app'].dataPath
    // }, function(err) {
    //     if (err) return next(err);
    //     res.json(req.userObject);
    // });
});

router.post('/files/add', function(req, res, next) {
    var procesed = 0;
    var ready = req.body.ready;
    req.body.data.forEach(function(data) {
        var da = new DataRecord(data);
        da.save(function(err, da) {
            procesed++;
            if (procesed === req.body.data.length) {
                res.send("200");
            }
        });
    })
    if (ready) {
        findAndCleanOccupations(req.body.fileName, req.body.dataUser)
        findAndCleanMotives(req.body.fileName, req.body.dataUser)
        findAndCleanDiagnosis(req.body.fileName, req.body.dataUser)
    }
});

router.get('/files/index/:userId', function(req, res, next) {
    var userId = req.params.userId;
    DataRecord.aggregate()
        .match({
            dataUser: ObjectId(userId)
        })
        .group({
            _id: "$fileName"
        })
        .exec(function(err, data) {
            data = data.reduce(function(a, b) {
                a.push(b._id)
                return a
            }, [])
            res.json(data)

        })
});

router.get('/files/:fileName/:userId', function(req, res, next) {
    var userId = req.params.userId,
        fileName = req.params.fileName;

    DataRecord.aggregate()
        .match({
            dataUser: ObjectId(userId),
            fileName: fileName
        })
        .group({
            _id: "$fileName",
            count: { $sum: 1 }
        })
        .exec(function(err, data) {
            res.json(data)
        })
});

router.delete('/files/:fileName/:userId', function(req, res, next) {
    var userId = req.params.userId,
        file = req.body.file,
        fileName = req.params.fileName;

    DataRecord.remove({ dataUser: userId, fileName: fileName }, function(err, data) {
        if (err) return next(err)
        var files = data;
        res.json(files)
    });
});

router.get('/files/:fileName/:userId/fields/:field', function(req, res, next) {
    var userId = req.params.userId,
        fileName = req.params.fileName,
        field = req.params.field;

    DataRecord.distinct(field, function(err, words) {
        if (err) return next(err)
        res.json(words)
    });
});

router.put('/files/:fileName/:userId/fields/:field', function(req, res, next) {
    var userId = req.params.userId,
        fileName = req.params.fileName,
        newName = req.body.newName,
        selected = req.body.selected,
        field = req.params.field,
        toUpdate = {};
    toUpdate['changes.' + field] = newName;

    var toFind = [{ dataUser: userId }, { fileName: fileName }, {}];
    toFind[2][field] = { $in: selected };

    DataRecord.update({ $and: toFind }, { $set: toUpdate }, { multi: true }, function(err, words) {
        if (err) return next(err)
        res.json(words)
    });
});


router.get('/files/:fileName/:userId/indicators/:indicator', function(req, res, next) {
    var userId = req.params.userId,
        fileName = req.params.fileName,
        indicator = req.params.indicator;
    getIndicatorResults({ dataUser: userId, fileName: fileName }, indicator, next, function(result) {
        res.json(result);
    });
});
router.get('/files/:fileName/:userId/indicator/digitel/details/:percentil', function(req, res, next) {
    var userId = req.params.userId,
        fileName = req.params.fileName,
        percentil = req.params.percentil,
        query = JSON.parse(req.query.query);

    query.fileName = fileName;
    var groupAge = {},
        groupGenre = {},
        groupEdu = {};
    if (percentil == "Sobrepeso y estatura normal") {
        query.percentilHeight = { $in: ["97th", "85th", "median"] }
        query["$or"] = [{
            $and: [{
                age: { $lt: 10 }
            }, {
                percentilWeight: { $eq: "97th" }
            }]
        }, {
            $and: [{
                age: { $gt: 9 }
            }, {
                percentilImc: { $eq: "97th" }

            }]
        }]

    }
    if (percentil == "Sobrepeso con riesgo de estatura baja") {
        query.percentilHeight = { $in: ["15th", "3rd"] }
        query["$or"] = [{
            $and: [{
                age: { $lt: 10 }
            }, {
                percentilWeight: { $eq: "97th" }
            }]
        }, {
            $and: [{
                age: { $gt: 9 }
            }, {
                percentilImc: { $eq: "97th" }

            }]
        }]

    }
    if (percentil == "Obesidad con estatura alta") {
        query.percentilHeight = { $in: ["97th"] }
        query["$or"] = [{
            $and: [{
                age: { $lt: 10 }
            }, {
                percentilWeight: { $eq: "85th" }
            }]
        }, {
            $and: [{
                age: { $gt: 9 }
            }, {
                percentilImc: { $eq: "85th" }

            }]
        }]

    }
    if (percentil == "Obesidad con estatura normal") {
        query.percentilHeight = { $in: ["85th", "median"] }
        query["$or"] = [{
            $and: [{
                age: { $lt: 10 }
            }, {
                percentilWeight: { $eq: "85th" }
            }]
        }, {
            $and: [{
                age: { $gt: 9 }
            }, {
                percentilImc: { $eq: "85th" }

            }]
        }]

    }
    if (percentil == "Obesidad con riesgo de estatura baja") {
        query.percentilHeight = { $in: ["15th", "3rd"] }
        query["$or"] = [{
            $and: [{
                age: { $lt: 10 }
            }, {
                percentilWeight: { $eq: "85th" }
            }]
        }, {
            $and: [{
                age: { $gt: 9 }
            }, {
                percentilImc: { $eq: "85th" }

            }]
        }]

    }
    if (percentil == "Peso adecuado con estatura alta") {
        query.percentilHeight = { $in: ["97th"] }
        query["$or"] = [{
            $and: [{
                age: { $lt: 10 }
            }, {
                percentilWeight: { $eq: "median" }
            }]
        }, {
            $and: [{
                age: { $gt: 9 }
            }, {
                percentilImc: { $eq: "median" }

            }]
        }]

    }
    if (percentil == "Peso adecuado con riesgo de estatura alta") {
        query.percentilHeight = { $in: ["85th"] }
        query["$or"] = [{
            $and: [{
                age: { $lt: 10 }
            }, {
                percentilWeight: { $eq: "median" }
            }]
        }, {
            $and: [{
                age: { $gt: 9 }
            }, {
                percentilImc: { $eq: "median" }

            }]
        }]

    }
    if (percentil == "Peso adecuado con estatura normal") {
        query.percentilHeight = { $in: ["median"] }
        query["$or"] = [{
            $and: [{
                age: { $lt: 10 }
            }, {
                percentilWeight: { $eq: "median" }
            }]
        }, {
            $and: [{
                age: { $gt: 9 }
            }, {
                percentilImc: { $eq: "median" }

            }]
        }]

    }
    if (percentil == "Peso adecuado con riesgo de estatura baja") {
        query.percentilHeight = { $in: ["15th"] }
        query["$or"] = [{
            $and: [{
                age: { $lt: 10 }
            }, {
                percentilWeight: { $eq: "median" }
            }]
        }, {
            $and: [{
                age: { $gt: 9 }
            }, {
                percentilImc: { $eq: "median" }

            }]
        }]

    }
    if (percentil == "Peso adecuado con estatura baja") {
        query.percentilHeight = { $in: ["3rd"] }
        query["$or"] = [{
            $and: [{
                age: { $lt: 10 }
            }, {
                percentilWeight: { $eq: "median" }
            }]
        }, {
            $and: [{
                age: { $gt: 9 }
            }, {
                percentilImc: { $eq: "median" }

            }]
        }]

    }
    if (percentil == "Bajo peso con estatura alta") {
        query.percentilHeight = { $in: ["97th"] }
        query["$or"] = [{
            $and: [{
                age: { $lt: 10 }
            }, {
                percentilWeight: { $eq: "15th" }
            }]
        }, {
            $and: [{
                age: { $gt: 9 }
            }, {
                percentilImc: { $eq: "15th" }

            }]
        }]

    }
    if (percentil == "Bajo peso con estatura normal") {
        query.percentilHeight = { $in: ["85th", "median"] }
        query["$or"] = [{
            $and: [{
                age: { $lt: 10 }
            }, {
                percentilWeight: { $eq: "15th" }
            }]
        }, {
            $and: [{
                age: { $gt: 9 }
            }, {
                percentilImc: { $eq: "15th" }

            }]
        }]

    }
    if (percentil == "Bajo peso con riesgo de estatura baja") {
        query.percentilHeight = { $in: ["15th"] }
        query["$or"] = [{
            $and: [{
                age: { $lt: 10 }
            }, {
                percentilWeight: { $eq: "15th" }
            }]
        }, {
            $and: [{
                age: { $gt: 9 }
            }, {
                percentilImc: { $eq: "15th" }

            }]
        }]

    }
    if (percentil == "Bajo peso con estatura baja") {
        query.percentilHeight = { $in: ["3rd"] }
        query["$or"] = [{
            $and: [{
                age: { $lt: 10 }
            }, {
                percentilWeight: { $eq: "15th" }
            }]
        }, {
            $and: [{
                age: { $gt: 9 }
            }, {
                percentilImc: { $eq: "15th" }

            }]
        }]

    }
    if (percentil == "Riesgo de deficit de peso y estatura alta") {
        query.percentilHeight = { $in: ["97th"] }
        query["$or"] = [{
            $and: [{
                age: { $lt: 10 }
            }, {
                percentilWeight: { $eq: "3rd" }
            }]
        }, {
            $and: [{
                age: { $gt: 9 }
            }, {
                percentilImc: { $eq: "3rd" }

            }]
        }]

    }
    if (percentil == "Riesgo de deficit de peso y estatura normal") {
        query.percentilHeight = { $in: ["85th", "median"] }
        query["$or"] = [{
            $and: [{
                age: { $lt: 10 }
            }, {
                percentilWeight: { $eq: "3rd" }
            }]
        }, {
            $and: [{
                age: { $gt: 9 }
            }, {
                percentilImc: { $eq: "3rd" }

            }]
        }]

    }
    if (percentil == "Riesgo de deficit de peso y estatura baja") {
        query.percentilHeight = { $in: ["15th", "3rd"] }
        query["$or"] = [{
            $and: [{
                age: { $lt: 10 }
            }, {
                percentilWeight: { $eq: "3rd" }
            }]
        }, {
            $and: [{
                age: { $gt: 9 }
            }, {
                percentilImc: { $eq: "3rd" }

            }]
        }]

    }
    groupAge = {
        _id: "$age",
        count: { $sum: 1 }
    }
    groupGenre = {
        _id: { $toUpper: "$genre" },
        count: { $sum: 1 }
    }
    groupEdu = {
        _id: "$educationalInstitute",
        count: { $sum: 1 }
    }
    var results = {}
    DataRecord.aggregate()
        .match(query)
        .group(groupAge)
        .exec(function(err, data) {
            if (err) {
                return err
            }
            results.age = data;
            DataRecord.aggregate()
                .match(query)
                .group(groupGenre)
                .exec(function(err, data) {
                    if (err) {
                        return err
                    }
                    results.genre = data;
                    DataRecord.aggregate()
                        .match(query)
                        .group(groupEdu)
                        .exec(function(err, data) {
                            if (err) {
                                return err
                            }
                            results.edu = data;
                            res.json(results)
                        })
                })
        })


});
router.get('/files/:fileName/:userId/indicator/digitel', function(req, res, next) {
    var userId = req.params.userId,
        fileName = req.params.fileName,
        lookingForMore = req.query.more;
    DataRecord.aggregate()
        .match({
            fileName: fileName,
            $and: [{
                    $or: [
                        { percentilWeight: { $exists: true } },
                        { percentilImc: { $exists: true } }
                    ]
                },
                { percentilHeight: { $exists: true } }
            ]
        })
        .group(groupQueryMongo)
        .exec(function(err, results) {
            if (!lookingForMore) {
                sendDataTo(req.params.userId, DataRecord.count({ fileName: fileName, dataUser: userId, $and: [{ percentilWeight: { $exists: false } }, { percentilHeight: { $exists: false } }] }), fileName, userId, req.io);
            }
            res.json(results)
        })

});

function sendDataTo(socketId, max, fileName, userId, io) {
    if (max == 0) {
        return
    }
    var limitSkip = parseInt(max);
    var i = 0
    DataRecord.find({ fileName: fileName, dataUser: userId, $and: [{ percentilWeight: { $exists: false } }, { percentilHeight: { $exists: false } }, { percentilImc: { $exists: false } }] }, function(err, data) {
        if (!!data) {

            for (var i = data.length - 1; i >= 0; i--) {
                let item = data[i];
                let genre = "";
                if (!!item && !!item.genre) {
                    if (item.genre.toLowerCase() == "m" || item.genre.toLowerCase() == "masculino" || item.genre.toLowerCase() == "hombre") {
                        genre = "boy";
                    } else {
                        genre = "girl";
                    }
                }
                if (!!item.age && item.age <= 20) {

                    let ageMonth = parseInt(item.age) * 12;
                    ageMonth = parseInt(ageMonth) + parseInt(item.ageMonth)
                    PercentilData.find({ months: ageMonth.toString(), genre: genre }, function(err, percentil) {
                        let perHeight = {},
                            perWeight = {},
                            perImc = {},
                            change = false;
                        if (err || !percentil) {
                            return
                        }
                        for (var i = 0; i < percentil.length; i++) {
                            if (percentil[i].type == "imc") {
                                perImc = percentil[i]
                            }
                            if (percentil[i].type == "weight") {
                                perWeight = percentil[i]
                            }
                            if (percentil[i].type == "height") {
                                perHeight = percentil[i]
                            }
                        }
                        if (!!perImc.type) {
                            let array = [];
                            for (let key in perImc) {
                                if (key == "3rd" || key == "15th" || key == "median" || key == "85th" || key == "97th") {
                                    array.push(perImc[key])
                                }
                            }
                            let per = closest(item.imc, array)
                            for (let key in perImc) {
                                if (perImc[key] == per) {
                                    item.percentilImc = key;
                                    change = true;
                                }
                            }
                        }
                        if (!!perWeight.type) {
                            let array = [];
                            for (let key in perWeight) {
                                if (key == "3rd" || key == "15th" || key == "median" || key == "85th" || key == "97th") {
                                    array.push(perWeight[key])
                                }
                            }
                            let per = closest(item.weight, array)
                            for (let key in perWeight) {
                                if (perWeight[key] == per) {
                                    item.percentilWeight = key;
                                    change = true;

                                }
                            }
                        }
                        if (!!perHeight.type) {
                            let array = [];
                            for (let key in perHeight) {
                                if (key == "3rd" || key == "15th" || key == "median" || key == "85th" || key == "97th") {
                                    array.push(perHeight[key])
                                }
                            }
                            let per = closest(item.height, array)
                            for (let key in perHeight) {
                                if (perHeight[key] == per) {
                                    item.percentilHeight = key;
                                    change = true;

                                }
                            }
                        }
                        if (change) {
                            item.save(function(err, save) {})
                        }
                    })
                }
            }
        }
    })

}

function closest(num, arr) {
    var curr = arr[0];
    var diff = Math.abs(num - curr);
    for (var val = 0; val < arr.length; val++) {
        var newdiff = Math.abs(num - arr[val]);
        if (newdiff < diff) {
            diff = newdiff;
            curr = arr[val];
        }
    }
    return curr;
}

var getIndicatorResults = function(query, indicator, next, callback) {
    var result;
    switch (indicator) {
        case 'ageYear':
            ageByYear(query.fileName, callback);
            break;
        case 'age':
            ageByTotal(query.fileName, callback);
            break;
        case 'genre':
            genreByTotal(query.fileName, callback);
            break;
        case 'genreYear':
            genreByYear(query.fileName, callback);
            break;
        case 'insurance':
            insuranceByTotal(query.fileName, callback);
            break;
        case 'occupation':
            occupationByTotal(query.fileName, callback);
            break;
        case 'referedBy':
            referedByByTotal(query.fileName, callback);
            break;
        case 'insuranceYear':
            insuranceByYear(query.fileName, callback);
            break;
        case 'occupationYear':
            occupationByYear(query.fileName, callback);
            break;
        case 'referedByYear':
            referedByYear(query.fileName, callback);
            break;
        case 'ageGenre':
            genreAge(query.fileName, callback);
            break;
        case 'motive':
            motivesByTotal(query.fileName, callback);
            break;
        case 'motiveByYear':
            motiveByYear(query.fileName, callback);
            break;
        case 'diagnosis':
            diagnosisByTotal(query.fileName, callback);
            break;
        case 'diagnosisByYear':
            diagnosisByYear(query.fileName, callback);
            break;
        case 'insuranceGenre':
            genreInsurance(query.fileName, callback);
            break;
        case 'occupationGenre':
            genreOccupation(query.fileName, callback);
            break;
        case 'motiveGenre':
            genreMotive(query.fileName, callback);
            break;
        case 'diagnosisGenre':
            genreDiagnosis(query.fileName, callback);
            break;
        case 'referedByGenre':
            genreReferedBy(query.fileName, callback);
            break;
        case 'motiveByAge':
            ageMotive(query.fileName, callback);
            break;
        case 'diagnosisByAge':
            ageDiagnosis(query.fileName, callback);
            break;
        case 'ageByMotive':
            motiveAge(query.fileName, callback);
            break;
        case 'ageByDiagnosis':
            diagnosisAge(query.fileName, callback);
            break;
        case 'diagnosisByMotives':
            motiveDiagnosis(query.fileName, callback);
            break;
        default:
    }
}

function findAndCleanOccupations(fileName, dataUser) {
    var genre = false,
        age = false,
        occupation = false,
        referedBy = false,
        doctor = false,
        insurance = false;
    var keysStringsArray = []
    DataRecord.aggregate()
        .match({ $and: [{ occupation: { $exists: true } }, { occupation: { $ne: '' } }, { occupation: { $ne: null } }, { fileName: fileName }] })
        .group({ _id: "$occupation", count: { $sum: 1 }, recommend: { $first: "$recommended.occupation" } })
        .sort({ count: -1 })
        .exec(function(err, result) {
            let changes = [];

            if (err) return;
            async.parallel([
                function(callback) {
                    for (let [index, key] of result.entries()) {
                        keysStringsArray.push(key._id)
                        if (index >= result.length - 1) {
                            callback()
                        }
                    }
                }
            ], function() {
                var wrapped = keysStringsArray.map(function(value, index) {
                    return { index: index, value: value };
                });

                function makeChanges() {
                    async.map(changes, function(item, callback) {
                        DataRecord.update({ fileName: fileName, dataUser: dataUser, occupation: item.before, "recommended.occupation": { $exists: false } }, { $set: { "recommended.occupation": item.after } }, { multi: true }, function(err, done) {
                            callback(err, done)
                        })
                    }, function(err, results) {
                        return true
                    })
                }

                function startCleaning() {
                    if (keysStringsArray.length < 2) {
                        cleanWords.cleanRecords.aloneOccupations(fileName).then((val) => {
                            changes = changes.concat(val);
                            return makeChanges()
                        })
                    }
                    let keysToLookFor = keysStringsArray.slice();
                    keysToLookFor.splice(0, 1);
                    cleanWords.cleanRecords.occupations(keysStringsArray[0], keysToLookFor).then((val) => {
                        changes = changes.concat(val);
                        let keysToRemove = [];
                        for (let key of changes) {
                            keysToRemove.push(key.before);
                        }

                        keysStringsArray = keysToLookFor.reduce(function(acc, val) {
                            if (keysToRemove.indexOf(val) == -1) {
                                acc.push(val)
                            }
                            return acc
                        }, [])
                        startCleaning()
                    }, (e) => {
                        cleanWords.cleanRecords.aloneOccupations(fileName).then((val) => {
                            changes.concat(val);
                            return makeChanges()
                        })
                    })
                }
                startCleaning()


            })

        })
}

function findAndCleanMotives(fileName, dataUser) {
    var genre = false,
        age = false,
        motive = false,
        referedBy = false,
        doctor = false,
        insurance = false;
    var keysStringsArray = []
    DataRecord.aggregate()
        .match({ $and: [{ motive: { $exists: true } }, { motive: { $ne: '' } }, { fileName: fileName }] })
        .group({ _id: "$motive", count: { $sum: 1 }, recommend: { $first: "$recommended.motive" } })
        .sort({ count: -1 })
        .exec(function(err, result) {
            let changes = [];

            if (err) return
            async.parallel([
                function(callback) {
                    for (let [index, key] of result.entries()) {
                        keysStringsArray.push(key._id)
                        if (index >= result.length - 1) {
                            callback()
                        }
                    }
                }
            ], function() {
                var wrapped = keysStringsArray.map(function(value, index) {
                    return { index: index, value: value };
                });

                function makeChanges() {
                    async.map(changes, function(item, callback) {
                        DataRecord.update({ fileName: fileName, dataUser: dataUser, motive: item.before, "recommended.motive": { $exists: false } }, { $set: { "recommended.motive": item.after } }, { multi: true }, function(err, done) {
                            callback(null, done)
                        })
                    }, function(err, results) {
                        return true
                    })
                }

                function startCleaning() {
                    if (keysStringsArray.length < 2) {
                        cleanWords.cleanRecords.aloneOccupations(fileName).then((val) => {
                            changes = changes.concat(val);
                            return makeChanges()
                        })
                    }
                    let keysToLookFor = keysStringsArray.slice();
                    keysToLookFor.splice(0, 1);
                    cleanWords.cleanRecords.occupations(keysStringsArray[0], keysToLookFor).then((val) => {
                        changes = changes.concat(val);
                        let keysToRemove = [];
                        for (let key of changes) {
                            keysToRemove.push(key.before);
                        }

                        keysStringsArray = keysToLookFor.reduce(function(acc, val) {
                            if (keysToRemove.indexOf(val) == -1) {
                                acc.push(val)
                            }
                            return acc
                        }, [])
                        startCleaning()
                    }, (e) => {
                        cleanWords.cleanRecords.aloneOccupations(fileName).then((val) => {
                            changes.concat(val);
                            return makeChanges()
                        })
                    })
                }
                startCleaning()


            })

        })
}

function findAndCleanDiagnosis(fileName, dataUser) {
    var genre = false,
        age = false,
        diagnosis = false,
        referedBy = false,
        doctor = false,
        insurance = false;
    var keysStringsArray = []
    DataRecord.aggregate()
        .match({ $and: [{ diagnosis: { $exists: true } }, { diagnosis: { $ne: '' } }, { fileName: fileName }] })
        .group({ _id: "$diagnosis", count: { $sum: 1 }, recommend: { $first: "$recommended.diagnosis" } })
        .sort({ count: -1 })
        .exec(function(err, result) {
            let changes = [];

            if (err) return;
            async.parallel([
                function(callback) {
                    for (let [index, key] of result.entries()) {
                        keysStringsArray.push(key._id)
                        if (index >= result.length - 1) {
                            callback()
                        }
                    }
                }
            ], function() {
                var wrapped = keysStringsArray.map(function(value, index) {
                    return { index: index, value: value };
                });

                function makeChanges() {
                    async.map(changes, function(item, callback) {
                        DataRecord.update({ fileName: fileName, dataUser: dataUser, diagnosis: item.before, "recommended.diagnosis": { $exists: false } }, { $set: { "recommended.diagnosis": item.after } }, { multi: true }, function(err, done) {
                            callback(null, done)
                        })
                    }, function(err, results) {
                        return true
                    })
                }

                function startCleaning() {
                    if (keysStringsArray.length < 2) {
                        cleanWords.cleanRecords.aloneOccupations(fileName).then((val) => {
                            changes = changes.concat(val);
                            return makeChanges()
                        })
                    }
                    let keysToLookFor = keysStringsArray.slice();
                    keysToLookFor.splice(0, 1);
                    cleanWords.cleanRecords.occupations(keysStringsArray[0], keysToLookFor).then((val) => {
                        changes = changes.concat(val);
                        let keysToRemove = [];
                        for (let key of changes) {
                            keysToRemove.push(key.before);
                        }

                        keysStringsArray = keysToLookFor.reduce(function(acc, val) {
                            if (keysToRemove.indexOf(val) == -1) {
                                acc.push(val)
                            }
                            return acc
                        }, [])
                        startCleaning()
                    }, (e) => {
                        cleanWords.cleanRecords.aloneOccupations(fileName).then((val) => {
                            changes.concat(val);
                            return makeChanges()
                        })
                    })
                }
                startCleaning()


            })

        })
}
router.get('/files/:fileName/:userId/indicator/digitel/educationalInstitutes', function(req, res, next) {
    var userId = req.params.userId,
        fileName = req.params.fileName;

    DataRecord.aggregate()
        .match({
            fileName: fileName,
            $and: [{
                    $or: [
                        { percentilWeight: { $exists: true } },
                        { percentilImc: { $exists: true } }
                    ]
                },
                { percentilHeight: { $exists: true } }
            ]
        })
        .group({
            "_id": "$educationalInstitute"
        })
        .exec(function(err, data) {
            if (err) {
                return err
            }
            res.json(data)
        })
})

router.get('/files/:fileName/:userId/indicator/digitel/coordinates', function(req, res, next) {
    var userId = req.params.userId,
        fileName = req.params.fileName;
    DataRecord.aggregate()
        .match({
            fileName: fileName,
            $and: [{
                latitude: { $exists: true }
            }, {
                longitude: { $exists: true }
            }]
        })
        .group({
            "_id": { name: "$educationalInstitute", lat: "$latitude", lon: "$longitude" },
        })
        .project({
            _id: "$_id.name",
            lat: "$_id.lat",
            lon: "$_id.lon"
        })
        .exec(function(err, data) {
            if (err) {
                return err
            }
            res.json(data)
        })
})

router.get('/files/:fileName/:userId/indicator/digitel/specific', function(req, res, next) {
    var userId = req.params.userId,
        fileName = req.params.fileName,
        query = JSON.parse(req.query.query);
    if (!query) {
        query = {
            fileName: fileName,
            $and: [{
                    $or: [
                        { percentilWeight: { $exists: true } },
                        { percentilImc: { $exists: true } }
                    ]
                },
                { percentilHeight: { $exists: true } }
            ]
        }
    } else {
        query.fileName = fileName;
        query["$and"] = [{
                $or: [
                    { percentilWeight: { $exists: true } },
                    { percentilImc: { $exists: true } }
                ]
            },
            { percentilHeight: { $exists: true } }
        ]
    }
    var group = {}
    if (!!query.diagnostic) {
        group["_id"] = "$fileName";
        group[query.diagnostic] = groupQueryMongo[query.diagnostic]
        delete query.diagnostic
    } else {
        group = groupQueryMongo
    }
    DataRecord.aggregate()
        .match(query)
        .group(group)
        .exec(function(err, results) {
            res.json(results)
        })
})

var groupQueryMongo = {
    "_id": "$fileName",
    // 3rd - 97th
    "Riesgo de deficit de peso y estatura alta": {
        $sum: {
            $cond: [{
                $or: [{
                    $and: [{
                        $eq: ["$percentilWeight", "3rd"]
                    }, {
                        $lt: ["$age", 10]
                    }, {
                        $or: [{
                            $eq: ["$percentilHeight", "97th"]
                        }]
                    }]
                }, {
                    $and: [{
                        $eq: ["$percentilImc", "3rd"]
                    }, {
                        $gt: ["$age", 9]
                    }, {
                        $or: [{
                            $eq: ["$percentilHeight", "97th"]
                        }]
                    }],
                }]
            }, 1, 0]
        }
    },
    // 3 - 85
    // "Riesgo de deficit de peso y estatura medianamente alta": {
    //     $sum: {
    //         $cond: [{
    //             $and: [{ $eq: ["$percentilWeight", "3rd"] }, {
    //                 $eq: [
    //                     "$percentilHeight", "85th"
    //                 ]
    //             }]
    //         }, 1, 0]
    //     }
    // },
    // // 97 - median
    // 3rd - median
    "Riesgo de deficit de peso y estatura normal": {
        $sum: {
            $cond: [{
                $or: [{
                    $and: [{
                        $eq: ["$percentilWeight", "3rd"]
                    }, {
                        $lt: ["$age", 10]
                    }, {
                        $or: [{
                            $eq: ["$percentilHeight", "85th"]
                        }, {
                            $eq: ["$percentilHeight", "median"]
                        }]
                    }]
                }, {
                    $and: [{
                        $eq: ["$percentilImc", "3rd"]
                    }, {
                        $gt: ["$age", 9]
                    }, {
                        $or: [{
                            $eq: ["$percentilHeight", "85th"]
                        }, {
                            $eq: ["$percentilHeight", "median"]
                        }]
                    }],
                }]
            }, 1, 0]
        }
    },
    // 3rd - 15th
    "Riesgo de deficit de peso y estatura baja": {
        $sum: {
            $cond: [{
                $or: [{
                    $and: [{
                        $eq: ["$percentilWeight", "3rd"]
                    }, {
                        $lt: ["$age", 10]
                    }, {
                        $or: [{
                            $eq: ["$percentilHeight", "3rd"]
                        }, {
                            $eq: ["$percentilHeight", "15th"]
                        }]
                    }]
                }, {
                    $and: [{
                        $eq: ["$percentilImc", "3rd"]
                    }, {
                        $gt: ["$age", 9]
                    }, {
                        $or: [{
                            $eq: ["$percentilHeight", "3rd"]
                        }, {
                            $eq: ["$percentilHeight", "15th"]
                        }]
                    }],
                }]
            }, 1, 0]
        }
    },
    // 3 - 3
    // "Riesgo de deficit de peso y baja estatura": {
    //     $sum: {
    //         $cond: [{
    //             $and: [{ $eq: ["$percentilWeight", "3rd"] }, {
    //                 $eq: [
    //                     "$percentilHeight", "3rd"
    //                 ]
    //             }]
    //         }, 1, 0]
    //     }
    // }
    // 15 - 97
    "Bajo peso con estatura alta": {
        $sum: {
            $cond: [{
                $or: [{
                    $and: [{
                        $eq: ["$percentilWeight", "15th"]
                    }, {
                        $lt: ["$age", 10]
                    }, {
                        $or: [{
                            $eq: ["$percentilHeight", "97th"]
                        }]
                    }]
                }, {
                    $and: [{
                        $eq: ["$percentilImc", "15th"]
                    }, {
                        $gt: ["$age", 9]
                    }, {
                        $or: [{
                            $eq: ["$percentilHeight", "97th"]
                        }]
                    }],
                }]
            }, 1, 0]
        }
    },
    // 15 - 85
    // "Bajo peso y estatura medianamente alta": {
    //     $sum: {
    //         $cond: [{
    //             $and: [{ $eq: ["$percentilWeight", "15th"] }, {
    //                 $eq: [
    //                     "$percentilHeight", "85th"
    //                 ]
    //             }]
    //         }, 1, 0]
    //     }
    // 15th - median
    "Bajo peso con estatura normal": {
        $sum: {
            $cond: [{
                $or: [{
                    $and: [{
                        $eq: ["$percentilWeight", "15th"]
                    }, {
                        $lt: ["$age", 10]
                    }, {
                        $or: [{
                            $eq: ["$percentilHeight", "85th"]
                        }, {
                            $eq: ["$percentilHeight", "median"]
                        }]
                    }]
                }, {
                    $and: [{
                        $eq: ["$percentilImc", "15th"]
                    }, {
                        $gt: ["$age", 9]
                    }, {
                        $or: [{
                            $eq: ["$percentilHeight", "85th"]
                        }, {
                            $eq: ["$percentilHeight", "median"]
                        }]
                    }],
                }]
            }, 1, 0]
        }
    },
    // 15th - 15th
    "Bajo peso con riesgo de estatura baja": {
        $sum: {
            $cond: [{
                $or: [{
                    $and: [{
                        $eq: ["$percentilWeight", "15th"]
                    }, {
                        $lt: ["$age", 10]
                    }, {
                        $or: [{
                            $eq: ["$percentilHeight", "15th"]
                        }]
                    }]
                }, {
                    $and: [{
                        $eq: ["$percentilImc", "15th"]
                    }, {
                        $gt: ["$age", 9]
                    }, {
                        $or: [{
                            $eq: ["$percentilHeight", "15th"]
                        }]
                    }],
                }]
            }, 1, 0]
        }
    },
    // 15th - 3rd
    "Bajo peso con estatura baja": {
        $sum: {
            $cond: [{
                $or: [{
                    $and: [{
                        $eq: ["$percentilWeight", "15th"]
                    }, {
                        $lt: ["$age", 10]
                    }, {
                        $or: [{
                            $eq: ["$percentilHeight", "3rd"]
                        }]
                    }]
                }, {
                    $and: [{
                        $eq: ["$percentilImc", "15th"]
                    }, {
                        $gt: ["$age", 9]
                    }, {
                        $or: [{
                            $eq: ["$percentilHeight", "3rd"]
                        }]
                    }],
                }]
            }, 1, 0]
        }
    },
    // median - 97th
    "Peso adecuado con estatura alta": {
        $sum: {
            $cond: [{
                $or: [{
                    $and: [{
                        $eq: ["$percentilWeight", "median"]
                    }, {
                        $lt: ["$age", 10]
                    }, {
                        $or: [{
                            $eq: ["$percentilHeight", "97th"]
                        }]
                    }]
                }, {
                    $and: [{
                        $eq: ["$percentilImc", "median"]
                    }, {
                        $gt: ["$age", 9]
                    }, {
                        $or: [{
                            $eq: ["$percentilHeight", "97th"]
                        }]
                    }],
                }]
            }, 1, 0]
        }
    },
    // median - 85th
    "Peso adecuado con riesgo de estatura alta": {
        $sum: {
            $cond: [{
                $or: [{
                    $and: [{
                        $eq: ["$percentilWeight", "median"]
                    }, {
                        $lt: ["$age", 10]
                    }, {
                        $or: [{
                            $eq: ["$percentilHeight", "85th"]
                        }]
                    }]
                }, {
                    $and: [{
                        $eq: ["$percentilImc", "median"]
                    }, {
                        $gt: ["$age", 9]
                    }, {
                        $or: [{
                            $eq: ["$percentilHeight", "85th"]
                        }]
                    }],
                }]
            }, 1, 0]
        }
    },
    // median - median
    "Peso adecuado con estatura normal": {
        $sum: {
            $cond: [{
                $or: [{
                    $and: [{
                        $eq: ["$percentilWeight", "median"]
                    }, {
                        $lt: ["$age", 10]
                    }, {
                        $or: [{
                            $eq: ["$percentilHeight", "median"]
                        }]
                    }]
                }, {
                    $and: [{
                        $eq: ["$percentilImc", "median"]
                    }, {
                        $gt: ["$age", 9]
                    }, {
                        $or: [{
                            $eq: ["$percentilHeight", "median"]
                        }]
                    }],
                }]
            }, 1, 0]
        }
    },
    // median - 15th
    "Peso adecuado con riesgo de estatura baja": {
        $sum: {
            $cond: [{
                $or: [{
                    $and: [{
                        $eq: ["$percentilWeight", "median"]
                    }, {
                        $lt: ["$age", 10]
                    }, {
                        $or: [{
                            $eq: ["$percentilHeight", "15th"]
                        }]
                    }]
                }, {
                    $and: [{
                        $eq: ["$percentilImc", "median"]
                    }, {
                        $gt: ["$age", 9]
                    }, {
                        $or: [{
                            $eq: ["$percentilHeight", "15th"]
                        }]
                    }],
                }]
            }, 1, 0]
        }
    },
    // median - 3rd
    "Peso adecuado con estatura baja": {
        $sum: {
            $cond: [{
                $or: [{
                    $and: [{
                        $eq: ["$percentilWeight", "median"]
                    }, {
                        $lt: ["$age", 10]
                    }, {
                        $or: [{
                            $eq: ["$percentilHeight", "3rd"]
                        }]
                    }]
                }, {
                    $and: [{
                        $eq: ["$percentilImc", "median"]
                    }, {
                        $gt: ["$age", 9]
                    }, {
                        $or: [{
                            $eq: ["$percentilHeight", "3rd"]
                        }]
                    }],
                }]
            }, 1, 0]
        }
    },
    // 85th - 97th
    "Obesidad con estatura alta": {
        $sum: {
            $cond: [{
                $or: [{
                    $and: [{
                        $eq: ["$percentilWeight", "85th"]
                    }, {
                        $lt: ["$age", 10]
                    }, {
                        $or: [{
                            $eq: ["$percentilHeight", "97th"]
                        }]
                    }]
                }, {
                    $and: [{
                        $eq: ["$percentilImc", "85th"]
                    }, {
                        $gt: ["$age", 9]
                    }, {
                        $or: [{
                            $eq: ["$percentilHeight", "97th"]
                        }]
                    }],
                }]
            }, 1, 0]
        }
    },
    // 85th - 85th - median 
    "Obesidad con estatura normal": {
        $sum: {
            $cond: [{
                $or: [{
                    $and: [{
                        $eq: ["$percentilWeight", "85th"]
                    }, {
                        $lt: ["$age", 10]
                    }, {
                        $or: [{
                            $eq: ["$percentilHeight", "85th"]
                        }, {
                            $eq: ["$percentilHeight", "median"]
                        }]
                    }]
                }, {
                    $and: [{
                        $eq: ["$percentilImc", "85th"]
                    }, {
                        $gt: ["$age", 9]
                    }, {
                        $or: [{
                            $eq: ["$percentilHeight", "85th"]
                        }, {
                            $eq: ["$percentilHeight", "median"]
                        }]
                    }],
                }]
            }, 1, 0]
        }
    },
    // 85th - 15th - 3rd
    "Obesidad con riesgo de estatura baja": {
        $sum: {
            $cond: [{
                $or: [{
                    $and: [{
                        $eq: ["$percentilWeight", "85th"]
                    }, {
                        $lt: ["$age", 10]
                    }, {
                        $or: [{
                            $eq: ["$percentilHeight", "15th"]
                        }, {
                            $eq: ["$percentilHeight", "3rd"]
                        }]
                    }]
                }, {
                    $and: [{
                        $eq: ["$percentilImc", "85th"]
                    }, {
                        $gt: ["$age", 9]
                    }, {
                        $or: [{
                            $eq: ["$percentilHeight", "15th"]
                        }, {
                            $eq: ["$percentilHeight", "3rd"]
                        }]
                    }],
                }]
            }, 1, 0]
        }
    },
    // 85 - 15th
    // "Obesidad y estatura medianamente baja": {
    //     $sum: {
    //         $cond: [{
    //             $and: [{ $eq: ["$percentilWeight", "85th"] }, {
    //                 $eq: [
    //                     "$percentilHeight", "15th"
    //                 ]
    //             }]
    //         }, 1, 0]
    //     }
    // },
    // 85 - 3
    // "Obesidad y baja estatura": {
    //     $sum: {
    //         $cond: [{
    //             $and: [{ $eq: ["$percentilWeight", "85th"] }, {
    //                 $eq: [
    //                     "$percentilHeight", "3rd"
    //                 ]
    //             }]
    //         }, 1, 0]
    //     }
    // },
    "Sobrepeso y estatura normal": {
        $sum: {
            $cond: [{
                    $or: [{
                        $and: [{
                            $eq: ["$percentilWeight", "97th"]
                        }, {
                            $lt: ["$age", 10]
                        }, {
                            $or: [{
                                $eq: ["$percentilHeight", "97th"]
                            }, {
                                $eq: ["$percentilHeight", "85th"]
                            }, {
                                $eq: ["$percentilHeight", "median"]
                            }]
                        }]
                    }, {
                        $and: [{
                            $eq: ["$percentilImc", "97th"]
                        }, {
                            $gt: ["$age", 9]
                        }, {
                            $or: [{
                                $eq: ["$percentilHeight", "97th"]
                            }, {
                                $eq: ["$percentilHeight", "85th"]
                            }, {
                                $eq: ["$percentilHeight", "median"]
                            }]
                        }],
                    }]
                },
                1,
                0
            ]
        }
    },
    // // 97 - 85
    // "Sobrepeso y estatura medianamente alta": {
    //     $sum: {
    //         $cond: [{
    //             $and: [{ $eq: ["$percentilWeight", "97th"] }, {
    //                 $eq: [
    //                     "$percentilHeight", "85th"
    //                 ]
    //             }]
    //         }, 1, 0]
    //     }
    // },
    // "Sobrepeso y estatura regular": {
    //     $sum: {
    //         $cond: [{
    //             $and: [{ $eq: ["$percentilWeight", "97th"] }, {
    //                 $eq: [
    //                     "$percentilHeight", "median"
    //                 ]
    //             }]
    //         }, 1, 0]
    //     }
    // },
    // 97th - 97th 85th median
    "Sobrepeso con riesgo de estatura baja": {
        $sum: {
            $cond: [{
                $or: [{
                    $and: [{
                        $eq: ["$percentilWeight", "97th"]
                    }, {
                        $lt: ["$age", 10]
                    }, {
                        $or: [{
                            $eq: ["$percentilHeight", "15th"]
                        }, {
                            $eq: ["$percentilHeight", "3rd"]
                        }]
                    }]
                }, {
                    $and: [{
                        $eq: ["$percentilImc", "97th"]
                    }, {
                        $gt: ["$age", 9]
                    }, {
                        $or: [{
                            $eq: ["$percentilHeight", "15th"]
                        }, {
                            $eq: ["$percentilHeight", "3rd"]
                        }]
                    }],
                }]
            }, 1, 0]
        }
    },
    // 97 - 3
    // "Sobrepeso y baja estatura": {
    //     $sum: {
    //         $cond: [{
    //             $and: [{ $eq: ["$percentilWeight", "97th"] }, {
    //                 $eq: [
    //                     "$percentilHeight", "3rd"
    //                 ]
    //             }]
    //         }, 1, 0]
    //     }
    // },

}


// REFERIDOS TOTALES
function referedByByTotal(fileName, callback) {
    DataRecord.aggregate()
        .match({ $and: [{ fileName: fileName }, { referedBy: { $exists: true } }, { referedBy: { $ne: '' } }, { referedBy: { $ne: null } }] })
        .project({
            refereds: {
                $ifNull: ['$recommended.referedBy', '$referedBy']
            }
        })
        .group({
            _id: "$refereds",
            count: { $sum: 1 }
        })
        .sort({ count: -1 })
        .project({
            _id: 0,
            name: "$_id",
            y: "$count"
        })
        .exec(function(err, results) {
            if (callback) callback(results)
        })
}

// OCUPACIONES TOTALES
function occupationByTotal(fileName, callback) {
    DataRecord.aggregate()
        .match({ $and: [{ fileName: fileName }, { occupation: { $exists: true } }, { occupation: { $ne: '' } }, { occupation: { $ne: null } }] })
        .project({
            occupations: {
                $ifNull: ['$recommended.occupation', '$occupation']
            }
        })
        .group({
            _id: "$occupations",
            count: { $sum: 1 }
        })
        .sort({ count: -1 })
        .project({
            _id: 0,
            name: "$_id",
            y: "$count"
        })
        .exec(function(err, results) {
            if (callback) callback(results);
        })
}

// GENEROS TOTALES
function genreByTotal(fileName, callback) {
    DataRecord.aggregate()
        .match({ $and: [{ fileName: fileName }, { genre: { $exists: true } }, { genre: { $ne: '' } }, { genre: { $ne: null } }] })
        .project({
            genres: {
                $ifNull: ['$recommended.genre', '$genre']
            }
        })
        .group({
            _id: "$genres",
            count: { $sum: 1 }
        })
        .sort({ count: -1 })
        .project({
            _id: 0,
            name: "$_id",
            y: "$count"
        })
        .exec(function(err, results) {
            if (callback) callback(results)
        })
}

// SEGUROS TOTALES
function insuranceByTotal(fileName, callback) {
    DataRecord.aggregate()
        .match({ $and: [{ fileName: fileName }, { insurance: { $exists: true } }, { insurance: { $ne: '' } }, { insurance: { $ne: null } }] })
        .project({
            insurances: {
                $ifNull: ['$recommended.insurance', '$insurance']
            }
        })
        .group({
            _id: "$insurances",
            count: { $sum: 1 }
        })
        .sort({ count: -1 })
        .project({
            _id: 0,
            name: "$_id",
            y: "$count"
        })
        .exec(function(err, results) {
            if (callback) callback(results)
        })
}

// EDADES TOTALES
function ageByTotal(fileName, callback) {
    DataRecord.aggregate()
        .match({ $and: [{ fileName: fileName }, { age: { $exists: true } }, { age: { $ne: '' } }, { age: { $ne: null } }] })
        .group({
            _id: "$age",
            count: { $sum: 1 }
        })
        .sort({ count: -1 })
        .project({
            _id: 0,
            name: "$_id",
            data: ["$count"]
        })
        .exec(function(err, results) {
            if (callback) callback(results)
        })
}

// MOTIVOS TOTALES
function motivesByTotal(fileName, callback) {
    DataRecord.aggregate()
        .match({ $and: [{ fileName: fileName }, { motive: { $exists: true } }, { motive: { $ne: '' } }, { motive: { $ne: null } }] })
        .project({
            motives: {
                $ifNull: ['$recommended.motive', '$motive']
            }
        })
        .group({
            _id: "$motives",
            count: { $sum: 1 }
        })
        .sort({ count: -1 })
        .project({
            _id: 0,
            name: "$_id",
            y: "$count"
        })
        .exec(function(err, results) {
            if (callback) callback(results)
        })
}

// DIAGNOSTICOS TOTALES
function diagnosisByTotal(fileName, callback) {
    DataRecord.aggregate()
        .match({ $and: [{ fileName: fileName }, { diagnosis: { $exists: true } }, { diagnosis: { $ne: '' } }, { diagnosis: { $ne: null } }] })
        .project({
            diagnosiss: {
                $ifNull: ['$recommended.diagnosis', '$diagnosis']
            }
        })
        .group({
            _id: "$diagnosiss",
            count: { $sum: 1 }
        })
        .sort({ count: -1 })
        .project({
            _id: 0,
            name: "$_id",
            y: "$count"
        })
        .exec(function(err, results) {
            if (callback) callback(results)
        })
}

// EDAD POR ANO
function ageByYear(fileName, callback) {
    DataRecord.aggregate()
        .match({ $and: [{ fileName: fileName }, { date: { $exists: true } }, { date: { $ne: '' } }, { date: { $ne: null } }, { age: { $exists: true } }, { age: { $ne: '' } }, { age: { $ne: null } }] })
        .project({
            age: {
                $ifNull: ['$recommended.age', '$age']
            },
            date: "$date"
        })
        .group({
            _id: {
                "year": { $year: "$date" },
                "age": "$age"
            },
            count: { $sum: 1 }
        })
        .group({
            _id: "$_id.year",
            ages: {
                $push: {
                    age: "$_id.age",
                    count: "$count"
                }
            },
            total: { $sum: "$count" }
        })
        .sort({ _id: -1 })
        .exec(function(err, results) {
            if (callback) callback(results);
        })
}

// SEGUROS POR ANO
function insuranceByYear(fileName, callback) {
    DataRecord.aggregate()
        .match({ $and: [{ fileName: fileName }, { date: { $exists: true } }, { date: { $ne: '' } }, { date: { $ne: null } }, { insurance: { $exists: true } }, { insurance: { $ne: '' } }, { insurance: { $ne: null } }] })
        .project({
            insurance: {
                $ifNull: ['$recommended.insurance', '$insurance']
            },
            date: "$date"
        })
        .group({
            _id: {
                "year": { $year: "$date" },
                "insurance": "$insurance"
            },
            count: { $sum: 1 }
        })
        .group({
            _id: "$_id.year",
            insurances: {
                $push: {
                    insurance: "$_id.insurance",
                    count: "$count"
                }
            },
            total: { $sum: "$count" }

        })
        .sort({ _id: -1 })
        .exec(function(err, results) {
            if (callback) callback(results)
        })
}

// SEGUROS POR ANO
function genreByYear(fileName, callback) {
    DataRecord.aggregate()
        .match({ $and: [{ fileName: fileName }, { date: { $exists: true } }, { date: { $ne: '' } }, { date: { $ne: null } }, { genre: { $exists: true } }, { genre: { $ne: '' } }, { genre: { $ne: null } }] })
        .project({
            genre: {
                $ifNull: ['$recommended.genre', '$genre']
            },
            date: "$date"
        })
        .group({
            _id: {
                "year": { $year: "$date" },
                "genre": "$genre"
            },
            count: { $sum: 1 }
        })
        .group({
            _id: "$_id.year",
            genres: {
                $push: {
                    genre: "$_id.genre",
                    count: "$count"
                }
            },
            total: { $sum: "$count" }
        })
        .sort({ _id: -1 })
        .exec(function(err, results) {
            if (callback) callback(results)
        })
}

// OCUPACION POR ANO
function occupationByYear(fileName, callback) {
    DataRecord.aggregate()
        .match({ $and: [{ fileName: fileName }, { date: { $exists: true } }, { date: { $ne: '' } }, { date: { $ne: null } }, { occupation: { $exists: true } }, { occupation: { $ne: '' } }, { occupation: { $ne: null } }] })
        .project({
            occupation: {
                $ifNull: ['$recommended.occupation', '$occupation']
            },
            date: "$date"
        })
        .group({
            _id: {
                "year": { $year: "$date" },
                "occupation": "$occupation"
            },
            count: { $sum: 1 }
        })
        .group({
            _id: "$_id.year",
            occupations: {
                $push: {
                    occupation: "$_id.occupation",
                    count: "$count"
                }
            },
            total: { $sum: "$count" }

        })
        .sort({ _id: -1 })
        .exec(function(err, results) {
            if (callback) callback(results)
        })
}

// REFERIDOS POR ANO
function referedByYear(fileName, callback) {
    DataRecord.aggregate()
        .match({ $and: [{ fileName: fileName }, { date: { $exists: true } }, { date: { $ne: '' } }, { date: { $ne: null } }, { referedBy: { $exists: true } }, { referedBy: { $ne: '' } }, { referedBy: { $ne: null } }] })
        .project({
            referedBy: {
                $ifNull: ['$recommended.referedBy', '$referedBy']
            },
            date: "$date"
        })
        .group({
            _id: {
                "year": { $year: "$date" },
                "referedBy": "$referedBy"
            },
            count: { $sum: 1 }
        })
        .group({
            _id: "$_id.year",
            referedBys: {
                $push: {
                    referedBy: "$_id.referedBy",
                    count: "$count"
                }
            },
            total: { $sum: "$count" }

        })
        .sort({ _id: -1 })
        .exec(function(err, results) {
            if (callback) callback(results)
        })
}

// MOTIVO POR ANO
function motiveByYear(fileName, callback) {
    DataRecord.aggregate()
        .match({ $and: [{ fileName: fileName }, { date: { $exists: true } }, { date: { $ne: '' } }, { date: { $ne: null } }, { motive: { $exists: true } }, { motive: { $ne: '' } }, { motive: { $ne: null } }] })
        .project({
            motive: {
                $ifNull: ['$recommended.motive', '$motive']
            },
            date: "$date"
        })
        .group({
            _id: {
                "year": { $year: "$date" },
                "motive": "$motive"
            },
            count: { $sum: 1 }
        })
        .group({
            _id: "$_id.year",
            motives: {
                $push: {
                    motive: "$_id.motive",
                    count: "$count"
                }
            },
            total: { $sum: "$count" }
        })
        .sort({ _id: -1 })
        .exec(function(err, results) {
            if (callback) callback(results)
        })
}

// DIAGNOSTICO POR ANO
function diagnosisByYear(fileName, callback) {
    DataRecord.aggregate()
        .match({ $and: [{ fileName: fileName }, { date: { $exists: true } }, { date: { $ne: '' } }, { date: { $ne: null } }, { diagnosis: { $exists: true } }, { diagnosis: { $ne: '' } }, { diagnosis: { $ne: null } }] })
        .project({
            diagnosis: {
                $ifNull: ['$recommended.diagnosis', '$diagnosis']
            },
            date: "$date"
        })
        .group({
            _id: {
                "year": { $year: "$date" },
                "diagnosis": "$diagnosis"
            },
            count: { $sum: 1 }
        })
        .group({
            _id: "$_id.year",
            diagnosiss: {
                $push: {
                    diagnosis: "$_id.diagnosis",
                    count: "$count"
                }
            },
            total: { $sum: "$count" }
        })
        .sort({ _id: -1 })
        .exec(function(err, results) {
            if (callback) callback(results)
        })
}

// EDAD POR GENERO
function genreAge(fileName, callback) {
    DataRecord.aggregate()
        .match({ $and: [{ fileName: fileName }, { genre: { $exists: true } }, { genre: { $ne: '' } }, { genre: { $ne: null } }, { age: { $exists: true } }, { age: { $ne: '' } }, { age: { $ne: null } }] })
        .project({
            age: {
                $ifNull: ['$recommended.age', '$age']
            },
            genre: { $ifNull: ['$recommended.genre', '$genre'] }
        })
        .group({
            _id: {
                "genre": "$genre",
                "age": "$age"
            },
            count: { $sum: 1 }
        })
        .group({
            _id: "$_id.genre",
            ages: {
                $push: {
                    age: "$_id.age",
                    count: "$count"
                }
            },
            total: { $sum: "$count" }
        })
        .sort({ _id: -1 })
        .exec(function(err, results) {
            if (callback) callback(results)
        })
}

// SEGURO POR GENERO
function genreInsurance(fileName, callback) {
    DataRecord.aggregate()
        .match({ $and: [{ fileName: fileName }, { genre: { $exists: true } }, { genre: { $ne: '' } }, { genre: { $ne: null } }, { insurance: { $exists: true } }, { insurance: { $ne: '' } }, { insurance: { $ne: null } }] })
        .project({
            insurance: {
                $ifNull: ['$recommended.insurance', '$insurance']
            },
            genre: {
                $ifNull: ['$recommended.genre', '$genre']
            }
        })
        .group({
            _id: {
                "genre": "$genre",
                "insurance": "$insurance"
            },
            count: { $sum: 1 }
        })
        .group({
            _id: "$_id.genre",
            insurances: {
                $push: {
                    insurance: "$_id.insurance",
                    count: "$count"
                }
            },
            total: { $sum: "$count" }
        })
        .sort({ _id: -1 })
        .exec(function(err, results) {
            if (callback) callback(results)
        })
}

// OCUPACIONES POR GENERO
function genreOccupation(fileName, callback) {
    DataRecord.aggregate()
        .match({ $and: [{ fileName: fileName }, { genre: { $exists: true } }, { genre: { $ne: '' } }, { genre: { $ne: null } }, { occupation: { $exists: true } }, { occupation: { $ne: '' } }, { occupation: { $ne: null } }] })
        .project({
            occupation: {
                $ifNull: ['$recommended.occupation', '$occupation']
            },
            genre: {
                $ifNull: ['$recommended.genre', '$genre']
            }
        })
        .group({
            _id: {
                "genre": "$genre",
                "occupation": "$occupation"
            },
            count: { $sum: 1 }
        })
        .group({
            _id: "$_id.genre",
            occupations: {
                $push: {
                    occupation: "$_id.occupation",
                    count: "$count"
                }
            },
            total: { $sum: "$count" }
        })
        .sort({ _id: -1 })
        .exec(function(err, results) {
            if (callback) callback(results)
        })
}

// REFERIDOS POR GENERO
function genreReferedBy(fileName, callback) {
    DataRecord.aggregate()
        .match({ $and: [{ fileName: fileName }, { genre: { $exists: true } }, { genre: { $ne: '' } }, { genre: { $ne: null } }, { referedBy: { $exists: true } }, { referedBy: { $ne: '' } }, { referedBy: { $ne: null } }] })
        .project({
            referedBy: {
                $ifNull: ['$recommended.referedBy', '$referedBy']
            },
            genre: {
                $ifNull: ['$recommended.genre', '$genre']
            }
        })
        .group({
            _id: {
                "genre": "$genre",
                "referedBy": "$referedBy"
            },
            count: { $sum: 1 }
        })
        .group({
            _id: "$_id.genre",
            referedBys: {
                $push: {
                    referedBy: "$_id.referedBy",
                    count: "$count"
                }
            },
            total: { $sum: "$count" }

        })
        .sort({ _id: -1 })
        .exec(function(err, results) {
            if (callback) callback(results)
        })
}

// MOTIVO POR GENERO
function genreMotive(fileName, callback) {
    DataRecord.aggregate()
        .match({ $and: [{ fileName: fileName }, { genre: { $exists: true } }, { genre: { $ne: '' } }, { genre: { $ne: null } }, { motive: { $exists: true } }, { motive: { $ne: '' } }, { motive: { $ne: null } }] })
        .project({
            motive: {
                $ifNull: ['$recommended.motive', '$motive']
            },
            genre: "$genre"
        })
        .group({
            _id: {
                "genre": "$genre",
                "motive": "$motive"
            },
            count: { $sum: 1 }
        })
        .group({
            _id: "$_id.genre",
            motives: {
                $push: {
                    motive: "$_id.motive",
                    count: "$count"
                }
            },
            total: { $sum: "$count" }
        })
        .sort({ _id: -1 })
        .exec(function(err, results) {
            if (callback) callback(results)
        })
}

// DIAGNOSTICO POR GENERO
function genreDiagnosis(fileName, callback) {
    DataRecord.aggregate()
        .match({ $and: [{ fileName: fileName }, { genre: { $exists: true } }, { genre: { $ne: '' } }, { genre: { $ne: null } }, { diagnosis: { $exists: true } }, { diagnosis: { $ne: '' } }, { diagnosis: { $ne: null } }] })
        .project({
            diagnosis: {
                $ifNull: ['$recommended.diagnosis', '$diagnosis']
            },
            genre: "$genre"
        })
        .group({
            _id: {
                "genre": "$genre",
                "diagnosis": "$diagnosis"
            },
            count: { $sum: 1 }
        })
        .group({
            _id: "$_id.genre",
            diagnosiss: {
                $push: {
                    diagnosis: "$_id.diagnosis",
                    count: "$count"
                }
            },
            total: { $sum: "$count" }
        })
        .sort({ _id: -1 })
        .exec(function(err, results) {
            if (callback) callback(results)
        })
}

// MOTIVO PRINCIPAL POR EDAD
function ageMotive(fileName, callback) {
    DataRecord.aggregate()
        .match({ $and: [{ fileName: fileName }, { age: { $exists: true } }, { age: { $ne: '' } }, { age: { $ne: null } }, { motive: { $exists: true } }, { motive: { $ne: "" } }, { motive: { $ne: null } }] })
        .project({
            motive: {
                $ifNull: ['$recommended.motive', '$motive']
            },
            age: "$age"
        })
        .group({
            _id: {
                "age": "$age",
                "motive": "$motive"
            },
            count: { $sum: 1 }
        })
        .group({
            _id: "$_id.age",
            motives: {
                $push: {
                    motive: "$_id.motive",
                    count: "$count"
                }
            }
        })
        .unwind("motives")
        .sort({ "motives.count": -1 })
        .group({
            _id: "$_id",
            motives: {
                $push: "$motives"
            }
        })
        .sort({ count: -1 })
        .project({
            motive: {
                $arrayElemAt: ["$motives", 0]
            }
        })
        .exec(function(err, results) {
            if (callback) callback(results)
        })
}

// DIAGNOSTICO PRINCIPAL POR EDAD
function ageDiagnosis(fileName, callback) {
    DataRecord.aggregate()
        .match({ $and: [{ fileName: fileName }, { age: { $exists: true } }, { age: { $ne: '' } }, { age: { $ne: null } }, { diagnosis: { $exists: true } }, { diagnosis: { $ne: "" } }, { diagnosis: { $ne: null } }] })
        .project({
            diagnosis: {
                $ifNull: ['$recommended.diagnosis', '$diagnosis']
            },
            age: "$age"
        })
        .group({
            _id: {
                "age": "$age",
                "diagnosis": "$diagnosis"
            },
            count: { $sum: 1 }
        })
        .group({
            _id: "$_id.age",
            diagnosiss: {
                $push: {
                    diagnosis: "$_id.diagnosis",
                    count: "$count"
                }
            }
        })
        .unwind("diagnosiss")
        .sort({ "diagnosiss.count": -1 })
        .group({
            _id: "$_id",
            diagnosiss: {
                $push: "$diagnosiss"
            }
        })
        .sort({ count: -1 })
        .project({
            diagnosis: {
                $arrayElemAt: ["$diagnosiss", 0]
            }
        })
        .exec(function(err, results) {
            if (callback) callback(results)
        })
}

// EDAD PRINCIPAL POR MOTIVOS
function motiveAge(fileName, callback) {
    DataRecord.aggregate()
        .match({ $and: [{ fileName: fileName }, { age: { $exists: true } }, { age: { $ne: '' } }, { age: { $ne: null } }, { motive: { $exists: true } }, { motive: { $ne: "" } }, { motive: { $ne: null } }] })
        .project({
            motive: {
                $ifNull: ['$recommended.motive', '$motive']
            },
            age: "$age"
        })
        .group({
            _id: {
                "age": "$age",
                "motive": "$motive"
            },
            count: { $sum: 1 }
        })
        .group({
            _id: "$_id.motive",
            ages: {
                $push: {
                    age: "$_id.age",
                    count: "$count"
                }
            }
        })
        .unwind("ages")
        .sort({ "ages.count": -1 })
        .group({
            _id: "$_id",
            ages: {
                $push: "$ages"
            }
        })
        .sort({ count: -1 })
        .project({
            age: {
                $arrayElemAt: ["$ages", 0]
            }
        })
        .exec(function(err, results) {
            if (callback) callback(results)
        })
}

// EDAD PRINCIPAL POR DIAGNOSTICOS
function diagnosisAge(fileName, callback) {
    DataRecord.aggregate()
        .match({ $and: [{ fileName: fileName }, { age: { $exists: true } }, { age: { $ne: '' } }, { age: { $ne: null } }, { diagnosis: { $exists: true } }, { diagnosis: { $ne: "" } }, { diagnosis: { $ne: null } }] })
        .project({
            diagnosis: {
                $ifNull: ['$recommended.diagnosis', '$diagnosis']
            },
            age: "$age"
        })
        .group({
            _id: {
                "age": "$age",
                "diagnosis": "$diagnosis"
            },
            count: { $sum: 1 }
        })
        .group({
            _id: "$_id.diagnosis",
            ages: {
                $push: {
                    age: "$_id.age",
                    count: "$count"
                }
            }
        })
        .unwind("ages")
        .sort({ "ages.count": -1 })
        .group({
            _id: "$_id",
            ages: {
                $push: "$ages"
            }
        })
        .sort({ count: -1 })
        .project({
            age: {
                $arrayElemAt: ["$ages", 0]
            }
        })
        .exec(function(err, results) {
            if (callback) callback(results)
        })
}

// DIAGNOSTICO PRINCIPAL POR MOTIVO
function motiveDiagnosis(fileName, callback) {
    DataRecord.aggregate()
        .match({ $and: [{ fileName: "godayol" }, { diagnosis: { $exists: true } }, { diagnosis: { $ne: '' } }, { diagnosis: { $ne: null } }, { motive: { $exists: true } }, { motive: { $ne: "" } }, { motive: { $ne: null } }] })
        .project({
            motive: {
                $ifNull: ['$recommended.motive', '$motive']
            },
            diagnosis: {
                $ifNull: ['$recommended.diagnosis', '$diagnosis']
            }
        })
        .group({
            _id: {
                "diagnosis": "$diagnosis",
                "motive": "$motive"
            },
            count: { $sum: 1 }
        })
        .group({
            _id: "$_id.motive",
            diagnosiss: {
                $push: {
                    diagnosis: "$_id.diagnosis",
                    count: "$count"
                }
            }
        })
        .unwind("diagnosiss")
        .sort({ "diagnosiss.count": -1 })
        .group({
            _id: "$_id",
            diagnosiss: {
                $push: "$diagnosiss"
            }
        })
        .sort({ count: -1 })
        .project({
            diagnosis: {
                $arrayElemAt: ["$diagnosiss", 0]
            }
        })
        .exec(function(err, results) {
            if (callback) callback(results)
        })
}
