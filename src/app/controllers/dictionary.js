var mongoose = require('mongoose'),
	router = require('express').Router(),
	async = require('async'),
	Dictionary = mongoose.model('Dictionary');

module.exports = function(app) {
	app.use('/api/services/dictionary', router);
};

router.get('/', function(req, res, next) {
	Dictionary.findAllAndPopulate(req.user.doctor._id, req.query, function(err, items) {
		res.json(items);
	});
});

router.get('/items/:word', function(req, res, next) {
	Dictionary.find({
			label: new RegExp(req.params.word, "i"),
			$or: [
				{ doctor: { $exists: false } },
				{ doctor: req.user.doctor._id }
			]
		})
		.limit(2)
		.exec(function(err, items) {
			async.map(items, function(item, callback) {
				if (item.parent) {
					Dictionary.populate(item, { path: 'parent' }, function(err, item) {
						if (err) return callback(err);
						callback(null, item)
					})
				} else {
					callback(null, item)
				}
			}, function(err, results) {
				res.json(results);
			});
		});
});

router.put('/:id', function(req, res, next) {
	Dictionary.findOne({name:req.params.id}, function(err, word) {
		var item = new Dictionary();
		item.assign(req.body);

		item.parent = word._id;
		item.doctor = req.user.doctor._id;
		word.items.push(item._id);
		async.parallel({
			item: function(callback) {
				item.save(function(err, item) {
					if (err) return next(err);
					callback(err, item)
				});
			},
			father: function(callback) {
				word.save(function(err, item) {
					if (err) return next(err);
					callback(err, item)
				});
			}
		}, function(err, results) {
			if (err) return next(err);

			res.json(results.item);
		});
	})
});

router.param('item', function(req, res, next, value) {
	Dictionary.findOne({ _id: value })
		.exec(function(err, item) {
			if (err) return next(err);
			if (!item) return next(new Error('ItemRequired'));

			req.item = item;
			next();
		});
});



router.get("/historys", function(req, res, next, value) {
	var docId = "";

	if (req.user && req.user.doctor && req.user.doctor._id) {
		docId = req.user.doctor._id
	};
	Dictionary.findOne({ name: "history" }, function(err, history) {
		if (err) return next(err);
		var query = {
			parent: history._id,
			$or: [
				{ doctor: { $exists: false } },
				{ doctor: docId }
			],
				label: new RegExp(req.params.word, 'i'),
		}
		Dictionary.find(query).limit(7).exec(function(err, historys) {
			if (err) return next(err);
			res.json(historys)
		})
	})
})
router.get('/diagnosis/:word', function(req, res, next) {
	var docId = "";

	if (req.user && req.user.doctor && req.user.doctor._id) {
		docId = req.user.doctor._id
	};
	Dictionary.findOne({ name: "diagnosis" }, function(err, diagnosis) {
		if (err) return next(err);
		var query = {
			parent: diagnosis._id,
			$or: [
				{ doctor: { $exists: false } },
				{ doctor: docId }
			],
			label: new RegExp(req.params.word, 'i'),
		}
		Dictionary.find(query).limit(5).exec(function(err, diagnosis) {
			if (err) return next(err);
			res.json(diagnosis)
		})
	})
});
router.get("/historys/:word", function(req, res, next) {
	var docId = "";

	if (req.user && req.user.doctor && req.user.doctor._id) {
		docId = req.user.doctor._id
	};
	Dictionary.findOne({ name: "history" }, function(err, history) {
		if (err) return next(err);
		var query = {
			parent: history._id,
			$or: [
				{ doctor: { $exists: false } },
				{ doctor: docId }
			]
		}
		if (req.params.word != '&all') {
			query.label =  new RegExp(req.params.word, 'i')
		}

		Dictionary.find(query).exec(function(err, historys) {
			if (err) return next(err);
			res.json(historys)
		})
	})
})
router.get("/historys/childs/:parent/:word", function(req, res, next) {
	var docId = "";

	if (req.user && req.user.doctor && req.user.doctor._id) {
		docId = req.user.doctor._id
	};
	var query = {
		parent: req.params.parent,
		$or: [
			{ doctor: { $exists: false } },
			{ doctor: docId }
		],
		label: new RegExp(req.params.word, 'gi'),
	}
	Dictionary.find(query).limit(4).exec(function(err, historys) {
		if (err) return next(err);
		res.json(historys)
	})
})
router.get("/motives/:word", function(req, res, next) {
	var docId = "";

	if (req.user && req.user.doctor && req.user.doctor._id) {
		docId = req.user.doctor._id
	};
	console.log("doc",docId)
	Dictionary.findOne({ name: "motive" }, function(err, motive) {
		if (err) return next(err);
		var query = {
			parent: motive._id,
			$or: [
				{ doctor: { $exists: false } },
				{ doctor: docId }
			],
			label: new RegExp(req.params.word, 'i'),
		}
		Dictionary.find(query).limit(5).exec(function(err, motives) {
			if (err) return next(err);
			console.log(motives)
			res.json(motives)
		})
	})
})
router.get("/plans/:word", function(req, res, next) {
	var docId = "";
	if (req.user && req.user.doctor && req.user.doctor._id) {
		docId = req.user.doctor._id
	};
	Dictionary.findOne({ name: "plan" }, function(err, plan) {
		if (err) return next(err);
		var query = {
			parent: plan._id,
			$or: [
				{ doctor: { $exists: false } },
				{ doctor: docId }
			],
			label: new RegExp(req.params.word, 'i'),
			
		}
		Dictionary.find(query).limit(5).exec(function(err, plans) {
			if (err) return next(err);
			console.log("plans",plans.length)
			res.json(plans)
		})
	})
})
router.get("/procedures/:word", function(req, res, next) {
	var docId = "";
	if (req.user && req.user.doctor && req.user.doctor._id) {
		docId = req.user.doctor._id
	};
	Dictionary.findOne({ name: "procedures" }, function(err, procedures) {
		if (err) return next(err);
		var query = {
			parent: procedures._id,
			$or: [
				{ doctor: { $exists: false } },
				{ doctor: docId }
			],
			label: new RegExp(req.query.word, 'i'),
			
		}
		Dictionary.find(query).limit(5).exec(function(err, procedures) {
			if (err) return next(err);
			res.json(procedures)
		})
	})
})
