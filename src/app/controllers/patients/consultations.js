var mongoose = require('mongoose'),
	common = require('../../common'),
	document = require('../../packages/document'),
	router = require('express').Router(),
	path = require('path'),
	moment = require('moment'),
	timezone = require('moment-timezone'),
	recordMiddlewares = require('../../common/middlewares/records'),
	multiparty = require('connect-multiparty'),
	fs = require('fs'),
	async = require('async'),
	Consultation = mongoose.model('Consultation'),
	Attachment = mongoose.model('Attachment'),
	Record = mongoose.model('Record'),
	Plan = mongoose.model('Plan'),
	Prescription = mongoose.model('Prescription'),
	Report = mongoose.model('Report'),
	Dictionary = mongoose.model('Dictionary');
var wkhtmltopdf = require('node-wkhtmltopdf');


module.exports = function(app) {
	app.use('/api/patients/', router);
};

router.get('/:identity/consultations', function(req, res, next) {
	var isAllowed = req.patient.doctors.some(function(doctor) {
		return doctor.equals(req.user.doctor._id);
	});

	if (!isAllowed) return next(new Error('NotAllowed'));

	req.user.doctor.getSharedOffices(function(err, offices) {
		var query = {
			patient: req.patient._id,
			$or: [{
				doctor: req.user.doctor._id
			}, {
				office: { $in: offices }
			}]
		};

		if (req.user.doctor.isPremium) {
			query.$or.push({
				doctor: { $in: req.patient.sharedWith }
			});
		}

		Consultation.find(query, null, { $sort: { date: 1 } })
			.populate('records')
			.populate('attachments')
			.populate('reports')
			.populate('prescription')
			.populate('plans')
			.populate('patient')
			.exec(function(err, consultations) {
				if (err) return next(err);
				async.filter(consultations, function(consultation, callback) {
					var isEmpty = ['records', 'attachments', 'reports', 'prescription', 'plans']
						.every(function(prop) {
							return consultation[prop].length == 0;
						}) && (moment(consultation.date).format('YYYY-MM-DD') !== moment().format('YYYY-MM-DD'));
					if (!isEmpty) return callback(consultation);
					consultation.remove(function(err) {
						callback();
					});
				}, function(consultations) {
					Record.populate(consultations, {
						path: 'records.item',
						model: Dictionary
					}, function(err) {
						Record.populate(consultations, {
							path: 'records.item.parent',
							model: Dictionary
						}, function(err) {

							Record.populate(consultations, {
								path: 'records.item.parent.parent',
								model: Dictionary
							}, function(err) {

								res.json(consultations);
							});
						});
					});
				});
			});
	});
});

router.post('/:identity/consultations', function(req, res, next) {

	async.auto({
		consultation: function(callback) {
			Consultation.findOne({
				patient: req.patient._id,
				doctor: req.user.doctor._id,
				date: moment(moment().format('YYYY-MM-DD')).toDate()
			}, function(err, consultation) {
				if (err) return callback(err);
				if (!consultation) {
					consultation = new Consultation();
					consultation.doctor = req.user.doctor._id;
					consultation.patient = req.patient._id;
					consultation.date = moment(moment().format('YYYY-MM-DD')).toDate()

					consultation.save(function(err, consultation) {
						callback(err, consultation);
					});
				} else {
					callback(null, consultation);
				}
			});
		},
		doctor: ['consultation', function(callback, results) {
			if (results.consultation.wasNew) {
				req.user.doctor.consultations.push(results.consultation._id);
				req.user.doctor.save(callback);
			} else {
				callback(null);
			}
		}],
		patient: ['consultation', function(callback, results) {
			if (results.consultation.wasNew) {
				req.patient.status = 'ACTIVE';
				req.patient.consultations.push(results.consultation._id);
				req.patient.save(callback);
			} else {
				callback(null);
			}
		}]
	}, function(err, results) {
		if (err) return next(err);
		res.json(results.consultation);
	});
});

router.get('/:identity/consultations/:consultation/attachments', function(req, res, next) {
	res.json(req.consultation.attachments);
});

router.post('/:identity/consultations/:consultation/attachments', multiparty({ uploadDir: './uploads' }),
	// 4194304 bytes == 4 megabytes
	function(req, res, next) {
		var attachment = new Attachment();

		attachment.doctor = req.user.doctor._id;
		attachment.patient = req.patient._id;
		attachment.consultation = req.consultation._id;

		if (req.files.attachment.size > 4194304) {
			return next(new Error('FileSizeLimit'));
		}

		attachment.name = req.files.attachment.name;
		attachment.path = 'uploads/' + attachment._id + path.extname(req.files.attachment.name);

		attachment.contentType = req.files.attachment.headers['content-type'];

		fs.rename(req.files.attachment.path, path.normalize(__dirname + '/../../../' + attachment.path), function(err) {
			if (err) return next(common.catchUnhandledErrors(err));

			req.consultation.attachments.push(attachment);

			async.parallel({
				attachment: function(callback) {
					attachment.save(function(err, attachment) {
						callback(err, attachment);
					});
				},
				consultation: function(callback) {
					req.consultation.save(function(err, consultation) {
						callback(err, consultation);
					});
				}
			}, function(err, results) {
				if (err) return next(common.catchUnhandledErrors(err));

				Consultation.findOneAndPopulateAllFields(results.attachment.consultation, function(err, consultation) {
					if (err) return next(common.catchUnhandledErrors(err));
					res.json(consultation);
				});
			});

		});

	}
);


router.post('/:identity/consultations/:consultation/records', function (req, res, next) {

	var record = new Record();

	common.setModelValues(req.body, record);
	record.office = req.body.office;
	record.patient = req.patient._id;
	record.consultation = req.consultation._id;
	record.doctor = req.user.doctor._id;

	record.save(function(err, record) {
		if (err) return next(common.catchUnhandledErrors(err));

		record.updateRefs([req.consultation], function(err, results) {
			if (err) return next(common.catchUnhandledErrors(err));

			Consultation.findOneAndPopulateAllFields(record.consultation, function(err, consultation) {
				res.json(consultation);
			});
		});

	});
});
router.put('/:identity/consultations/:consultation/records/:record/edit', function (req, res, next) {
    Record.findOne({_id: req.params.record},function (err, record) {
        common.setModelValues(req.body, record);
        record.save(function (err, record) {
            if (err) return next(common.catchUnhandledErrors(err));

            record.updateRefs([req.consultation], function (err, results) {
                if (err) return next(common.catchUnhandledErrors(err));

                Consultation.findOneAndPopulateAllFields(record.consultation, function (err, consultation){
                    res.json(consultation);
                });
            });

        });
    })
});


router.get('/:identity/consultations/:consultation/plans', function(req, res, next) {
	Plan
		.find({ doctor: req.user.doctor, patient: req.patient._id })
		.exec(function(err, plans) {
			if (err) return next(err);

			res.json(plans);
		})
});

router.get('/:identity/consultations/:consultation/plans/:plan', function(req, res, next) {

	res.format({
		json: function() {
			res.send(req.plan);
		},
		pdf: function() {
			var plansHTML = ""
			req.plan.plans.forEach(function(plan) {
				plansHTML = plansHTML + "<div style=' font-size: 20px; margin-top: 5%;'> - " + plan.name + "</div>"
			})
			var options = [
				'--encoding utf-8',
				'--margin-bottom 0',
				'--margin-left 0',
				'--margin-right 0',
				'--margin-top 0'
			];

			var html = "<body style='margin:0px;padding-top: 80px;padding-left: 10%;padding-right: 10%;'> <div style='height: 80px;width: 100%;background: -webkit-linear-gradient(left, #D88148, #FE1E31);background: linear-gradient(to right, #D88148, #FE1E31);color: #ffffff;position: fixed;top: 0;left: 0;text-align: right;'> <p style=' margin-right: 5%; font-family: sans-serif; font-size: 14px; margin-bottom: 0px; margin-top: 2%;'>Dr(a). " + req.plan.doctor.fullNameLA + "</p><p style=' margin-right: 5%; font-family: sans-serif; font-size: 14px; margin-top: 6px;'>" + req.plan.doctor.phone + " / " + req.user.email + "</p></div><div> <div style=' text-align: center; margin-top: 8%; font-size: 40px; font-family: sans-serif;'>&Oacute;RDENES E INTERCONSULTAS</div><div style=' margin-top: 10%; font-size: 24px; font-family: sans-serif;'> bdac de 33 de edad C.I. </div><div style=' font-size: 22PX; font-family: sans-serif; margin-top: 5%;'>SOLICITUD" + plansHTML + "</div><div style=' margin-top: 5%; font-size: 18px; font-family: sans-serif;'>" + req.plan.notes + "</div></div><div style='position: fixed;width: 100%;bottom: 0;left:0;'><p style='text-align: center;font-size: 18px;color: #d88148;font-family: sans-serif;'>www.siplik.com</p><ul class='nav-colors' style='margin: 0;list-style:none;width:100%;font-size:0;padding: 0px'><li style='display:inline-block;width:14%;height:11px;width:15%;background: #D88148'></li><li style='display:inline-block;width:14%;height:11px;background: #FF9500'></li><li style='display:inline-block;width:14%;height:11px;background: #CF4929'></li><li style='display:inline-block;width:14%;height:11px;background: #B4242A'></li><li style='display:inline-block;width:14%;height:11px;background: #6F4D9A'></li><li style='display:inline-block;width:14%;height:11px;background: #00BDBA'></li><li style='display:inline-block;width:14%;height:11px;width:15%;background: #6DB152'></li></ul></div></body>"
			var doc = wkhtmltopdf(options, html);
			doc.stdout.pipe(res);
		}
	});
});

router.post('/:identity/consultations/:consultation/plans', function(req, res, next) {

	var plan = new Plan();

	common.setModelValues(req.body, plan);

	plan.consultation = req.consultation._id;
	plan.patient = req.patient._id;
	plan.doctor = req.user.doctor._id;

	plan.save(function(err, plan) {
		if (err) return next(common.catchUnhandledErrors(err));
		plan.updateRefs([req.consultation], function(err, results) {
			if (err) return next(common.catchUnhandledErrors(err));
			res.json(plan);
		});
	});
});

router.put('/:identity/consultations/:consultation/plans/:plan', function(req, res, next) {

	if (!req.plan.consultation.equals(req.body.consultation))
		return next(common.catchUnhandledErrors(err));

	common.setModelValues(req.body, req.plan);

	req.plan.save(function(err, plan) {
		if (err) return next(common.catchUnhandledErrors(err));
		res.json(plan);
	});
});

router.get('/:identity/consultations/:consultation/reports', function(req, res, next) {
	Report
		.find({ doctor: req.user.doctor, patient: req.patient._id })
		.exec(function(err, reports) {
			if (err) return next(err);

			res.json(reports);
		})
});

router.get('/:identity/consultations/:consultation/reports/:report', function(req, res, next) {
	res.format({
		json: function() {
			res.send(req.report);
		},
		pdf: function() {
			var notes = req.report.notes.split("\n")
			for (var i = 1; i < notes.length; i += 2) {
				notes.splice(i, 0, "<br/>")
			};
			notes.splice(notes.length - 2, 0, "<br/>")
			req.report.notes = ""
			for (var i = 0; i < notes.length; i++) {
				req.report.notes += notes[i]
			};
			console.log(req.report.notes)
			console.log(notes)
			var options = [
				'--encoding utf-8',
				'--margin-bottom 0',
				'--margin-left 0',
				'--margin-right 0',
				'--margin-top 0'
			];
			var html = "<body style='margin:0px;padding-top: 80px;padding-left: 10%;padding-right: 10%;'> <div style='height: 80px;width: 100%;color: black;position: fixed;top: 0;left: 0;text-align: right;'> <p style=' margin-right: 5%; font-family: sans-serif; font-size: 14px; margin-bottom: 0px; margin-top: 2%;'>Dr(a). " + req.report.doctor.fullNameLA + "</p><p style=' margin-right: 5%; font-family: sans-serif; acutefont-size: 14px; margin-top: 6px;'>" + req.report.doctor.phone + " / " + req.user.username + "</p></div><div> <div style=' text-align: center; margin-top: 8%; font-size: 40px; font-family: sans-serif;'> INFORME M&Eacute;DICO </div><div style=' margin-top: 10%; font-size: 22px; font-family: sans-serif;'> Se trata de " + req.report.patient.firstName + " " + req.report.patient.lastName + " de " + req.report.patient.age + "  de edad C.I. " + req.report.patient.identity + " </div><div style=' margin-top: 2%; font-size: 22px; font-family: sans-serif;'> " + req.report.notes + " </div></div><div style='position: fixed;width: 100%;bottom: 0;left:0;'><p style='text-align: center;font-size: 18px;color: #d88148;font-family: sans-serif;'>www.siplik.com</p></div></body>"
			var doc = wkhtmltopdf(options, html);
			doc.stdout.pipe(res);
		}
	});
});

router.post('/:identity/consultations/:consultation/reports', function(req, res, next) {


	var report = new Report();

	common.setModelValues(req.body, report);
	report.consultation = req.consultation._id;
	report.patient = req.patient._id;
	report.doctor = req.user.doctor._id;
	report.start = moment(moment().format('YYYY-MM-DD')).toDate();

	report.save(function(err, report) {
		if (err) return next(common.catchUnhandledErrors(err));
		report.updateRefs([req.consultation], function(err, results) {
			if (err) return next(common.catchUnhandledErrors(err));
			res.json(report);
		});
	});
});

router.put('/:identity/consultations/:consultation/reports/:report', function(req, res, next) {

	if (!req.report.consultation.equals(req.body.consultation))
		return next(common.catchUnhandledErrors(err));

	common.setModelValues(req.body, req.report);

	req.report.save(function(err, report) {
		if (err) return next(common.catchUnhandledErrors(err));
		res.json(report);
	});
});

router.get('/:identity/consultations/:consultation/prescriptions', function(req, res, next) {
	Prescription
		.find({ doctor: req.user.doctor, patient: req.patient._id })
		.exec(function(err, prescriptions) {
			if (err) return next(err);

			res.json(prescriptions);
		})
});

router.get('/:identity/consultations/:consultation/prescriptions/:prescription', function(req, res, next) {
	res.format({
		json: function() {
			res.send(req.prescription);
		},
		pdf: function() {

			var options = [
				'--encoding utf-8',
				'--margin-bottom 0',
				'--margin-left 0',
				'--margin-right 0',
				'--margin-top 0',
				'--orientation landscape'
			];
			var treatmentsHTML = "";
			var indicationsHTML = "";
			req.prescription.treatments.forEach(function(tratament) {
				treatmentsHTML = treatmentsHTML + ("<div style=' font-size: 20px; margin-top: 5%;'> - " + tratament.name + "</div>")
				indicationsHTML = indicationsHTML + ("<div style=' font-size: 20px; margin-top: 5%;'> - " + tratament.notes + "</div>")
			});

			var html = "<body style='margin:0px;padding-top: 80px;padding-left: 10%;padding-right: 10%;'> <div style='height: 80px;width: 100%;color: black;position: fixed;top: 0;left: 0;text-align: right;'> <p style=' margin-right: 5%; font-family: sans-serif; font-size: 14px; margin-bottom: 0px; margin-top: 2%;'>Dr(a). " + req.prescription.doctor.fullNameLA + "</p><p style=' margin-right: 5%; font-family: sans-serif; font-size: 14px; margin-top: 6px;'>" + req.prescription.doctor.phone + " / " + req.user.username + "</p></div><div><div style=' margin-top: 10%; font-size: 24px; font-family: sans-serif;'>" + req.prescription.patient.firstName + " " + req.prescription.patient.lastName + " de " + req.prescription.patient.age + " de edad C.I. " + req.prescription.patient.identity + "</div><div style='float:left;width: 45%;'><div style=' text-align: center; margin-top: 8%; font-size: 40px; font-family: sans-serif;'> TRATAMIENTO</div><div style=' font-size: 22PX; font-family: sans-serif; margin-top: 5%;'>TRATAMIENTOS " + treatmentsHTML + "</div></div><div style='float: left;width: 45%;margin-left: 5%;'><div style=' text-align: center; margin-top: 8%; font-size: 40px; font-family: sans-serif;'> INDICACIONES</div><div style=' font-size: 22px; font-family: sans-serif; margin-top: 5%;'> INDICACIONES " + indicationsHTML + "</div><div style=' margin-top: 5%; font-size: 18px; font-family: sans-serif;'>" + req.prescription.notes + "</div></div></div><div style='position: fixed;width: 100%;bottom: 0;left:0;'> <p style='text-align: center;font-size: 18px;color: #d88148;font-family: sans-serif;'>www.siplik.com</p></div></body>"
			var doc = wkhtmltopdf(options, html);
			doc.stdout.pipe(res);
		}
	});
});

router.post('/:identity/consultations/:consultation/prescriptions', function(req, res, next) {

	var prescription = new Prescription();

	common.setModelValues(req.body, prescription);

	prescription.consultation = req.consultation._id;
	prescription.patient = req.patient._id;
	prescription.doctor = req.user.doctor._id;

	prescription.save(function(err, prescription) {
		if (err) return next(common.catchUnhandledErrors(err));
		prescription.updateRefs([req.consultation], function(err, results) {
			if (err) return next(common.catchUnhandledErrors(err));
			res.json(prescription);
		});
	});
});

router.put('/:identity/consultations/:consultation/prescriptions/:prescription', function(req, res, next) {

	if (!req.prescription.consultation.equals(req.body.consultation))
		return next(common.catchUnhandledErrors(err));

	common.setModelValues(req.body, req.prescription);

	req.prescription.save(function(err, prescription) {
		if (err) return next(common.catchUnhandledErrors(err));
		res.json(prescription);
	});
});

router.param('consultation', function(req, res, next, consultation) {
	Consultation.findOne({ _id: consultation })
		.populate('doctor')
		.populate('patient')
		.populate('attachments')
		.populate('records')
		.exec(function(err, consultation) {
			if (err) return next(common.catchUnhandledErrors(err));
			if (!consultation) return next({ message: 'ConsultationNotExists', status: 400 });

			req.consultation = consultation;
			next();
		});
});

router.param('plan', function(req, res, next, plan) {
	Plan.findOne({ _id: plan })
		.populate('doctor')
		.populate('patient')
		.exec(function(err, plan) {
			if (err) return next(common.catchUnhandledErrors(err));
			if (!plan) return next({ message: 'InvalidPlan', status: 400 });

			req.plan = plan;
			next();
		});
});

router.param('report', function(req, res, next, report) {
	Report.findOne({ _id: report })
		.populate('doctor')
		.populate('patient')
		.exec(function(err, report) {
			if (err) return next(common.catchUnhandledErrors(err));
			if (!report) return next({ message: 'InvalidReport', status: 400 });

			req.report = report;
			next();
		});
});

router.param('prescription', function(req, res, next, prescription) {
	Prescription.findOne({ _id: prescription })
		.populate('doctor')
		.populate('patient')
		.exec(function(err, prescription) {
			if (err) return next(common.catchUnhandledErrors(err));
			if (!prescription) return next({ message: 'InvalidPrescription', status: 400 });

			req.prescription = prescription;
			next();
		});
});

router.get('/:identity/consultations/doctors/:doctor', function(req, res, next) {
	var query = {
		patient: req.patient._id,
		doctor: req.params.doctor
	};

	Consultation.find(query, null, { $sort: { date: 1 } })
		.populate('records')
		.populate('attachments')
		.populate('reports')
		.populate('prescription')
		.populate('plans')
		.populate('patient')
		.populate('doctor')

	.exec(function(err, consultations) {
		if (err) return next(err);
		async.filter(consultations, function(consultation, callback) {
			var isEmpty = ['records', 'attachments', 'reports', 'prescription', 'plans']
				.every(function(prop) {
					return consultation[prop].length == 0;
				}) && (moment(consultation.date).format('YYYY-MM-DD') !== moment().format('YYYY-MM-DD'));
			if (!isEmpty) return callback(consultation);
			consultation.remove(function(err) {
				callback();
			});
		}, function(consultations) {
			Record.populate(consultations, {
				path: 'records.item',
				model: Dictionary
			}, function(err) {
				Record.populate(consultations, {
					path: 'records.item.parent',
					model: Dictionary
				}, function(err) {

					Record.populate(consultations, {
						path: 'records.item.parent.parent',
						model: Dictionary
					}, function(err) {

						res.json(consultations);
					});
				});
			});
		});
	});
});

router.get('/:identity/consultations/doctors', function(req, res, next) {
	var query = {
		patient: req.patient._id,
	};

	Consultation.find(query, null, { $sort: { date: 1 } })
		.populate('records')
		.populate('attachments')
		.populate('reports')
		.populate('prescription')
		.populate('plans')
		.populate('patient')
		.populate('doctor')
		.limit(10)

	.exec(function(err, consultations) {
		if (err) return next(err);
		async.filter(consultations, function(consultation, callback) {
			var isEmpty = ['records', 'attachments', 'reports', 'prescription', 'plans']
				.every(function(prop) {
					return consultation[prop].length == 0;
				}) && (moment(consultation.date).format('YYYY-MM-DD') !== moment().format('YYYY-MM-DD'));
			if (!isEmpty) return callback(consultation);
			consultation.remove(function(err) {
				callback();
			});
		}, function(consultations) {
			Record.populate(consultations, {
				path: 'records.item',
				model: Dictionary
			}, function(err) {
				Record.populate(consultations, {
					path: 'records.item.parent',
					model: Dictionary
				}, function(err) {

					Record.populate(consultations, {
						path: 'records.item.parent.parent',
						model: Dictionary
					}, function(err) {

						res.json(consultations);
					});
				});
			});
		});
	});
});

router.get('/:identity/physical', function(req, res, next) {
	var query = {
		patient: req.patient._id,
	};
	Dictionary.find({ name: 'physical' }, function(err, physical) {
		Consultation.find(query, null, { $sort: { date: 1 } })
			.populate('records')
			.populate('attachments')
			.populate('reports')
			.populate('prescription')
			.populate('plans')
			.populate('patient')
			.populate('doctor')
			.limit(10)

		.exec(function(err, consultations) {
			if (err) return next(err);
			async.filter(consultations, function(consultation, callback) {
				var isEmpty = ['records', 'attachments', 'reports', 'prescription', 'plans']
					.every(function(prop) {
						return consultation[prop].length == 0;
					}) && (moment(consultation.date).format('YYYY-MM-DD') !== moment().format('YYYY-MM-DD'));
				if (!isEmpty) return callback(consultation);
				consultation.remove(function(err) {
					callback();
				});
			}, function(consultations) {
				Record.populate(consultations, {
					path: 'records.item',
					model: Dictionary
				}, function(err) {
					Record.populate(consultations, {
						path: 'records.item.parent',
						model: Dictionary
					}, function(err) {

						Record.populate(consultations, {
							path: 'records.item.parent.parent',
							model: Dictionary
						}, function(err) {
							var records = [];
							for (var i = 0; i < consultations.length; i++) {
								for (var j = 0; j < consultations[i].records.length; j++) {
									if (consultations[i].records[j].item.id == physical[0].id ||
										consultations[i].records[j].item.parent && consultations[i].records[j].item.parent.id == physical[0].id ||
										consultations[i].records[j].item.parent.parent && consultations[i].records[j].item.parent.parent.id == physical[0].id) {
										records.push(consultations[i])
										break
									};
								};
							};
							res.json(records);
						});
					});
				});
			});
		});
	})
});
