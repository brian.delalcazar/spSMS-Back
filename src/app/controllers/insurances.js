var mongoose = require('mongoose'),
	async = require('async'),
	router = require('express').Router(),
	common = require('../common'),
	Insurance = mongoose.model('Insurance');

module.exports = function(app) {
	app.use('/api/insurances', router);
};

router.get('/', function (req, res, next){
    Insurance.find({default:true},function (err, insurances){
        if (err) return next(err);
        res.json(insurances);
    })
});

router.post('/', function(req, res, next) {
	if (!req.body.name) return next(new Error('RequiredName'));

	var insurance = new Insurance(req.body);

	insurance.save(function(err, insurance) {
		if (err) return next(common.catchUnhandledErrors(err));
		res.json(insurance);
	});
});
