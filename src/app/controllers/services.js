var mongoose = require('mongoose'),
	router = require('express').Router(),
	common = require('../common'),
	async = require('async'),
	_ = require('lodash'),
	path = require('path'),
	crypto = require('crypto'),
	middlewares = require('../common/middlewares'),
	userMiddleware = require('../common/middlewares').user,
	Lab = mongoose.model('Lab'),
	LabAppointment = mongoose.model('LabAppointment'),
	LabService = mongoose.model('LabService'),
	Office = mongoose.model('Office'),
	Address = mongoose.model('Address'),
	Doctor = mongoose.model('Doctor'),
	Patient = mongoose.model('Patient'),
	Consultation = mongoose.model('Consultation'),
	User = mongoose.model('User'),
	config = require('../../config/config'),
	mailer = null;

module.exports = function(app) {
	app.use('/api/services', router);
	mailer = app.mailer;
};

router.get('/', function(req, res, next) {
	var query = {};
	if (_.has(req.query, 'description')) {
		var regexp = new RegExp(req.query.description, 'i');
		query.description = { $regex: regexp };
	}

	LabService.find(query, function(err, services) {
		res.json(services);
	});
});

router.get('/:service', function(req, res, next) {
	res.json(req.service);
});

// router.get('/:service/labs', function (req, res, next) {
//     Lab.find({
//         services: req.service._id
//     })
//     .populate('address')
//     .exec(function (err, labs) {
//         res.json(labs);
//     });
// });

router.get('/:service/labs/:country', function(req, res, next) {
	Lab.find({
			services: req.service._id
		})
		.populate({ path: 'address' })
		.exec(function(err, labs) {
			var labsCountry = [];
			labs.map(function(lab) {
				if (lab.address.country == req.params.country) {
					labsCountry.push(lab);
				};
			})
			res.json(labsCountry);
		});
});

router.param('service', function(req, res, next, value) {
	LabService.findById(value, function(err, service) {
		if (err) return next(err);
		if (!service) return next(new Error('ServiceNotFound'));
		req.service = service;
		next();
	});
});
