var mongoose = require('mongoose'),
	router = require('express').Router(),
	multiparty = require('connect-multiparty'),
	common = require('../common'),
	async = require('async'),
	fs = require('fs'),
	_ = require('lodash'),
	path = require('path'),
	crypto = require('crypto'),
	middlewares = require('../common/middlewares'),
	userMiddleware = require('../common/middlewares').user,
	Lab = mongoose.model('Lab'),
	LabAppointment = mongoose.model('LabAppointment'),
	LabService = mongoose.model('LabService'),
	Notification = mongoose.model('Notification'),
	Address = mongoose.model('Address'),
	Doctor = mongoose.model('Doctor'),
	Patient = mongoose.model('Patient'),
	Consultation = mongoose.model('Consultation'),
	User = mongoose.model('User'),
	config = require('../../config/config'),
	mailer = null;

module.exports = function(app) {
	app.use('/api/notifications', router);
	mailer = app.mailer;
};

router.get('/', function (req, res, next) {
    var query = {
        $or: [
            {owner:req.user.doctor._id},
            {all:true}
        ],
        seen:false
    };
    Notification
    .find(query)
    .sort('-created')
    .exec(function (err, notifications) {
        if (err) return next(err);
        res.json(notifications);
    });
});
router.get('/all', function (req, res, next) {
    var query = {
        $or: [
            {owner:req.user.doctor._id},
            {all:true}
        ]
    };
    Notification
    .find(query)
    .sort('-created')
    .exec(function (err, notifications) {
        if (err) return next(err);
        res.json(notifications);
    });
});

router.put('/:notification/seen', function (req, res, next) {
    req.notification.seen = true;
    req.notification.seenDate = new Date();
    req.notification.save(function (err, notification) {
        if (err) return next(err);
        res.json(notification);
    });
});

router.param('notification', function(req, res, next, value) {
	Notification.findById(value, function(err, notification) {
		if (err) return next(err);
		if (!notification) return next(new Error('NotificationNotFound'));
		req.notification = notification;
		next();
	});
});


router.get('/testNotification', function (req, res, next) {
    var noti = {
        owner:req.user.doctor._id,
        type: "newAppointment",
        created: new Date(),
        data: {
            appointment: "id",
            patient: "id"
        },
        title: "Nueva Cita",
        message: "Tienes una nueva cita con Brian Del Alcazar"
    }
    var notification = new Notification(noti)
    notification.save(function(err, noti){
        if (err) return next(err);
        res.json(noti);
    })
});