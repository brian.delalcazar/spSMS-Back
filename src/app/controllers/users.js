var mongoose = require('mongoose'),
	router = require('express').Router(),
	common = require('../common'),
	User = mongoose.model('User'),
	Role = mongoose.model('Role'),
	mailer = null;

module.exports = function(app) {
	app.use('/api/users', router);
	mailer = app.mailer;
};

router.put('/:user', function(req, res, next) {
	common.setModelValues(req.body, req.puser);

	req.puser.save(function(err, user) {
		if (err) return next(common.catchUnhandledErrors(err));

		res.json(user);
	});
});

router.put('/:user/roles', function(req, res, next) {
	Role.find({ name: { $in: req.body.roles || [] } }).exec(
		function(err, roles) {
			if (err) return next(common.catchUnhandledErrors(err));

			for (var i = 0; i < roles.length; i++) {
				req.puser.roles.push(roles[i]);
			}
			req.puser.save(function(err, user) {
				if (err) return next(common.catchUnhandledErrors(err));
				res.json(user);
			});
		}
	);
});

router.delete('/:user/roles', function(req, res, next) {

	for (var i = 0; i < req.puser.roles.length; i++) {
		if (req.body.roles.indexOf(req.puser.roles[i].name) != -1) {
			req.puser.roles.splice(i, 1);
		}
	}

	req.puser.save(function(err, user) {
		if (err) return next(common.catchUnhandledErrors(err));
		res.json(user);
	});
});

router.param('user', function(req, res, next, user) {
	User.findOne({ username: user })
		.populate('roles')
		.populate('doctor')
		.populate('patient')
		.exec(function(err, user) {
			if (err) return next(common.catchUnhandledErrors(err));
			if (!user) return next({ message: 'UserNotFound', status: 400 });

			req.puser = user;

			next();
		});
});
