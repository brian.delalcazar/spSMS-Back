var mongoose = require('mongoose'),
	async = require('async'),
	path = require('path'),
	fs = require('fs'),
	router = require('express').Router(),
	common = require('../../common'),
	userMiddleware = require('../../common/middlewares').user,
	multiparty = require('connect-multiparty'),
	config = require('../../../config/config'),
	_ = require('lodash'),
	moment = require('moment'),
	Doctor = mongoose.model('Doctor'),
	Patient = mongoose.model('Patient'),
	User = mongoose.model('User'),
	Address = mongoose.model('Address'),
    DataRecord = mongoose.model('DataRecord'),
	Office = mongoose.model('Office'),
	Week = mongoose.model('Week'),
	Role = mongoose.model('Role'),
	LabAppointment = mongoose.model('LabAppointment'),
	Block = mongoose.model('Block'),
	Group = mongoose.model('Group'),
	Clinic = mongoose.model('Clinic'),
	Record = mongoose.model('Record'),
	Dictionary = mongoose.model('Dictionary'),
	Consultation = mongoose.model('Consultation'),
	Appointment = mongoose.model('Appointment'),
	MercadoPago = mongoose.model('MercadoPago'),
	Payment = mongoose.model('Payment'),
	Premium = mongoose.model('Premium'),
	PremiumPlan = mongoose.model('PremiumPlan'),
	Protocol = mongoose.model('Protocol'),
	nodeXls = require('node-xls'),
	ENV = require("../../../config/config.js"),
	stripe = require("stripe")(ENV.stripeKey),
	mailer = null;


module.exports = function(app) {
	app.use('/', router);
	mailer = app.mailer;
};

router.get("/indicators/second", function(req, res, next) {
	Patient.aggregate(
    [  
        { "$project": {
        	"year": { $cond: [{ $ifNull: ['$birthday', 0] }, { $year: '$birthday' }, -1] },
            "range": { 
                "$switch": { 
                    "branches": [ 
                        { 
                            "case": { "$lte": [ "$age", 5 ] }, 
                            "then": "0-5" 
                        }, 
                        { 
                            "case": { 
                                "$and": [ 
                                    { "$gt": [ "$age", 5 ] }, 
                                    { "$lte": [ "$age", 10 ] } 
                                ] 
                            }, 
                            "then": "6-10" 
                        }, 
                        { 
                            "case": { 
                                "$and": [ 
                                    { "$gt": [ "$age", 10 ] }, 
                                    { "$lte": [ "$age", 15 ] } 
                                ] 
                            }, 
                            "then": "11-15" 
                        }, 
                        { 
                            "case": { 
                                "$and": [ 
                                    { "$gt": [ "$age", 15 ] }, 
                                    { "$lte": [ "$age", 20 ] } 
                                ] 
                            }, 
                            "then": "16-20" 
                        }, 
                        { 
                            "case": { 
                                "$and": [ 
                                    { "$gt": [ "$age", 20 ] }, 
                                    { "$lte": [ "$age", 25 ] } 
                                ] 
                            }, 
                            "then": "21-25" 
                        }, 
                        { 
                            "case": { 
                                "$and": [ 
                                    { "$gt": [ "$age", 25 ] }, 
                                    { "$lte": [ "$age", 30 ] } 
                                ] 
                            }, 
                            "then": "26-30" 
                        }, 
                        { 
                            "case": { 
                                "$and": [ 
                                    { "$gt": [ "$age", 30 ] }, 
                                    { "$lte": [ "$age", 35 ] } 
                                ] 
                            }, 
                            "then": "31-35" 
                        }, 
                        { 
                            "case": { 
                                "$and": [ 
                                    { "$gt": [ "$age", 35 ] }, 
                                    { "$lte": [ "$age", 40 ] } 
                                ] 
                            }, 
                            "then": "36-40" 
                        }, 
                        { 
                            "case": { 
                                "$and": [ 
                                    { "$gt": [ "$age", 40 ] }, 
                                    { "$lte": [ "$age", 45 ] } 
                                ] 
                            }, 
                            "then": "41-45" 
                        }, 
                        { 
                            "case": { 
                                "$and": [ 
                                    { "$gt": [ "$age", 45 ] }, 
                                    { "$lte": [ "$age", 50 ] } 
                                ] 
                            }, 
                            "then": "46-50" 
                        }, 
                        { 
                            "case": { 
                                "$and": [ 
                                    { "$gt": [ "$age", 50 ] }, 
                                    { "$lte": [ "$age", 55 ] } 
                                ] 
                            }, 
                            "then": "51-55" 
                        }, 
                        { 
                            "case": { 
                                "$and": [ 
                                    { "$gt": [ "$age", 55 ] }, 
                                    { "$lte": [ "$age", 60 ] } 
                                ] 
                            }, 
                            "then": "56-60" 
                        }, 
                        { 
                            "case": { 
                                "$and": [ 
                                    { "$gt": [ "$age", 60 ] }, 
                                    { "$lte": [ "$age", 65 ] } 
                                ] 
                            }, 
                            "then": "61-65" 
                        }, 
                        { 
                            "case": { 
                                "$and": [ 
                                    { "$gt": [ "$age", 65 ] }, 
                                    { "$lte": [ "$age", 70 ] } 
                                ] 
                            }, 
                            "then": "66-70" 
                        }, 
                        { 
                            "case": { 
                                "$and": [ 
                                    { "$gt": [ "$age", 70 ] }, 
                                    { "$lte": [ "$age", 78 ] } 
                                ] 
                            }, 
                            "then": "71-75" 
                        }, 
                        { 
                            "case": { 
                                "$and": [ 
                                    { "$gt": [ "$age", 75 ] }, 
                                    { "$lte": [ "$age", 80 ] } 
                                ] 
                            }, 
                            "then": "76-80" 
                        }, 
                        { 
                            "case": { 
                                "$and": [ 
                                    { "$gt": [ "$age", 80 ] }, 
                                    { "$lte": [ "$age", 85 ] } 
                                ] 
                            }, 
                            "then": "81-85" 
                        }, 
                        { 
                            "case": { 
                                "$and": [ 
                                    { "$gt": [ "$age", 85 ] }, 
                                    { "$lte": [ "$age", 90 ] } 
                                ] 
                            }, 
                            "then": "86-90" 
                        }, 
                        { 
                            "case": { 
                                "$and": [ 
                                    { "$gt": [ "$age", 90 ] }, 
                                    { "$lte": [ "$age", 95 ] } 
                                ] 
                            }, 
                            "then": "91-95" 
                        } , 
                        { 
                            "case": { 
                                "$and": [ 
                                    { "$gt": [ "$age", 95 ] }, 
                                    { "$lte": [ "$age", 100 ] } 
                                ] 
                            }, 
                            "then": "96-100" 
                        } , 
                        { 
                            "case": { 
                                "$and": [ 
                                    { "$gt": [ "$age", 100 ] }, 
                                    { "$lte": [ "$age", 105 ] } 
                                ] 
                            }, 
                            "then": "101-105" 
                        } , 
                        { 
                            "case": { 
                                "$and": [ 
                                    { "$gt": [ "$age", 105 ] }, 
                                    { "$lte": [ "$age", 110 ] } 
                                ] 
                            }, 
                            "then": "106-110" 
                        } , 
                        { 
                            "case": { 
                                "$and": [ 
                                    { "$gt": [ "$age", 110 ] }
                                ] 
                            }, 
                            "then": "111+" 
                        } 
                    ], 
                    "default": "Sin Edad" 
                } 
            } 
        }}
        , 
        { "$group": { 
            "_id": "$year", 
            "ages": {$push:"$range"}  
        }}
    ])
		.exec(function(err, results) {
			if (err) return next(err)
			res.json(results)
		})
})

router.get("/indicators/first", function(req, res, next) {
	Patient.aggregate(
    [  
        { "$project": { 
            "range": { 
                "$switch": { 
                    "branches": [ 
                        { 
                            "case": { "$lte": [ "$age", 5 ] }, 
                            "then": "0-5" 
                        }, 
                        { 
                            "case": { 
                                "$and": [ 
                                    { "$gt": [ "$age", 5 ] }, 
                                    { "$lte": [ "$age", 10 ] } 
                                ] 
                            }, 
                            "then": "6-10" 
                        }, 
                        { 
                            "case": { 
                                "$and": [ 
                                    { "$gt": [ "$age", 10 ] }, 
                                    { "$lte": [ "$age", 15 ] } 
                                ] 
                            }, 
                            "then": "11-15" 
                        }, 
                        { 
                            "case": { 
                                "$and": [ 
                                    { "$gt": [ "$age", 15 ] }, 
                                    { "$lte": [ "$age", 20 ] } 
                                ] 
                            }, 
                            "then": "16-20" 
                        }, 
                        { 
                            "case": { 
                                "$and": [ 
                                    { "$gt": [ "$age", 20 ] }, 
                                    { "$lte": [ "$age", 25 ] } 
                                ] 
                            }, 
                            "then": "21-25" 
                        }, 
                        { 
                            "case": { 
                                "$and": [ 
                                    { "$gt": [ "$age", 25 ] }, 
                                    { "$lte": [ "$age", 30 ] } 
                                ] 
                            }, 
                            "then": "26-30" 
                        }, 
                        { 
                            "case": { 
                                "$and": [ 
                                    { "$gt": [ "$age", 30 ] }, 
                                    { "$lte": [ "$age", 35 ] } 
                                ] 
                            }, 
                            "then": "31-35" 
                        }, 
                        { 
                            "case": { 
                                "$and": [ 
                                    { "$gt": [ "$age", 35 ] }, 
                                    { "$lte": [ "$age", 40 ] } 
                                ] 
                            }, 
                            "then": "36-40" 
                        }, 
                        { 
                            "case": { 
                                "$and": [ 
                                    { "$gt": [ "$age", 40 ] }, 
                                    { "$lte": [ "$age", 45 ] } 
                                ] 
                            }, 
                            "then": "41-45" 
                        }, 
                        { 
                            "case": { 
                                "$and": [ 
                                    { "$gt": [ "$age", 45 ] }, 
                                    { "$lte": [ "$age", 50 ] } 
                                ] 
                            }, 
                            "then": "46-50" 
                        }, 
                        { 
                            "case": { 
                                "$and": [ 
                                    { "$gt": [ "$age", 50 ] }, 
                                    { "$lte": [ "$age", 55 ] } 
                                ] 
                            }, 
                            "then": "51-55" 
                        }, 
                        { 
                            "case": { 
                                "$and": [ 
                                    { "$gt": [ "$age", 55 ] }, 
                                    { "$lte": [ "$age", 60 ] } 
                                ] 
                            }, 
                            "then": "56-60" 
                        }, 
                        { 
                            "case": { 
                                "$and": [ 
                                    { "$gt": [ "$age", 60 ] }, 
                                    { "$lte": [ "$age", 65 ] } 
                                ] 
                            }, 
                            "then": "61-65" 
                        }, 
                        { 
                            "case": { 
                                "$and": [ 
                                    { "$gt": [ "$age", 65 ] }, 
                                    { "$lte": [ "$age", 70 ] } 
                                ] 
                            }, 
                            "then": "66-70" 
                        }, 
                        { 
                            "case": { 
                                "$and": [ 
                                    { "$gt": [ "$age", 70 ] }, 
                                    { "$lte": [ "$age", 78 ] } 
                                ] 
                            }, 
                            "then": "71-75" 
                        }, 
                        { 
                            "case": { 
                                "$and": [ 
                                    { "$gt": [ "$age", 75 ] }, 
                                    { "$lte": [ "$age", 80 ] } 
                                ] 
                            }, 
                            "then": "76-80" 
                        }, 
                        { 
                            "case": { 
                                "$and": [ 
                                    { "$gt": [ "$age", 80 ] }, 
                                    { "$lte": [ "$age", 85 ] } 
                                ] 
                            }, 
                            "then": "81-85" 
                        }, 
                        { 
                            "case": { 
                                "$and": [ 
                                    { "$gt": [ "$age", 85 ] }, 
                                    { "$lte": [ "$age", 90 ] } 
                                ] 
                            }, 
                            "then": "86-90" 
                        }, 
                        { 
                            "case": { 
                                "$and": [ 
                                    { "$gt": [ "$age", 90 ] }, 
                                    { "$lte": [ "$age", 95 ] } 
                                ] 
                            }, 
                            "then": "91-95" 
                        } , 
                        { 
                            "case": { 
                                "$and": [ 
                                    { "$gt": [ "$age", 95 ] }, 
                                    { "$lte": [ "$age", 100 ] } 
                                ] 
                            }, 
                            "then": "96-100" 
                        } , 
                        { 
                            "case": { 
                                "$and": [ 
                                    { "$gt": [ "$age", 100 ] }, 
                                    { "$lte": [ "$age", 105 ] } 
                                ] 
                            }, 
                            "then": "101-105" 
                        } , 
                        { 
                            "case": { 
                                "$and": [ 
                                    { "$gt": [ "$age", 105 ] }, 
                                    { "$lte": [ "$age", 110 ] } 
                                ] 
                            }, 
                            "then": "106-110" 
                        } , 
                        { 
                            "case": { 
                                "$and": [ 
                                    { "$gt": [ "$age", 110 ] }
                                ] 
                            }, 
                            "then": "111+" 
                        } 
                    ], 
                    "default": "Sin Edad" 
                } 
            } 
        }}, 
        { "$group": { 
            "_id": "$range", 
            "count": { "$sum": 1 } 
        }}
    ])
		.exec(function(err, results) {
			if (err) return next(err)
			res.json(results)
		})
})

router.get("/indicators/third", function(req, res, next) {
	Patient.aggregate(
    [  
        { "$project": { 
            "range": { 
                "$switch": { 
                    "branches": [{
                    	case:{
                    		genre:"male"
                    	},
                    	then:"Masculino"
                    },
                    {
                    	case:{
                    		genre:"female"
                    	},
                    	then:"Femenino"
                    }], 
                    "default": "Sin Genero" 
                } 
            } 
        }}, 
        { "$group": { 
            "_id": "$range", 
            "count": { "$sum": 1 } 
        }}
    ])
		.exec(function(err, results) {
			if (err) return next(err)
			res.json(results)
		})
})

router.get("/indicators/fifth", function(req, res, next) {
	Patient.aggregate(
    [  
        { "$group": { 
            "_id": "$ocupation", 
            "count": { "$sum": 1 } 
        }}
    ])
		.exec(function(err, results) {
			if (err) return next(err)
			res.json(results)
		})
})
