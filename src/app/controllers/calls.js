var mongoose = require('mongoose'),
    router = require('express').Router(),
    Call = mongoose.model('Call');
    
module.exports = function (app) {
    app.use('/api/calls', router);
};

router.get('/lastCall/:patient', function (req, res, next) {

    var startdate = new Date();

    var durationInMinutes = 2;

    startdate.setMinutes(startdate.getMinutes() - durationInMinutes) 
    Call.find({receiver: req.params.patient, status:false, created_at: {$gt:startdate }}).sort({created_at:-1}).limit(1).exec(function (err, call) {
        console.log(err, call)
        if (err) return next(err);
        if (!call) {
            res.json({status:404})
        } else if (!!call) {
            res.json(call[0])
        }
    })
});