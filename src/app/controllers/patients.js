var mongoose = require('mongoose'),
	common = require('../common'),
	insuranceMiddleware = require('../common/middlewares').insurance,
	router = require('express').Router(),
	path = require('path'),
	fs = require('fs'),
	async = require('async'),
	multiparty = require('connect-multiparty'),
	User = mongoose.model('User'),
	Patient = mongoose.model('Patient'),
	Doctor = mongoose.model('Doctor'),
	Consultation = mongoose.model('Consultation'),
    LabAppointment = mongoose.model('LabAppointment'),
    Call = mongoose.model('Call'),
	Record = mongoose.model('Record'),
	Attachment = mongoose.model('Attachment'),
    Dictionary = mongoose.model('Dictionary'),
	Client = mongoose.model('Client'),
	Plan = mongoose.model('Plan'),
	Address = mongoose.model('Address'),
	Prescription = mongoose.model('Prescription'),
	Report = mongoose.model('Report'),
	Office = mongoose.model('Office'),
	Role = mongoose.model('Role'),
	Clinic = mongoose.model('Clinic'),
	Appointment = mongoose.model('Appointment'),
	Insurance = mongoose.model('Insurance'),
    ionicPushServer = require('ionic-push-server'),
    ENV = require("../../config/config.js"),
    gcm = require('node-gcm'),
    socket = require("../../config/socket.js"),
    credentials = {
        IonicApplicationID: "70569daa",
        IonicApplicationAPItoken: "eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJqdGkiOiIwM2Q4NzQyOC04YmUxLTQ3YmItODhlMi1hYzllOWU1MjA1NDgifQ.RecBGBfkqy1Xb8-evHNkVw8Lk67XoHnCESbXCCbRMPY"
    };
module.exports = function(app, io) {
	app.use('/api/patients', router);
};

router.get('/', function (req, res, next) {
    async.waterfall([
        function (callback) {
            if (req.user && req.user.doctor && req.user.doctor.getSharedOffices) {
                req.user.doctor.getSharedOffices(callback);                
            }
        },
        function (offices, callback) {
            Consultation.distinct('patient', {
                office: {$in: offices}
            }, callback);
        },
        function (patientIds, callback) {
            Patient
                .find({
                    $or: [{
                        _id: {$in: patientIds}
                    }, {
                        doctors: req.user.doctor._id,
                        status: 'ACTIVE'
                    }]
                })
                .sort({ identity: 'asc' })
                .exec(callback);
        }
    ], function (err, patients) {
        if (err) return next({message: err, status: 400});
        //TODO sera que mongo soporta esto ???
        async.map(patients, function(patient, callback){
            patient.populateNextAppointment(req.user.doctor._id, callback);
        }, function (err, patients){
            res.json(patients);
        });
    });
});

router.post('/', insuranceMiddleware.create,function (req, res, next) {
    var patient = new Patient();
    if (req.body.address) {
        var address = new Address(req.body.address);
        patient.address = address._id;
        delete req.body.address
    };
    common.setModelValues(req.body, patient);
    if (req.user) {
        patient.creator = req.user.doctor._id;
        patient.doctors.push(req.user.doctor._id);
        req.user.doctor.patients.push(patient._id);
    }

    if (req.body.referedBy) {
        patient.referedBy = [req.body.referedBy]
    }
    async.series({
        validations: function (callback) {
            Insurance.findById(req.body.insurance, function (err, insurance){
                if (err) return callback(err);
                if (!insurance) patient.insurance = undefined;
                callback(null, insurance);
            });
        },
        changes: function (callback){
            async.parallel({
                address: function (callback) {
                    if (!patient.address) return callback();
                    address.save(function (err, address) {
                        if (err) return callback(err);
                        callback(null, address);
                    })
                },
                patient: function (callback) {
                    patient.save(function (err, patient){
                        if (err) return callback(err);
                        if (!patient) return callback(new Error('PatientNotSaved'));
                        callback(null, patient);
                    });
                },
                doctor: function (callback) {
                    if (!req.user) return callback();
                    req.user.doctor.save(function (err, doctor) {
                        if (err) return callback(err);
                        callback(null, doctor);
                    });
                }
            }, function (err, results){
                if (err) return callback(err);
                callback(err, results);
            });
        }
    }, function (err, results){
        if (err) return next(common.catchUnhandledErrors(err));
        // if (results.changes.patient.email && req.user) {
        //     if (ENV.env == "staging" || ENV.env == "production") {
        //         mailer.send('patient/new', {
        //             to: results.changes.patient.email,
        //             subject: 'El doctor '+ req.user.doctor.firstName + " te ha registrado en Siplik.",
        //             docName: req.user.doctor.fullName,
        //             patientName: results.changes.patient.fullName,
        //             frontendUrl: config['app'].frontendPath,
        //             backendUrl: config['app'].publicPath,
        //             identity: results.changes.patient.identity

        //         }, function(err) {
        //             if (err) {
        //                 if (results.doctor) results.doctor.remove();
        //                 if (results.user) results.user.remove();
        //                 if (results.addresses) results.addresses.remove();

        //                 return next(common.catchUnhandledErrors(err));
        //             }
        //         });
        //     }
        // }
        res.json(results.changes.patient);
    });

});

router.get('/:identity', function(req, res, next) {
	if (req.user && req.user.doctor) {
		req.patient.populateNextAppointment(req.user.doctor._id, function(err, patient) {
			res.json(patient);
		});
	} else {
		res.json(req.patient);
	}
});

router.post('/:identity/user', function(req, res, next) {
	User.findOne({
		username: req.patient.email
	}, function(err, user) {
		if (err) return next(err);
		Role.findOne({ name: "patients" }, function(err, role) {
			if (err) return next(err);
			if (!role) return next(new Error("NoRole"));
			if (user) {
				if (user.doctor && !user.patient) {
					user.patient = req.patient;
					user.roles.push(role.id)
					user.save(function(err, user) {
						if (err) return next(err);
						res.json(user);
					});
				} else {
					return next(new Error('UserAlreadyExists'));
				};
			};
			user = new User(req.body);
			user.patient = req.patient;
			user.roles.push(role.id)
			user.active = { status: true };
			user.save(function(err, user) {
				if (err) return next(err);
				res.json(user);
			});
		})
	});
});

router.get('/:identity/lab-appointments', function(req, res, next) {
	LabAppointment
		.find({
			patient: req.patient._id
		})
		.populate('service patient')
		.exec(function(err, appointments) {
			res.json(appointments);
		});
});

router.post('/:patientId/share/:doctor', function(req, res, next) {
	Doctor.findById(req.params.doctor, function(err, doctor) {
		if (err) return next(err);
		if (!doctor) return next(new Error('NotFound'));
		if (!doctor.isPremium) return next(new Error('NotPremium'));
		Patient.findOneAndUpdate({
			_id: req.params.patientId
		}, {
			$addToSet: {
				sharedWith: doctor._id,
				doctors: doctor._id
			}
		}, function(err, patient) {
			if (err) return next(err);
			res.json(patient);
		});
	});
});

router.put('/:identity', function (req, res, next) {

    if (!req.patient.address && req.body.address) {
        var address = new Address(req.body.address);
        req.patient.address = address._id;
        delete req.body.address;
    } else if (req.patient.address && req.body.address) {
        var address = req.patient.address
        common.setModelValues(req.body.address, address);
        delete req.body.address        
    };
    common.setModelValues(req.body, req.patient);
    if (req.patient.doctors.indexOf(req.user.doctor._id) == -1) {
        req.patient.doctors.push(req.user.doctor._id);
    };

    if (req.user.doctor.patients.indexOf(req.patient._id) == -1) {
        req.user.doctor.patients.push(req.patient._id);
    };


    if (req.patient.referedBy && req.body.referedBy) {
        req.patient.referedBy.push(req.body.referedBy)
    } else if (!req.patient.referedBy && req.body.referedBy) {
        req.patient.referedBy = [req.body.referedBy];
    };
    // if (req.body.creator == req.user.doctor.id) {

    // }

    // if (req.body.creator != req.user.doctor.id) {
    //     var patientBody =JSON.stringify(req.body),
    //         patientUser =JSON.stringify(req.patient);
    //     if (patientBody != patientUser) {
    //         return next(new Error('NotAllowed'));
    //     }
    // }

    async.parallel({
        address: function (callback) {
            if (address) {
                address.save(function (err, address) {
                    if (err) return callback(err);
                    callback(err, address);
                })
            } else if (req.address) {
                req.address.save(function (err, address) {
                    if (err) return callback(err);
                    callback(err, address);
                })
            }
        },
        patient: function (callback) {
            req.patient.status = 'ACTIVE';
            req.patient.save(function (err, patient) {
                if (err) return callback(err);
                callback(err, patient);
            });

        },
        doctor: function (callback) {
            if (!!req.user.doctor) {
                req.user.doctor.save(function (err, doctor) {
                    callback(err, doctor);
                });
            }
        }
    }, function (err, results) {
        if (err) return next(common.catchUnhandledErrors(err));
        // if (req.userPatient) {
        //     var notification = {
        //         "profile": "production_test",
        //         "notification": {
        //             "android": {
        //                 "title": "Te han agregado como paciente",
        //                 "message": "El/La Dr(a). " + results.doctor.firstName + " te ha agregado como su paciente",
        //                 "sound": "",
        //                 "payload": {
        //                     "doctor": results.doctor.id
        //                 }
        //             },
        //             "ios": {
        //                 "title": "Te han agregado como paciente",
        //                 "message": "El/La Dr(a). " + results.doctor.firstName + " te ha agregado como su paciente",
        //                 "sound": "",
        //                 "payload": {
        //                     "doctor": results.doctor.id
        //                 }
        //             }
        //         }
        //     }
        //     socket.sendNotification(results.patient.id, notification)
        // }
        // if (results.patient.email) {
        //     if (ENV.env == "staging" || ENV.env == "production") {
        //         mailer.send('patient/edit', {
        //             to: results.patient.email,
        //             subject: 'El doctor '+ results.doctor.firstName + " te ha agregado en Siplik.",
        //             docName: results.doctor.fullName,
        //             patientName: results.patient.fullName,
        //             frontendUrl: config['app'].frontendPath,
        //             backendUrl: config['app'].publicPath,
        //             identity: results.patient.identity,
        //             url: config['app'].frontendPath + "/remove/access/" + results.patient.identity + "/" + results.doctor.id

        //         }, function(err) {
        //             if (err) {
        //                 if (results.doctor) results.doctor.remove();
        //                 if (results.user) results.user.remove();
        //                 if (results.addresses) results.addresses.remove();

        //                 return next(common.catchUnhandledErrors(err));
        //             }
        //         });
        //     }
        // }
        res.json(results.patient);
    });
});

router.put('/:identity/photo', multiparty({ uploadDir: './uploads' }), function(req, res, next) {
	// 4194304 bytes == 4 megabytes
	if (req.files.image.size > 4194304) {
		return next(new Error('FileSizeLimit'));
	}

	async.series({
		validation: function(callback) {
			if (!!req.patient.photo.path) {
				fs.unlink(path.normalize(__dirname + '/../../' + req.patient.photo.path), function(err) {
					if (err) return next(common.catchUnhandledErrors(err));
					callback(null, null);
				});
			} else {
				callback(null, null);
			}
		},
		photo: function(callback) {
			req.patient.photo.path = 'uploads/' + req.patient._id + path.extname(req.files.image.name);

			fs.rename(req.files.image.path, path.normalize(__dirname + '/../../' + req.patient.photo.path), function(err) {
				if (err) return next(common.catchUnhandledErrors(err));

				req.patient.save(function(err, savedPatient) {
					if (err) return next(common.catchUnhandledErrors(err));

					callback(err, savedPatient);
				});

			});
		}
	}, function(err, results) {
		if (err) return next(common.catchUnhandledErrors(err));

		res.json(results.photo);
	});
});

router.get('/:identity/records', function(req, res, next) {
	//TODO permisologia de doctores
	Record.find({ patient: req.patient._id, doctor: req.user.doctor._id })
		.populate('item')
		.populate('consultation')
		.exec(function(err, records) {
			res.json(records);
		})
});


router.get('/:identity/documents', function(req, res, next) {

	async.parallel({
		prescriptions: function(callback) {
			Prescription.find({ patient: req.patient._id }, null, { sort: { created: -1 } })
				.exec(function(err, prescriptions) {
					callback(err, prescriptions.map(function(prescription) {
						prescription = prescription.toObject();
						prescription.type = 'prescription';
						return prescription;
					}));
				});
		},
		reports: function(callback) {
			Report.find({ patient: req.patient._id }, null, { sort: { created: -1 } })
				.exec(function(err, reports) {
					callback(err, reports.map(function(report) {
						report = report.toObject();
						report.type = 'report';
						return report;
					}));
				});
		},
		plans: function(callback) {
			Plan.find({ patient: req.patient._id }, null, { sort: { created: -1 } })
				.exec(function(err, plans) {
					callback(err, plans.map(function(plan) {
						plan = plan.toObject();
						plan.type = 'plan';
						return plan;
					}));
				});
		},
		attachments: function(callback) {
			Attachment.find({ patient: req.patient._id }, null, { sort: { created: -1 } })
				.exec(function(err, attachments) {
					callback(err, attachments.map(function(attachment) {
						attachment = attachment.toObject();
						attachment.type = 'attachment';
						return attachment;
					}));
				})
		}
	}, function(err, results) {
		if (err) return next(common.catchUnhandledErrors(err));

		var documents = []
			.concat(results.prescriptions)
			.concat(results.reports)
			.concat(results.plans)
			.concat(results.attachments);

		documents.sort(function(docA, docB) {
			return docB.created - docA.created;
		});

		res.json(documents);
	});

});

router.get('/:identity/doctors', function(req, res, next) {
	Doctor.find({ patients: { $in: [req.patient._id] } }, function(err, doctors) {
		res.json(doctors)
	})
});


router.get('/:identity/appointments', function(req, res, next) {
	Appointment
		.find({
			patient: req.patient._id,
			status: 'PENDING'
		})
		.populate('doctor')
		.populate('block')
		.exec(function(err, appointments) {
			res.json(appointments);
		});
});

router.get('/users/:username', function(req, res, next) {
	User.findOne({ username: req.params.username }, function(err, user) {
		if (err) return next(common.catchUnhandledErrors(err));
		if (!user) return next({ message: 'NoUser', status: 400 });
		res.json(user)
	})
});

router.post('/users/update', function(req, res, next) {
	User.findOne({ username: req.body.email }, function(err, user) {
		if (err) return next(common.catchUnhandledErrors(err));
		if (!user) return next({ message: 'NoUser', status: 400 });
		Role.findOne({ name: "patients" }, function(err, role) {
			if (err) return next(err);
			if (!role) return next(new Error("NoRole"));
			var patient = new Patient();

			common.setModelValues(req.body, patient);
			user.patient = patient._id;
			user.roles.push(role.id)
			patient.user = user._id;
			async.parallel({
				patient: function(callback) {
					patient.save(function(err, patient) {
						if (err) return callback(err);
						if (!patient) return callback(new Error('PatientNotSaved'));
						callback(null, patient);
					});
				},
				user: function(callback) {
					user.save(function(err, user) {
						if (err) return callback(err);
						callback(null, user);
					});
				}
			}, function(err, results) {
				if (err) return next(common.catchUnhandledErrors(err));

				res.json(results);
			});
		})
	})
});

router.post('/:identity/appointments/:appointment', function(req, res, next) {
	Appointment
		.findOne({
			_id: req.params.appointment
		})
		.populate('doctor')
		.populate('patient')
		.exec(function(err, appointment) {
			if (err) return next(common.catchUnhandledErrors(err));
			appointment.notes = req.body.notes;
			appointment.save(function(err, appointment) {
				if (err) return next(common.catchUnhandledErrors(err));
				res.json(appointment)
			})
		});
});

router.get('/:identity/labExams', function(req, res, next) {
	LabAppointment.find({ patient: req.patient._id, done: true })
		.populate('service')
		.populate('patient')
		.populate('lab')
		.exec(function(err, exams) {
			if (err) return next(common.catchUnhandledErrors(err));
			res.json(exams);
		})
});

router.get('/:identity/labExams/pending', function(req, res, next) {
	LabAppointment.find({ patient: req.patient._id, done: false })
		.populate('service')
		.populate('patient')
		.populate('lab')
		.exec(function(err, exams) {
			if (err) return next(common.catchUnhandledErrors(err));
			res.json(exams);
		})
});

router.post('/notifications/call', function (req, res, next) {

    var device_tokens = []; //create array for storing device tokens
    var retry_times = 4; //the number of times to retry sending the message if it fails

    var sender = new gcm.Sender('AIzaSyDZeB6NjhJkMYM252vjGUg2NwjBbAGICmc'); //create a new sender
    var message = new gcm.Message({
        collapseKey: 'siplik',
        priority:'high',
        contentAvailable: true,
        delayWhileIdle: true,
        timeToLive: 3,
        notification: {
            title: "Llamada Entrante!",
            icon: "myicon",
            body: "El Dr(a) "+ req.body.name +" esta tratando de contactarte",
            sound: "notification"
        },
        data: {
            doctor: req.body.doctor
        }
    }); //create a new message

    if (!req.body.patient) {
        return next(new Error("NoPatientProvided"))
    }
    if (!req.body.doctor) {
        return next(new Error("NoDoctorProvided"))
    }
    Client.findOne({clientId: req.body.patient}, function (err, client) {
        if (err) return next(err);
        if (!client) return next(new Error("NoPushFound"));
        device_tokens.push(client.pushNotification);

        sender.send(message, device_tokens, retry_times, function(result,algo){
        });
        res.send('NotificationSended');
    })

});
