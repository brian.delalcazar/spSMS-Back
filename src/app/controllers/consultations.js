var mongoose = require('mongoose'),
    router = require('express').Router(),
    async = require('async'),
    fs = require('fs'),
    path = require('path'),
    Consultation = mongoose.model('Consultation'),
    Record = mongoose.model('Record'),
    Attachment = mongoose.model('Attachment'),
    Dictionary = mongoose.model('Dictionary');

module.exports = function (app) {
    app.use('/api/consultations', router);
};
router.get("/motives", function(req, res, next, value) {
    var docId = "";
    if (req.user && req.user.doctor && req.user.doctor._id) {
        docId = req.user.doctor._id
    };
    Dictionary.findOne({ name: "motive" }, function (err, motive) {
            if (err) return next(err);
            Dictionary.find({ parent: motive._id, $or: [
                    { doctor: { $exists: false } },
                    { doctor: docId }
            ]}).exec(function (err, motives) {
                if (err) return next(err);
                res.json(motives)
            })
    })
})
router.get('/getOne', function (req, res, next) {
    Consultation.findOneAndPopulateAllFields(req.query.consultation, function (err, consultation){
        if (err) return next(err);
        res.json(consultation);
    })
})

router.delete('/:consultation/records/:record', function (req, res, next) {

    if(!req.consultation._id.equals(req.record.consultation)) return next(new Error('InvalidRecord'));
    async.auto({
        record: function (callback) {
            req.record.remove(callback);
        },
        consultation:['record', function (callback, results) {
            req.consultation.update({$pull: { records: results.record._id }}, callback);
        }]
    }, function (err, results) {
        if (err) return next(err);
        Consultation.findOneAndPopulateAllFields(req.consultation, function (err, consultation){
            if (err) return next(err);
            res.json(consultation);
        })
    })
});

router.delete('/:consultation/attachments/:attachment', function(req, res, next) {
	if (!req.consultation._id.equals(req.attachment.consultation)) return next(new Error('InvalidAttachment'));

	async.auto({
		attachment: function(callback) {
			req.attachment.remove(function(err, attachment) {
				fs.unlink(path.normalize(__dirname + '/../../' + req.attachment.path), function (err) {
					if (err) return next(err); callback(null, attachment);
				});
			});
		},
		consultation: ['attachment', function(callback, results) {
			req.consultation.update({ $pull: { attachment: results.attachment._id } }, callback)
		}]
	}, function(err, results) {
		if (err) return next(err);
		Consultation.findOneAndPopulateAllFields(req.consultation, function(err, consultation) {
			if (err) return next(err);

			res.json(consultation);
		})
	})
});

router.post('/office/:office/:consultation', function(req, res, next) {
	req.consultation.office = req.params.office
	req.consultation.save(function(err, consultation) {
		if (err) return next(common.catchUnhandledErrors(err))
		res.json(consultation);
	})
});

router.param('consultation', function(req, res, next, value) {
	Consultation.findOne({ _id: value })
		.exec(function(err, consultation) {
			if (err) return next(err);
			if (!consultation) return next(new Error('NoConsultationFound'));

			req.consultation = consultation;
			next();
		});
});

router.param('record', function (req, res, next, value) {
    Record.findOne({ _id: value })
        .exec(function (err, record) {
            if (err) return next(err);
            if (!record) return next(new Error('NoRecordFound'));
			req.record = record;
			next();
		});
});

router.param('attachment', function(req, res, next, value) {
	Attachment.findOne({ _id: value })
		.exec(function(err, attachment) {
			if (err) return next(err);
			if (!attachment) return next(new Error('NoAttachmentFound'));

			req.attachment = attachment;
			next();
		});
});
