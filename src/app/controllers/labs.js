var mongoose = require('mongoose'),
	router = require('express').Router(),
	multiparty = require('connect-multiparty'),
	common = require('../common'),
	async = require('async'),
	fs = require('fs'),
	_ = require('lodash'),
	fs = require('fs'),
	multiparty = require('connect-multiparty'),
	path = require('path'),
	crypto = require('crypto'),
	middlewares = require('../common/middlewares'),
	userMiddleware = require('../common/middlewares').user,
	Lab = mongoose.model('Lab'),
	LabAppointment = mongoose.model('LabAppointment'),
	LabService = mongoose.model('LabService'),
	Office = mongoose.model('Office'),
	Address = mongoose.model('Address'),
	Doctor = mongoose.model('Doctor'),
	Patient = mongoose.model('Patient'),
	Consultation = mongoose.model('Consultation'),
	User = mongoose.model('User'),
	config = require('../../config/config'),
	mailer = null;

module.exports = function(app) {
	app.use('/api/labs', router);
	mailer = app.mailer;
};

router.get('/', function(req, res, next) {
	if (_.has(req.query, 'country')) {
		Address.find({
			country: req.query.country
		}, function(err, addresses) {
			exec({
				address: { $in: _.map(addresses, '_id') }
			});
		});
	} else {
		exec({});
	}

	function exec(query) {
		Lab.find(query)
			.populate('user address services')
			.exec(function(err, labs) {
				res.json(labs);
			});
	};
});

router.get('/:lab', function(req, res, next) {
	res.json(req.lab);
});

router.get('/:lab/appointments', function(req, res, next) {
	var query = {
		lab: req.lab._id,
		done: false
	};

	if (_.has(req.query, 'done')) {
		query.done = !!JSON.parse(req.query.done);
	}

	if (_.has(req.query, 'patient')) {
		return Patient.findOne({
			identity: req.query.patient
		}, function(err, patient) {
			if (err) return next(err);
			if (!patient) return next(new Error('PatientNotFound'));
			query.patient = patient._id;
			exec();
		});
	}

	exec();

	function exec() {
		LabAppointment
			.find(query)
			.populate('service patient')
			.exec(function(err, appointments) {
				res.json(appointments);
			});
	};
});

router.get('/:lab/appointments/:appointment', function(req, res, next) {
	if (!req.appointment.seen) {
		req.appointment.seen = true;
		req.appointment.save(function(err, result) {
			res.json(req.appointment);
		});
	} else {
		res.json(req.appointment);
	}
});

router.post('/:lab/appointments', function(req, res, next) {
	var appointment = new LabAppointment();
	appointment.lab = req.lab;
	appointment.service = req.body.service;
	appointment.patient = req.body.patient;
	appointment.owner = req.user._id;
	appointment.save(function(err, appointment) {
		if (err) return next(err);
		res.json(appointment);
	});
});

router.post(
	'/:lab/appointments/:appointment/results',
	multiparty({ uploadDir: './uploads' }),
	// 4194304 bytes == 4 megabytes
	function(req, res, next) {
		var file = req.files.results;
		if (file.headers['content-type'] !== 'application/pdf') {
			return next(new Error('OnlyPDF'));
		}
		if (file.size > 4194304) {
			return next(new Error('FileSizeLimit'));
		}

		var resultsPath = path.join(
			'uploads',
			req.appointment._id + '.pdf'
		);

		var base = path.normalize(__dirname + '/../../');

		async.auto({
			rename: function(callback) {
				fs.rename(
					path.normalize(file.path),
					path.join(base, resultsPath),
					callback
				);
			},
			appointment: ['rename', function(callback) {
				req.appointment.results = config.app.publicPath + '/' + resultsPath;
				req.appointment.done = true;
				req.appointment.save(function(err, appointment) {
					callback(err, appointment);
				});
			}],
			owner: function(callback) {
				User.findById(req.appointment.owner, callback);
			},
			notify: ['owner', 'appointment', function(callback, results) {
				results.owner.notify({
					ref: results.appointment._id,
					type: 'LabAppointment'
				}, callback);
			}]
		}, function(err, results) {
			if (err) return next(err);
			res.json(results.appointment);
		});
	}
);

router.post('/', userMiddleware.create, function(req, res, next) {
	var lab = new Lab();

	var address = new Address(req.body);
	lab.address = address._id;

	lab.name = req.body.name;
	lab.type = req.body.type;
	lab.phone = req.body.phone;
	lab.user = req.userObject._id;

	req.userObject.lab = lab._id;
	req.userObject.active.status = true;

	async.auto({
		services: function(callback) {
			LabService.find({
				type: lab.type
			}, function(err, services) {
				callback(err, services);
			});
		},
		user: function(callback) {
			req.userObject.save(function(err, user) {
				callback(err, user);
			});
		},
		lab: ['services', function(callback, results) {
			lab.services = results.services;
			lab.save(function(err, lab) {
				callback(err, lab);
			});
		}],
		address: function(callback) {
			if (address) {
				address.save(function(err, address) {
					callback(err, address);
				});
			} else {
				callback(null);
			}
		}
	}, function(err, results) {
		if (err) {
			return next(common.catchUnhandledErrors(err));
		}

		results.lab.populate('user address', function(err, lab) {
			if (err) return next(common.catchUnhandledErrors(err));
			res.json(lab);
		});
	});
});

router.get('/:lab/services', function(req, res, next) {
	if (_.has(req.query, 'description')) {
		LabService.find({
			_id: { $nin: req.lab.services },
			type: req.lab.type,
			description: {
				$regex: new RegExp(req.query.description, 'i')
			},
			$or: [{
				owner: { $exists: false }
			}, {
				owner: req.lab.user
			}]
		}, function(err, services) {
			res.json(services);
		});
	} else {
		res.json(req.lab.services);
	}
});

router.post('/:lab/services', function(req, res, next) {
	var service = new LabService({
		description: req.body.description,
		owner: req.user._id
	});

	req.lab.services.push(service);

	async.parallel([
		function(callback) {
			service.save(callback);
		},
		function(callback) {
			req.lab.save(callback);
		}
	], function(err) {
		if (err) return next(err);
		res.json(service);
	});
});

router.post('/:lab/services/:service', function(req, res, next) {
	req.lab.services.push(req.service);
	req.lab.save(function(err) {
		if (err) return next(err);
		res.json(req.service);
	});
});

router.put('/:lab/services/:service', function(req, res, next) {
	req.service.description = req.body.description;
	req.service.save(function(err, service) {
		if (err) return next(err);
		res.json(service);
	});
});

router.put('/:lab', function (req, res, next) {
    req.lab.name = req.body.name;
    req.lab.phone = req.body.phone;
    req.lab.rif = req.body.rif
    if (req.body.address.state) {
        req.lab.address.state = req.body.address.state;
        req.lab.address.town = req.body.address.town;
        req.lab.address.parish = req.body.address.parish;
    }
    req.lab.address.otherAddress = req.body.address.otherAddress;
    req.lab.address.postalCode = req.body.address.postalCode;
    common.setModelValues(req.body.address, req.lab.address)

    async.parallel({
        address: function (callback) {
            req.lab.address.save(function (err, address) {
                callback(err, address);
            });
        },
        lab: function (callback) {
            req.lab.save(function (err, lab) {
                callback(err, lab);
            });
        }
    },function (err, results) {
        if (err) return next(common.catchUnhandledErrors(err));
        res.json(req.lab);
    });
});

router.delete('/:labId/services/:service', function(req, res, next) {
	async.auto({
		appointment: function(callback) {
			LabAppointment.findOne({ service: req.service._id }, callback);
		},
		remove: ['appointment', function(callback, results) {
			if (results.appointment || !req.service.owner) return callback();
			req.service.remove(callback);
		}],
		lab: function(callback) {
			var query = {};
			if (mongoose.Types.ObjectId.isValid(req.params.labId)) {
				query._id = req.params.labdId
			} else {
				query.user = req.user._id
			}
			Lab.findOneAndUpdate(query, {
				$pull: { services: req.service._id }
			}, callback);
		}
	}, function(err, results) {
		if (err) return next(err);
		res.json(results);
	});
});

router.param('lab', function(req, res, next, value) {
	var query;
	if (value === 'me') {
		query = Lab.findOne({
			user: req.user._id
		})
	} else {
		query = Lab.findById(value)
	}
	query
		.populate('user address services')
		.exec(function(err, lab) {
			if (err) return next(err);
			if (!lab) return next({
				message: 'LabNotFound',
				status: 400
			})
			req.lab = lab;
			next();
		});
});

router.param('service', function(req, res, next, value) {
	LabService.findOne({
		_id: value
	}, function(err, service) {
		if (err) return next(err);
		if (!service) return next({
			message: 'ServiceNotFound',
			status: 400
		});
		req.service = service;
		next();
	});
});

router.param('appointment', function(req, res, next, value) {
	LabAppointment.findById(value)
		.populate('service patient')
		.exec(function(err, appointment) {
			if (err) return next(err);
			if (!appointment) return next(new Error('AppointmentNotFound'));
			req.appointment = appointment;
			next();
		});
});
router.put('/:lab/photo', multiparty({ uploadDir: './uploads' }), function(req, res, next) {
	// 4194304 bytes == 4 megabytes
	if (req.files.image.size > 4194304) {
		return next(new Error('FileSizeLimit'));
	}

	async.series({
		validation: function(callback) {
			if (!!req.lab.photo.path) {
				fs.unlink(path.normalize(__dirname + '/../../' + req.lab.photo.path), function(err) {
					if (err) return callback(err);
					callback();
				});
			} else {
				callback();
			}
		},
		photo: function(callback) {
			req.lab.photo.path = 'uploads/' + req.lab._id + path.extname(req.files.image.name);
			fs.rename(req.files.image.path, path.normalize(__dirname + '/../../' + req.lab.photo.path), function(err) {
				if (err) return callback(err);

				req.lab.save(function(err, savedClinic) {
					if (err) return callback(err);

					savedClinic = savedClinic.toObject();

					callback(err, savedClinic.photo);
				});

			});
		}
	}, function(err, results) {
		if (err) return next(common.catchUnhandledErrors(err));

		res.json(results.photo);
	});
});
