var mongoose = require('mongoose'),
    router = require('express').Router(),
    common = require('../common'),
    jwt = require('jsonwebtoken'),
    _ = require('lodash'),
    User = mongoose.model('User'),
    Title = mongoose.model('Title'),
    Client = mongoose.model('Client'),
    viewsMiddleware = require('../common/middlewares').views,
    middlewares = require('../common/middlewares'),
    config = require('../../config/config'),
    mailer = null,
    gcm = require('node-gcm'),
    async = require('async'),
    twilio = require('twilio'),
    pug = require('pug'),
    cleanWords = require('../common/middlewares/cleanRecords'),
    DataRecord = mongoose.model('DataRecord'),
    WordDictionary = mongoose.model('WordDictionary'),
    AWS = require("aws-sdk");
    var modem = require('modem').Modem()

var ses = new AWS.SES({
    accessKeyId: "AKIAILQGFDBGE3QMZBMQ",
    secretAccessKey: "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",
    endpoint: 'email.us-west-2.amazonaws.com',
    region: "us-west-2"
})
module.exports = function(app) {
    app.use('/', router);
    mailer = app.mailer;
};


router.post('/activate/:username', function(req, res, next) {
    User.findByUserNameForActivate(req.params.username, function(err, user) {
        if (err) return next(common.catchUnhandledErrors(err));
        if (!user) return next({
            message: 'UserNotExists',
            status: 400
        });

        if (user.active.code != req.body.code) return next({
            message: 'TokenInvalid',
            status: 400
        });

        user.password = req.body.password;
        user.active.status = true;
        user.active.code = '';

        user.update = new Date();

        user.save(
            function(err, user) {
                if (err) return next(common.catchUnhandledErrors(err));

                mailer.send('activated', {
                    to: user.username,
                    subject: 'Siplik account activated',
                    frontendUrl: config['app'].frontendPath,
                    backendUrl: config['app'].publicPath
                }, function(err) {
                    //if (err) return next({message: 'UnhandledError', status: 400});
                    //TODO arreglar envío de mails en debian

                    res.json(user);
                });
            }
        );

    });
});

router.get('/token', function(req, res, next) {

    if (!!req.user) {

        User.findOne({
                _id: req.user._id
            }).select(
                ['username', 'active.status', 'alive', 'hash', 'salt', 'iterations', 'roles', 'created', 'updated', 'doctor', 'clinic', 'patient'].join(' ')
            )
            .populate('roles')
            .populate('doctor')
            .populate('patient')
            .populate('clinic')
            .populate('data')
            .exec(function(err, user) {

                if (err) return next(common.catchUnhandledErrors(err));
                if (!user) return next({
                    message: 'UserNotFound',
                    status: 400
                });

                user = user.toObject();
                delete user.hash;
                delete user.iterations;
                delete user.salt;
                delete user.password;

                res.json(user);
            });
    } else {
        return next({
            message: 'TokenNotFound',
            status: 400
        });
    }
});

router.post('/token', function(req, res, next) {
    User.findOne({
            username: req.body.username,
            'active.status': true
        }).select(
            ['username', 'active.status', 'alive', 'hash', 'salt', 'iterations', 'roles', 'created', 'updated', 'doctor', 'clinic', 'patient', 'data'].join(' ')
        )
        .populate('roles')
        .populate('doctor')
        .populate('clinic')
        .populate('patient')
        .populate('data')
        .exec(
            function(err, user) {
                console.log("err", err)
                if (err) return next({
                    message: 'UnhandledError',
                    status: 400
                });
                if (!user) return next({
                    message: 'UserNotFound',
                    status: 400
                });
                if (req.body.app && user.doctor && !user.patient) return next({
                    message: 'NotRegisteredAsPatient',
                    status: 400
                });
                user.encrypt(req.body.password, function(err, hash) {
                    if (err) return next(common.catchUnhandledErrors(err));
                    if (user.hash != hash) return next({
                        message: 'InvalidUserPassword',
                        status: 401
                    });

                    user = user.toObject();
                    delete user.hash;
                    delete user.iterations;
                    delete user.salt;
                    delete user.password;
                    if (!!user.doctor) {
                        delete user.doctor.offices;
                        delete user.doctor.patients;
                        delete user.doctor.consultations;
                        delete user.doctor.addresses;
                        delete user.doctor.appointments;
                    }

                    var toToken = _.clone(user);
                    delete toToken.doctor;

                    if (!user.alive) {
                        mongoose.model('User').findByIdAndUpdate(user._id, {
                                $set: {
                                    alive: true
                                }
                            },
                            function(err) {
                                if (err) return next(err);

                                res.json({
                                    token: jwt.sign(toToken, config.key),
                                    user: user
                                });
                            }
                        );
                    } else {
                        console.log("el user", user)
                        let token = {
                            token: jwt.sign(user, config.key),
                            user: user
                        }
                        console.log(token)
                        res.json(token);
                    }
                });
            }
        );
});

router.post('/contact', function(req, res, next) {



    if (req.body.clinic) {

        var options = {
            to: 'info@siplik.com',
            from: 'Clinicas Siplik <form-contact@siplik.com>',
            replyTo: req.body.email,
            subject: 'Pre-Registro Clinica',
            frontendUrl: config['app'].frontendPath,
            backendUrl: config['app'].publicPath,
            name: req.body.name,
            phone: req.body.phone,
            email: req.body.email,
            rif: req.body.rif,
            password: req.body.password,
            country: req.body.country,
            state: req.body.state,
            town: req.body.town,
            parish: req.body.parish,
            postalCode: req.body.postalCode,
            otherAddress: req.body.otherAddress
        }
        var html = pug.renderFile("app/views/clinic.pug", options);

        var params = {
            Destination: {
                ToAddresses: ["info@siplik.com"]
            },
            Message: {
                Body: {
                    Html: {
                        Data: html
                    }
                },
                Subject: {
                    Data: 'Pre-Registro Clinica'
                }
            },
            Source: "g.escalona@siplik.com"
        }
        ses.sendEmail(params, function(err, email) {
            if (err) return next(err);
            res.json({
                code: 200
            });
        });

    } else if (req.body.laboratory) {
        var options = {
            to: 'info@siplik.com',
            from: 'Laboratorio Siplik <form-contact@siplik.com>',
            replyTo: req.body.email,
            subject: 'Pre-Registro Laboratorio',
            frontendUrl: config['app'].frontendPath,
            backendUrl: config['app'].publicPath,
            name: req.body.nameLaboratory,
            phone: req.body.phoneOffice,
            password: req.body.passwordLabs,
            rif: req.body.rif,
            country: req.body.country,
            state: req.body.state,
            postalCode: req.body.postalCode
        }
        var html = pug.renderFile("app/views/laboratory.pug", options);

        var params = {
            Destination: {
                ToAddresses: ["info@siplik.com"]
            },
            Message: {
                Body: {
                    Html: {
                        Data: html
                    }
                },
                Subject: {
                    Data: 'Pre-Registro Laboratorio'
                }
            },
            Source: "g.escalona@siplik.com"
        }
        ses.sendEmail(params, function(err, email) {
            if (err) return next(err);
            res.json({
                code: 200
            });
        });

    } else if (req.body.marketPlace) {
        var options = {
            to: 'info@siplik.com',
            from: 'Market-Place Siplik <form-contact@siplik.com>',
            replyTo: req.body.email,
            subject: 'Market-Place',
            frontendUrl: config['app'].frontendPath,
            backendUrl: config['app'].publicPath,
            name: req.body.name,
            company: req.body.company,
            business: req.body.business,
            country: req.body.country,
            about: req.body.about
        }
        var html = pug.renderFile("app/views/marketPlace.pug", options);

        var params = {
            Destination: {
                ToAddresses: ["info@siplik.com"]
            },
            Message: {
                Body: {
                    Html: {
                        Data: html
                    }
                },
                Subject: {
                    Data: 'Market-Place'
                }
            },
            Source: "g.escalona@siplik.com"
        }
        ses.sendEmail(params, function(err, email) {
            if (err) return next(err);
            res.json({
                code: 200
            });
        });

    } else if (req.body.inversors) {
        var options = {
            to: 'info@siplik.com',
            from: 'Laboratorio Siplik <form-contact@siplik.com>',
            replyTo: req.body.email,
            subject: 'Market-Place',
            frontendUrl: config['app'].frontendPath,
            backendUrl: config['app'].publicPath,
            name: req.body.name,
            business: req.body.business,
            country: req.body.country,
            about: req.body.about
        }
        var html = pug.renderFile("app/views/inversors.pug", options);

        var params = {
            Destination: {
                ToAddresses: ["info@siplik.com"]
            },
            Message: {
                Body: {
                    Html: {
                        Data: html
                    }
                },
                Subject: {
                    Data: 'Inversor'
                }
            },
            Source: "g.escalona@siplik.com"
        }
        ses.sendEmail(params, function(err, email) {
            if (err) return next(err);
            res.json({
                code: 200
            });
        });

    } else if (req.body.phone && req.body.email && req.body.message && req.body.name) {
        var options = {
            to: 'info@siplik.com',
            from: 'Contacto Siplik <form-contact@siplik.com>',
            replyTo: req.body.email,
            subject: req.body.title,
            frontendUrl: config['app'].frontendPath,
            backendUrl: config['app'].publicPath,
            name: req.body.name + ' ' + req.body.phone,
            phone: req.body.phone,
            message: req.body.message
        }
        var html = pug.renderFile("app/views/contact.pug", options);

        var params = {
            Destination: {
                ToAddresses: ["info@siplik.com"]
            },
            Message: {
                Body: {
                    Html: {
                        Data: html
                    }
                },
                Subject: {
                    Data: 'Contacto'
                }
            },
            Source: "g.escalona@siplik.com"
        }
        ses.sendEmail(params, function(err, email) {
            if (err) return next(err);
            res.json({
                code: 200
            });
        });

    } else {
        return next(new Error('ValidationError'));
    }
});


router.get('/remember/:email', middlewares.remember.generateCode, function(req, res, next) {
    var options = {
        to: req.params.email,
        subject: 'Recupera tu cuenta.',
        // url: 'http://localhost:9000/'+ '#/recovery/' + req.params.email + '/' + req.userObject.remember
        url: config['app'].frontendPath + '/#/recovery/' + req.params.email + '/' + req.userObject.remember,
        backendUrl: config['app'].publicPath,
        frontendUrl: config['app'].frontendPath
    }
    var html = pug.renderFile("app/views/doctor/remember.pug", options);

    var params = {
        Destination: {
            ToAddresses: [req.params.email]
        },
        Message: {
            Body: {
                Html: {
                    Data: html
                }
            },
            Subject: {
                Data: 'Recupera tu cuenta.'
            }
        },
        Source: "g.escalona@siplik.com"
    }
    ses.sendEmail(params, function(err, email) {
        if (err) return next(err);
        res.json(req.userObject);

    });

});

router.get('/mobile/remember/:email', middlewares.remember.mobileCode, function(req, res, next) {
    var options = {
        to: req.params.email,
        subject: 'Recupera tu cuenta.',
        // url: 'http://localhost:9000/'+ '#/recovery/' + req.params.email + '/' + req.userObject.remember
        code: req.userObject.remember,
        backendUrl: config['app'].publicPath,
        frontendUrl: config['app'].frontendPath
    }
    var html = pug.renderFile("app/views/patient/remember.pug", options);

    var params = {
        Destination: {
            ToAddresses: [req.params.email]
        },
        Message: {
            Body: {
                Html: {
                    Data: html
                }
            },
            Subject: {
                Data: 'Recupera tu cuenta.'
            }
        },
        Source: "g.escalona@siplik.com"
    }
    ses.sendEmail(params, function(err, email) {
        if (err) return next(err);
        res.json(req.userObject);

    });

});
router.post('/mobile/remember/:email/:code', middlewares.remember.changePassword, function(req, res, next) {
    var options = {
        to: req.params.email,
        subject: 'Tu cuenta está recuperada.',
        backendUrl: config['app'].publicPath,
        frontendUrl: config['app'].frontendPath
    }
    var html = pug.renderFile("app/views/patient/recovery.pug", options);

    var params = {
        Destination: {
            ToAddresses: [req.params.email]
        },
        Message: {
            Body: {
                Html: {
                    Data: html
                }
            },
            Subject: {
                Data: 'Tu cuenta está recuperada.'
            }
        },
        Source: "g.escalona@siplik.com"
    }
    ses.sendEmail(params, function(err, email) {
        if (err) return next(err);
        res.json(req.userObject);

    });

});
router.post('/remember/:email/:code', middlewares.remember.changePassword, function(req, res, next) {
    var options = {
        to: req.params.email,
        subject: 'Tu cuenta está recuperada.',
        backendUrl: config['app'].publicPath,
        frontendUrl: config['app'].frontendPath
    }
    var html = pug.renderFile("app/views/doctor/recovery.pug", options);

    var params = {
        Destination: {
            ToAddresses: [req.params.email]
        },
        Message: {
            Body: {
                Html: {
                    Data: html
                }
            },
            Subject: {
                Data: 'Tu cuenta está recuperada.'
            }
        },
        Source: "g.escalona@siplik.com"
    }
    ses.sendEmail(params, function(err, email) {
        if (err) return next(err);
        res.json(req.userObject);

    });

});
router.post('/registerPush', function(req, res, next) {
    var device_token = req.body.device_token;
    console.log("here\n\n\n")
    if (!req.body.patient) {
        return next(new Error("NoPatientProvided"))
    }
    console.log("herehere\n\n\n")
    Client.findOne({ clientId: req.body.patient }, function(err, client) {
        if (!client) {
            console.log("nocleinte\n\n\n")
            var obj = new Client({
                clientId: options.id,
                pushNotification: device_token
            });
            obj.save(function(err, save) {
                res.send("ok")
            })

        } else {
            console.log("cliente\n\n\n")
            client.pushNotification = device_token;
            client.save(function(err, save) {
                res.send("ok")
            })
        }
    })

    // User.findOne({patient:req.body.patient}).exec(function (err, user) {
    //     if (err) return next(err);
    //     if (!user) return next(new Error("UserNotFound"));
    //     PushNotification.findOne({user:user._id}, function (err, push) {
    //         if (err) return next(err);
    //         if (!push) {
    //             var push = new PushNotification();
    //             push.user = user._id;
    //             push.deviceToken = device_token;
    //             push.save(function (err, push) {
    //                 if (err) return next(err);
    //                 res.send('ok');
    //             })
    //         } else {
    //             if (push.deviceToken != device_token) {
    //                 push.deviceToken = device_token;
    //                 push.save(function (err, push) {
    //                     if (err) return next(err);
    //                     res.send('ok');
    //                 })
    //             } else {
    //                 res.send('ok');
    //             }
    //         }
    //     })
    // })
});

router.post('/push', function(req, res, next) {
    var device_tokens = []; //create array for storing device tokens
    var retry_times = 4; //the number of times to retry sending the message if it fails

    var sender = new gcm.Sender('AIzaSyDZeB6NjhJkMYM252vjGUg2NwjBbAGICmc'); //create a new sender
    var message = new gcm.Message({
        collapseKey: 'siplik',
        priority: 'high',
        contentAvailable: true,
        delayWhileIdle: true,
        timeToLive: 3,
        notification: {
            title: "Llamada Entrante!",
            icon: "myicon",
            body: "El Dr(a) esta tratando de contactarte",
            sound: "notification"
        },
    }); //create a new message

    message.addData('title', 'Llamada Entrante!');
    message.addData('message', 'El Dr(a) esta tratando de contactarte');
    message.addData('sound', 'notification');
    message.addData('icon', 'myicon');

    message.collapseKey = 'siplik'; //grouping messages
    message.delayWhileIdle = true; //delay sending while receiving device is offline
    message.timeToLive = 3; //the number of seconds to keep the message on the server if the device is offline

    Client.findOne({ clientId: req.body.toSend }, function(err, client) {
        if (err) return next(err);
        if (!client) return next(new Error("NoPushFound"));
        device_tokens.push(client.pushNotification);
        sender.send(message, device_tokens, retry_times, function(result) {});
        res.json({ status: 200 });
    })
});

router.get('/titles', function(req, res, next) {
    modem.open("/dev/disk2", function (a,b,c) {
        console.log(a,b,c)
    })

});

router.post('/titles', function(req, res, next) {
    var accountSid = 'AC5cd486d4be53350df8eb7c3420f88069'; // Your Account SID from www.twilio.com/console
    var authToken = '7cb4e4d1edd674dc7a0f0e2497b839b2';   // Your Auth Token from www.twilio.com/console
    console.log(req.body)

    const MessagingResponse = require('twilio').twiml.MessagingResponse;
        const response = new MessagingResponse();

        console.log(response.toString());
        res.send(response)

});

// function maxProductFinder (arr) {
//     var maxNumbers = [];
//     arr = multiply(arr)
//     for (var i = 3; i > 0; i--) {
//         let n = Math.max( ...arr ); 
//         maxNumbers.push(n);
//         arr.splice(arr.indexOf(n),1);
//     }
//     return maxNumbers.reduce((a,b) => a*b);
// }

// function maxProductFinderK (arr, k) {
//     var maxNumbers = [];
//     arr = multiply(arr)
//     for (var i = k; i > 0; i--) {
//         let n = Math.max( ...arr ); 
//         maxNumbers.push(n);
//         arr.splice(arr.indexOf(n),1);
//     }
//     return maxNumbers.reduce((a,b) => a*b);
// }

// function multiply (ints) {
//     var arr = [];
//     for (var i = ints.length - 1; i >= 0; i--) {
//         let n = ints[i];
//         ints.splice(ints.indexOf(n),1);
//         ints.map( (int) => { return arr.push(int * n) });
//     }
//     return arr
// }

// console.log("\n\n\nRESULTADOS\n\n\n")
// console.log(maxProductFinder([-8, 6, -7, 3, 2, 1, -9]))
// console.log(maxProductFinderK([-6, -8, 4, 2, 5, 3, -1, 9, 10], 4))
// console.log("\n\n\nRESULTADOS\n\n\n")
function findAndCleanOccupations(fileName) {
    console.log("Nombre del archivo:", fileName);
    var genre = false,
        age = false,
        occupation = false,
        referedBy = false,
        doctor = false,
        insurance = false;
    var keysStringsArray = []
    DataRecord.aggregate()
        .match({ $and: [{ occupation: { $exists: true } }, { occupation: { $ne: '' } }, { fileName: fileName }] })
        .group({ _id: "$occupation", count: { $sum: 1 }, recommend: { $first: "$recommended.occupation" } })
        .sort({ count: -1 })
        .exec(function(err, result) {
            let changes = [];
            console.log("Resultado de agrupacion de motivos:", err, !!result);
            if (err) return;
            async.parallel([
                function(callback) {
                    for (let [index, key] of result.entries()) {
                        keysStringsArray.push(key._id)
                        if (index >= result.length - 1) {
                            callback()
                        }
                    }
                }
            ], function() {
                console.log("Nombres de palabras a tal:", keysStringsArray.length);
                var wrapped = keysStringsArray.map(function(value, index) {
                    return { index: index, value: value };
                });

                function makeChanges() {
                    console.log("Cambios a procesar", changes.length)
                    async.map(changes, function(item, callback) {
                        DataRecord.update({ fileName: fileName, occupation: item.before, "recommended.occupation": { $exists: false } }, { $set: { "recommended.occupation": item.after } }, { multi: true }, function(err, done) {
                            callback(err, done)
                        })
                    }, function(err, results) {
                        return true
                    })
                }
                var keysRecommended = [];

                function hello() {
                    if (keysStringsArray.length < 2) {
                        console.log("voy a limpiar los solos")
                        cleanWords.cleanRecords.aloneOccupations(fileName).then((val) => {
                            changes = changes.concat(val);
                            return makeChanges()
                        })
                    }
                    let keysToLookFor = keysStringsArray.slice();
                    keysToLookFor.splice(0, 1);
                    cleanWords.cleanRecords.occupations(keysStringsArray[0], keysToLookFor, keysRecommended).then((val) => {
                        changes = changes.concat(val);
                        let keysToRemove = [];
                        for (let key of changes) {
                            keysToRemove.push(key.before);
                        }
                        if (!!changes[0].after) {
                            keysRecommended.push(changes[0].after)
                        }
                        keysStringsArray = keysToLookFor.reduce(function(acc, val) {
                            if (keysToRemove.indexOf(val) == -1) {
                                acc.push(val)
                            }
                            return acc
                        }, [])
                        hello()
                    }, (e) => {
                        cleanWords.cleanRecords.aloneOccupations(fileName).then((val) => {
                            changes.concat(val);
                            return makeChanges()
                        })
                    })
                }
                hello()


            })

        })
}

function findAndCleanMotives(fileName) {
    console.log("Nombre del archivo:", fileName);
    var genre = false,
        age = false,
        occupation = false,
        referedBy = false,
        doctor = false,
        insurance = false;
    var keysStringsArray = []
    DataRecord.aggregate()
        .match({ motive: { $exists: true }, motive: { $ne: '' }, fileName: fileName })
        .group({ _id: "$motive", count: { $sum: 1 }, recommend: { $first: "$recommended.motive" } })
        .sort({ count: -1 })
        .exec(function(err, result) {
            let changes = [];
            console.log("Resultado de agrupacion de motivos:", err, result.length);
            if (err) return;
            async.parallel([
                function(callback) {
                    for (let [index, key] of result.entries()) {
                        keysStringsArray.push(key._id)
                        if (index >= result.length - 1) {
                            callback()
                        }
                    }
                }
            ], function() {
                console.log("Nombres de palabras a tal:", keysStringsArray.length);
                var wrapped = keysStringsArray.map(function(value, index) {
                    return { index: index, value: value };
                });

                function makeChanges() {
                    console.log("Cambios a procesar", changes.length)
                    async.map(changes, function(item, callback) {
                        DataRecord.update({ fileName: fileName, motive: item.before, "recommended.motive": { $exists: false } }, { $set: { "recommended.motive": item.after } }, { multi: true }, function(err, done) {
                            callback(err, done)
                        })
                    }, function(err, results) {
                        return true
                    })
                }
                var keysRecommended = [];

                function hello() {
                    if (keysStringsArray.length < 2) {
                        console.log("voy a limpiar los solos")
                        cleanWords.cleanRecords.aloneMotives(fileName).then((val) => {
                            changes = changes.concat(val);
                            return makeChanges()
                        })
                    }
                    let keysToLookFor = keysStringsArray.slice();
                    keysToLookFor.splice(0, 1);
                    cleanWords.cleanRecords.motives(keysStringsArray[0], keysToLookFor, keysRecommended).then((val) => {
                        changes = changes.concat(val);
                        let keysToRemove = [];
                        for (let key of changes) {
                            keysToRemove.push(key.before);
                        }
                        if (!!changes[0].after) {
                            keysRecommended.push(changes[0].after)
                        }
                        keysStringsArray = keysToLookFor.reduce(function(acc, val) {
                            if (keysToRemove.indexOf(val) == -1) {
                                acc.push(val)
                            }
                            return acc
                        }, [])
                        hello()
                    }, (e) => {
                        cleanWords.cleanRecords.aloneMotives(fileName).then((val) => {
                            changes.concat(val);
                            return makeChanges()
                        })
                    })
                }
                hello()


            })

        })
}

function findAndCleanDiagnosis(fileName) {
    console.log("Nombre del archivo:", fileName);
    var genre = false,
        age = false,
        occupation = false,
        referedBy = false,
        doctor = false,
        insurance = false;
    var keysStringsArray = []
    DataRecord.aggregate()
        .match({ $and: [{ diagnosis: { $exists: true } }, { diagnosis: { $ne: '' } }, { fileName: fileName }] })
        .group({ _id: "$diagnosis", count: { $sum: 1 }, recommend: { $first: "$recommended.diagnosis" } })
        .sort({ count: -1 })
        .exec(function(err, result) {
            let changes = [];
            console.log("Resultado de agrupacion de motivos:", err, !!result);
            if (err) return;
            async.parallel([
                function(callback) {
                    for (let [index, key] of result.entries()) {
                        keysStringsArray.push(key._id)
                        if (index >= result.length - 1) {
                            callback()
                        }
                    }
                }
            ], function() {
                console.log("Nombres de palabras a tal:", keysStringsArray.length);
                var wrapped = keysStringsArray.map(function(value, index) {
                    return { index: index, value: value };
                });

                function makeChanges() {
                    console.log("Cambios a procesar", changes.length)
                    async.map(changes, function(item, callback) {
                        DataRecord.update({ fileName: fileName, diagnosis: item.before, "recommended.diagnosis": { $exists: false } }, { $set: { "recommended.diagnosis": item.after } }, { multi: true }, function(err, done) {
                            callback(err, done)
                        })
                    }, function(err, results) {
                        return true
                    })
                }
                var keysRecommended = [];

                function hello() {
                    if (keysStringsArray.length < 2) {
                        console.log("voy a limpiar los solos")
                        cleanWords.cleanRecords.aloneOccupations(fileName).then((val) => {
                            changes = changes.concat(val);
                            return makeChanges()
                        })
                    }
                    let keysToLookFor = keysStringsArray.slice();
                    keysToLookFor.splice(0, 1);
                    cleanWords.cleanRecords.occupations(keysStringsArray[0], keysToLookFor, keysRecommended).then((val) => {
                        changes = changes.concat(val);
                        let keysToRemove = [];
                        for (let key of changes) {
                            keysToRemove.push(key.before);
                        }
                        if (!!changes[0].after) {
                            keysRecommended.push(changes[0].after)
                        }
                        keysStringsArray = keysToLookFor.reduce(function(acc, val) {
                            if (keysToRemove.indexOf(val) == -1) {
                                acc.push(val)
                            }
                            return acc
                        }, [])
                        hello()
                    }, (e) => {
                        cleanWords.cleanRecords.aloneOccupations(fileName).then((val) => {
                            changes.concat(val);
                            return makeChanges()
                        })
                    })
                }
                hello()


            })

        })
}
