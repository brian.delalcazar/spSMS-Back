const gulp = require('gulp');
const babel = require('gulp-babel');
const nodemon = require('gulp-nodemon');
const livereload = require('gulp-livereload');


gulp.task('compile', function() {
    var stream = gulp.src('./src/**/*.js') // your ES2015 code
        .pipe(babel()) // compile new ones
        .pipe(gulp.dest('./dist')) // write them
    return stream // important for gulp-nodemon to wait for completion
})

gulp.task('develop', ['compile'], function() {
    var stream = nodemon({
        script: './dist/app.js' // run ES5 code
            ,
        watch: 'src/' // watch ES2015 code
            ,
        tasks: ['compile'] // compile synchronously onChange
    })
    return stream           
})

gulp.task('default', [
    'develop'
]);
