var mongoose = require('mongoose'),
	Permission = mongoose.model('Permission'),
	Role = mongoose.model('Role'),
	User = mongoose.model('User');
exports.up = function(next) {

	Role.findOne({ name: "doctor" }, function(err, role) {
		var role = role;
		User.find({ doctor: { $ne: null }, roles: { $nin: [role.id] } })
			.exec(function(err, doctors) {
				var doctorsTotal = doctors.length,
					doctorsResult = [];

				function saveAlldoctors() {
					var doctor = doctors.pop();
					doctor.roles.push(role.id)
					doctor.save(function(err, saved) {
						if (err) throw err; //handle error
						doctorsResult.push(saved[0]);

						if (--doctorsTotal) {
							saveAlldoctors()
						} else {
							return next()
						}
					})
				}
				if (doctorsTotal <= 0) {
					next()
				} else {
					saveAlldoctors()
				};
			})
	})

};

exports.down = function(next) {};
