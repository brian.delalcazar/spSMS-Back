var Permission = require('mongoose').model('Permission'),
	async = require('async');

exports.up = function(next) {

	var permission = new Permission({
		name: 'patients',
		method: 'GET PUT PATH',
		path: '/api/patients'
	});

	permission.save(function(err, permission) {
		if (err) return next(err);
		next();
	})

};

exports.down = function(next) {

	Permission.findOneAndRemove({ name: 'patients' }, next);
};
