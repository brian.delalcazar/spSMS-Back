var mongoose = require('mongoose'),
	Permission = mongoose.model('Permission'),
	Role = mongoose.model('Role'),
	User = mongoose.model('User');
exports.up = function(next) {

	Role.findOne({ name: "patients" }, function(err, role) {
		var role = role;
		User.find({ patient: { $ne: null }, roles: { $nin: [role.id] } })
			.exec(function(err, patients) {
				var patientsTotal = patients.length,
					patientsResult = [];

				function saveAllPatients() {
					var paciente = patients.pop();
					paciente.roles.push(role.id)
					paciente.save(function(err, saved) {
						if (err) throw err; //handle error
						patientsResult.push(saved[0]);

						if (--patientsTotal) {
							saveAllPatients()
						} else {
							return next()
						}
					})
				}
				if (patientsTotal <= 0) {
					next()
				} else {
					saveAllPatients()
				};
			})
	})

};

exports.down = function(next) {};
