var mongoose = require('mongoose'),
	Permission = mongoose.model('Permission'),
	Role = mongoose.model('Role');

exports.up = function(next) {

	Permission.findOne({ name: 'patients' }, function(err, permission) {

		var role = new Role();
		role.name = 'patients';
		role.permissions.push(permission);
		console.log(permission)
		role.save(function(err, role) {
			if (err) return next(err);
			next();
		})
	});
};

exports.down = function(next) {

	Role.findOneAndRemove({ name: 'patients' }, next);
};
