var mongoose = require('mongoose'),
	async = require('async'),
	Insurance = mongoose.model('Insurance');

exports.up = function(next) {

	Insurance.find().exec(function(err, insurances) {
		for (var i = 0; i < insurances.length; i++) {
			insurances[i].default = true
			insurances[i].save(function() {
				if (i == insurances.length) {
					next()
				};
			})
		};
	})

};

exports.down = function(next) {
	Insurance.remove({}, next);
};
