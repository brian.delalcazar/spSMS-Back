var mongoose = require('mongoose'),
	Group = mongoose.model('Group'),
	Week = mongoose.model('Week');
exports.up = function(next) {

	Group.findOne({ name: "20 min" }, function(err, group) {
		Week.find({ group:{$ne:group.id} })
			.exec(function(err, weeks) {
				var weeksTotal = weeks.length,
					weeksResult = [];

				function saveAllweeks() {
					var week = weeks.pop();
					week.group = group.id
					week.save(function(err, saved) {
						if (err) throw err; //handle error
						weeksResult.push(saved[0]);

						if (--weeksTotal) {
							saveAllweeks()
						} else {
							return next()
						}
					})
				}
				if (weeksTotal <= 0) {
					next()
				} else {
					saveAllweeks()
				};
			})
	})

};

exports.down = function(next) {};
