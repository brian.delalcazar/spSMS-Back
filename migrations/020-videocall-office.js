var mongoose = require('mongoose'),
	_ = require('lodash'),
	async = require('async'),
	Doctor = mongoose.model('Doctor'),
	Office = mongoose.model('Office');

exports.up = function(next) {
	Doctor.find().populate('offices').exec(function(err, doctors) {
		for (var i = 0; i < doctors.length; i++) {
			var checked = false;
			var noOffices = false;
			if (!doctors[i].offices) {
				noOffices = true
				var office = new Office();
				office.name = "Videollamada";
				office.doctor = doctors[i]._id;
				async.parallel({
					office: function(callback) {
						office.save(function(err, office) {
							if (err) return callback(office);

							callback(null, office);
						});
					},
					doctor: function(callback) {
						doctors[i].update({
							$set: {
								offices: [office._id]
							}
						}, function(err, doctor) {
							if (err) return callback(doctor);
							callback(null, doctor);
						});
					}
				}, function(err, results) {
					if (err) return next(err);
				});
			};
			if (!noOffices && doctors[i].offices.length > 0) {
				for (var j = 0; j < doctors[i].offices.length; j++) {
					if (doctors[i].offices[j] && doctors[i].offices[j].name.toLowerCase() == "videollamada") {
						checked = true;
					};
				};
			};
			if (!checked && !noOffices) {
				var office = new Office();
				office.name = "Videollamada";
				office.doctor = doctors[i]._id;
				async.parallel({
					office: function(callback) {
						office.save(function(err, office) {
							if (err) return callback(office);

							callback(null, office);
						});
					},
					doctor: function(callback) {
						doctors[i].update({
							$push: {
								offices: office._id
							}
						}, function(err, doctor) {
							if (err) return callback(doctor);
							callback(null, doctor);
						});
					}
				}, function(err, results) {
					if (err) return next(err);
				});
			};
		};
		next()
	})
};

exports.down = function(next) {
	next()
};
