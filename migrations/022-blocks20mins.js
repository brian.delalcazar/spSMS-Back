var mongoose = require('mongoose'),
	async = require('async'),
	Block = mongoose.model('Block'),
	Group = mongoose.model('Group');

exports.up = function(next) {

	var blocksString = [],
		hour = 6,
		min = 0,
		index = 1;

	for (var i = hour; hour <= 19; i) {
		var amPm = "",
			beginHour = hour,
			endMin = 0,
			endHour = hour;
		if (hour == 12) {
			amPm = "m"
		} else if (hour > 12) {
			amPm = "p.m."
		} else {
			amPm = "a.m."
		};
		if (beginHour > 12) {
			beginHour = beginHour - 12;
		};
		var begin = beginHour.toString() + ":" + min.toString() + "0 " + amPm; 
		if (min == 4) {
			endMin = 0;
			endHour += 1;
		} else if (min == 2) {
			endMin = 4
		} else {
			endMin = 2			
		};		
		amPm = "";
		if (endHour == 12) {
			amPm = "m"
		} else if (endHour > 12) {
			amPm = "p.m."
		} else {
			amPm = "a.m."
		};
		if (endHour > 12) {
			endHour = endHour - 12;
		};
		var end = endHour.toString() + ":" + endMin.toString() + "0 " + amPm;
		var b = {
			begin: begin,
			end: end,
			index: index
		};
		blocksString.push(b);
		index+=1;
		if (hour == 19) {
			hour+=1;
		};
		if (min == 4) {
			min = 0;
			hour += 1
		} else {
			min+=2
		};
	};

	var blocks = [];

	var group = new Group();
	group.name = '20 min';

	blocksString.forEach(function(blockString) {

		var block = new Block(blockString);
		group.blocks.push(block._id);
		blocks.push(function(callback) {
			block.save(callback);
		});
	});

	blocks.push(function(callback) {
		group.save(callback);
	});

	async.parallel(blocks, function(err, results) {
		if (err) return next(err);
		next();
	});

};

exports.down = function(next) {
	async.parallel({
		block: function(callback) {
			Block.remove({}, callback);
		},
		group: function(callback) {
			Group.remove({}, callback);
		}
	}, next)

};
